<?php

class grid_bayer_pacientes_rtf
{
   var $Db;
   var $Erro;
   var $Ini;
   var $Lookup;
   var $nm_data;
   var $Texto_tag;
   var $Arquivo;
   var $Tit_doc;
   var $sc_proc_grid; 
   var $NM_cmp_hidden = array();

   //---- 
   function grid_bayer_pacientes_rtf()
   {
      $this->nm_data   = new nm_data("es");
      $this->Texto_tag = "";
   }

   //---- 
   function monta_rtf()
   {
      $this->inicializa_vars();
      $this->gera_texto_tag();
      $this->grava_arquivo_rtf();
      $this->monta_html();
   }

   //----- 
   function inicializa_vars()
   {
      global $nm_lang;
      $dir_raiz          = strrpos($_SERVER['PHP_SELF'],"/") ;  
      $dir_raiz          = substr($_SERVER['PHP_SELF'], 0, $dir_raiz + 1) ;  
      $this->nm_location = $this->Ini->sc_protocolo . $this->Ini->server . $dir_raiz; 
      $this->Arquivo    = "sc_rtf";
      $this->Arquivo   .= "_" . date("YmdHis") . "_" . rand(0, 1000);
      $this->Arquivo   .= "_grid_bayer_pacientes";
      $this->Arquivo   .= ".rtf";
      $this->Tit_doc    = "grid_bayer_pacientes.rtf";
   }

   //----- 
   function gera_texto_tag()
   {
     global $nm_lang;
      global
             $nm_nada, $nm_lang;

      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->sc_proc_grid = false; 
      $nm_raiz_img  = ""; 
      if (isset($_SESSION['scriptcase']['sc_apl_conf']['grid_bayer_pacientes']['field_display']) && !empty($_SESSION['scriptcase']['sc_apl_conf']['grid_bayer_pacientes']['field_display']))
      {
          foreach ($_SESSION['scriptcase']['sc_apl_conf']['grid_bayer_pacientes']['field_display'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_pacientes']['usr_cmp_sel']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_pacientes']['usr_cmp_sel']))
      {
          foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_pacientes']['usr_cmp_sel'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_pacientes']['php_cmp_sel']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_pacientes']['php_cmp_sel']))
      {
          foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_pacientes']['php_cmp_sel'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_pacientes']['campos_busca']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_pacientes']['campos_busca']))
      { 
          $Busca_temp = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_pacientes']['campos_busca'];
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
              $Busca_temp = NM_conv_charset($Busca_temp, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->id_paciente = $Busca_temp['id_paciente']; 
          $tmp_pos = strpos($this->id_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->id_paciente = substr($this->id_paciente, 0, $tmp_pos);
          }
          $this->id_paciente_2 = $Busca_temp['id_paciente_input_2']; 
          $this->estado_paciente = $Busca_temp['estado_paciente']; 
          $tmp_pos = strpos($this->estado_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->estado_paciente = substr($this->estado_paciente, 0, $tmp_pos);
          }
          $this->fecha_activacion_paciente = $Busca_temp['fecha_activacion_paciente']; 
          $tmp_pos = strpos($this->fecha_activacion_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->fecha_activacion_paciente = substr($this->fecha_activacion_paciente, 0, $tmp_pos);
          }
          $this->fecha_retiro_paciente = $Busca_temp['fecha_retiro_paciente']; 
          $tmp_pos = strpos($this->fecha_retiro_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->fecha_retiro_paciente = substr($this->fecha_retiro_paciente, 0, $tmp_pos);
          }
      } 
      $this->nm_field_dinamico = array();
      $this->nm_order_dinamico = array();
      $this->sc_where_orig   = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_pacientes']['where_orig'];
      $this->sc_where_atual  = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_pacientes']['where_pesq'];
      $this->sc_where_filtro = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_pacientes']['where_pesq_filtro'];
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_pacientes']['rtf_name']))
      {
          $this->Arquivo = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_pacientes']['rtf_name'];
          $this->Tit_doc = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_pacientes']['rtf_name'];
          unset($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_pacientes']['rtf_name']);
      }
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase))
      { 
          $nmgp_select = "SELECT ID_PACIENTE, ESTADO_PACIENTE, FECHA_ACTIVACION_PACIENTE, FECHA_RETIRO_PACIENTE, MOTIVO_RETIRO_PACIENTE, OBSERVACION_MOTIVO_RETIRO_PACIENTE, IDENTIFICACION_PACIENTE, NOMBRE_PACIENTE, APELLIDO_PACIENTE, TELEFONO_PACIENTE, TELEFONO2_PACIENTE, TELEFONO3_PACIENTE, CORREO_PACIENTE, DIRECCION_PACIENTE, BARRIO_PACIENTE, DEPARTAMENTO_PACIENTE, CIUDAD_PACIENTE, GENERO_PACIENTE, FECHA_NACIMINETO_PACIENTE, EDAD_PACIENTE, ACUDIENTE_PACIENTE, TELEFONO_ACUDIENTE_PACIENTE, ID_ULTIMA_GESTION, USUARIO_CREACION from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
      { 
          $nmgp_select = "SELECT ID_PACIENTE, ESTADO_PACIENTE, FECHA_ACTIVACION_PACIENTE, FECHA_RETIRO_PACIENTE, MOTIVO_RETIRO_PACIENTE, OBSERVACION_MOTIVO_RETIRO_PACIENTE, IDENTIFICACION_PACIENTE, NOMBRE_PACIENTE, APELLIDO_PACIENTE, TELEFONO_PACIENTE, TELEFONO2_PACIENTE, TELEFONO3_PACIENTE, CORREO_PACIENTE, DIRECCION_PACIENTE, BARRIO_PACIENTE, DEPARTAMENTO_PACIENTE, CIUDAD_PACIENTE, GENERO_PACIENTE, FECHA_NACIMINETO_PACIENTE, EDAD_PACIENTE, ACUDIENTE_PACIENTE, TELEFONO_ACUDIENTE_PACIENTE, ID_ULTIMA_GESTION, USUARIO_CREACION from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
      { 
       $nmgp_select = "SELECT ID_PACIENTE, ESTADO_PACIENTE, FECHA_ACTIVACION_PACIENTE, FECHA_RETIRO_PACIENTE, MOTIVO_RETIRO_PACIENTE, OBSERVACION_MOTIVO_RETIRO_PACIENTE, IDENTIFICACION_PACIENTE, NOMBRE_PACIENTE, APELLIDO_PACIENTE, TELEFONO_PACIENTE, TELEFONO2_PACIENTE, TELEFONO3_PACIENTE, CORREO_PACIENTE, DIRECCION_PACIENTE, BARRIO_PACIENTE, DEPARTAMENTO_PACIENTE, CIUDAD_PACIENTE, GENERO_PACIENTE, FECHA_NACIMINETO_PACIENTE, EDAD_PACIENTE, ACUDIENTE_PACIENTE, TELEFONO_ACUDIENTE_PACIENTE, ID_ULTIMA_GESTION, USUARIO_CREACION from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
      { 
          $nmgp_select = "SELECT ID_PACIENTE, ESTADO_PACIENTE, FECHA_ACTIVACION_PACIENTE, FECHA_RETIRO_PACIENTE, MOTIVO_RETIRO_PACIENTE, OBSERVACION_MOTIVO_RETIRO_PACIENTE, IDENTIFICACION_PACIENTE, NOMBRE_PACIENTE, APELLIDO_PACIENTE, TELEFONO_PACIENTE, TELEFONO2_PACIENTE, TELEFONO3_PACIENTE, CORREO_PACIENTE, DIRECCION_PACIENTE, BARRIO_PACIENTE, DEPARTAMENTO_PACIENTE, CIUDAD_PACIENTE, GENERO_PACIENTE, FECHA_NACIMINETO_PACIENTE, EDAD_PACIENTE, ACUDIENTE_PACIENTE, TELEFONO_ACUDIENTE_PACIENTE, ID_ULTIMA_GESTION, USUARIO_CREACION from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
      { 
          $nmgp_select = "SELECT ID_PACIENTE, ESTADO_PACIENTE, FECHA_ACTIVACION_PACIENTE, FECHA_RETIRO_PACIENTE, MOTIVO_RETIRO_PACIENTE, OBSERVACION_MOTIVO_RETIRO_PACIENTE, IDENTIFICACION_PACIENTE, NOMBRE_PACIENTE, APELLIDO_PACIENTE, TELEFONO_PACIENTE, TELEFONO2_PACIENTE, TELEFONO3_PACIENTE, CORREO_PACIENTE, DIRECCION_PACIENTE, BARRIO_PACIENTE, DEPARTAMENTO_PACIENTE, CIUDAD_PACIENTE, GENERO_PACIENTE, FECHA_NACIMINETO_PACIENTE, EDAD_PACIENTE, ACUDIENTE_PACIENTE, TELEFONO_ACUDIENTE_PACIENTE, ID_ULTIMA_GESTION, USUARIO_CREACION from " . $this->Ini->nm_tabela; 
      } 
      else 
      { 
          $nmgp_select = "SELECT ID_PACIENTE, ESTADO_PACIENTE, FECHA_ACTIVACION_PACIENTE, FECHA_RETIRO_PACIENTE, MOTIVO_RETIRO_PACIENTE, OBSERVACION_MOTIVO_RETIRO_PACIENTE, IDENTIFICACION_PACIENTE, NOMBRE_PACIENTE, APELLIDO_PACIENTE, TELEFONO_PACIENTE, TELEFONO2_PACIENTE, TELEFONO3_PACIENTE, CORREO_PACIENTE, DIRECCION_PACIENTE, BARRIO_PACIENTE, DEPARTAMENTO_PACIENTE, CIUDAD_PACIENTE, GENERO_PACIENTE, FECHA_NACIMINETO_PACIENTE, EDAD_PACIENTE, ACUDIENTE_PACIENTE, TELEFONO_ACUDIENTE_PACIENTE, ID_ULTIMA_GESTION, USUARIO_CREACION from " . $this->Ini->nm_tabela; 
      } 
      $nmgp_select .= " " . $_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_pacientes']['where_pesq'];
      $nmgp_order_by = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_pacientes']['order_grid'];
      $nmgp_select .= $nmgp_order_by; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nmgp_select;
      $rs = $this->Db->Execute($nmgp_select);
      if ($rs === false && !$rs->EOF && $GLOBALS["NM_ERRO_IBASE"] != 1)
      {
         $this->Erro->mensagem(__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg());
         exit;
      }

      $this->Texto_tag .= "<table>\r\n";
      $this->Texto_tag .= "<tr>\r\n";
      foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_pacientes']['field_order'] as $Cada_col)
      { 
          $SC_Label = (isset($this->New_label['id_paciente'])) ? $this->New_label['id_paciente'] : "ID PACIENTE"; 
          if ($Cada_col == "id_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['estado_paciente'])) ? $this->New_label['estado_paciente'] : "ESTADO PACIENTE"; 
          if ($Cada_col == "estado_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['fecha_activacion_paciente'])) ? $this->New_label['fecha_activacion_paciente'] : "FECHA ACTIVACION PACIENTE"; 
          if ($Cada_col == "fecha_activacion_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['fecha_retiro_paciente'])) ? $this->New_label['fecha_retiro_paciente'] : "FECHA RETIRO PACIENTE"; 
          if ($Cada_col == "fecha_retiro_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['motivo_retiro_paciente'])) ? $this->New_label['motivo_retiro_paciente'] : "MOTIVO RETIRO PACIENTE"; 
          if ($Cada_col == "motivo_retiro_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['observacion_motivo_retiro_paciente'])) ? $this->New_label['observacion_motivo_retiro_paciente'] : "OBSERVACION MOTIVO RETIRO PACIENTE"; 
          if ($Cada_col == "observacion_motivo_retiro_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['identificacion_paciente'])) ? $this->New_label['identificacion_paciente'] : "IDENTIFICACION PACIENTE"; 
          if ($Cada_col == "identificacion_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['nombre_paciente'])) ? $this->New_label['nombre_paciente'] : "NOMBRE PACIENTE"; 
          if ($Cada_col == "nombre_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['apellido_paciente'])) ? $this->New_label['apellido_paciente'] : "APELLIDO PACIENTE"; 
          if ($Cada_col == "apellido_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['telefono_paciente'])) ? $this->New_label['telefono_paciente'] : "TELEFONO PACIENTE"; 
          if ($Cada_col == "telefono_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['telefono2_paciente'])) ? $this->New_label['telefono2_paciente'] : "TELEFONO2 PACIENTE"; 
          if ($Cada_col == "telefono2_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['telefono3_paciente'])) ? $this->New_label['telefono3_paciente'] : "TELEFONO3 PACIENTE"; 
          if ($Cada_col == "telefono3_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['correo_paciente'])) ? $this->New_label['correo_paciente'] : "CORREO PACIENTE"; 
          if ($Cada_col == "correo_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['direccion_paciente'])) ? $this->New_label['direccion_paciente'] : "DIRECCION PACIENTE"; 
          if ($Cada_col == "direccion_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['barrio_paciente'])) ? $this->New_label['barrio_paciente'] : "BARRIO PACIENTE"; 
          if ($Cada_col == "barrio_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['departamento_paciente'])) ? $this->New_label['departamento_paciente'] : "DEPARTAMENTO PACIENTE"; 
          if ($Cada_col == "departamento_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['ciudad_paciente'])) ? $this->New_label['ciudad_paciente'] : "CIUDAD PACIENTE"; 
          if ($Cada_col == "ciudad_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['genero_paciente'])) ? $this->New_label['genero_paciente'] : "GENERO PACIENTE"; 
          if ($Cada_col == "genero_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['fecha_nacimineto_paciente'])) ? $this->New_label['fecha_nacimineto_paciente'] : "FECHA NACIMINETO PACIENTE"; 
          if ($Cada_col == "fecha_nacimineto_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['edad_paciente'])) ? $this->New_label['edad_paciente'] : "EDAD PACIENTE"; 
          if ($Cada_col == "edad_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['acudiente_paciente'])) ? $this->New_label['acudiente_paciente'] : "ACUDIENTE PACIENTE"; 
          if ($Cada_col == "acudiente_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['telefono_acudiente_paciente'])) ? $this->New_label['telefono_acudiente_paciente'] : "TELEFONO ACUDIENTE PACIENTE"; 
          if ($Cada_col == "telefono_acudiente_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['id_ultima_gestion'])) ? $this->New_label['id_ultima_gestion'] : "ID ULTIMA GESTION"; 
          if ($Cada_col == "id_ultima_gestion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['usuario_creacion'])) ? $this->New_label['usuario_creacion'] : "USUARIO CREACION"; 
          if ($Cada_col == "usuario_creacion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
      } 
      $this->Texto_tag .= "</tr>\r\n";
      while (!$rs->EOF)
      {
         $this->Texto_tag .= "<tr>\r\n";
         $this->id_paciente = $rs->fields[0] ;  
         $this->id_paciente = (string)$this->id_paciente;
         $this->estado_paciente = $rs->fields[1] ;  
         $this->fecha_activacion_paciente = $rs->fields[2] ;  
         $this->fecha_retiro_paciente = $rs->fields[3] ;  
         $this->motivo_retiro_paciente = $rs->fields[4] ;  
         $this->observacion_motivo_retiro_paciente = $rs->fields[5] ;  
         $this->identificacion_paciente = $rs->fields[6] ;  
         $this->nombre_paciente = $rs->fields[7] ;  
         $this->apellido_paciente = $rs->fields[8] ;  
         $this->telefono_paciente = $rs->fields[9] ;  
         $this->telefono2_paciente = $rs->fields[10] ;  
         $this->telefono3_paciente = $rs->fields[11] ;  
         $this->correo_paciente = $rs->fields[12] ;  
         $this->direccion_paciente = $rs->fields[13] ;  
         $this->barrio_paciente = $rs->fields[14] ;  
         $this->departamento_paciente = $rs->fields[15] ;  
         $this->ciudad_paciente = $rs->fields[16] ;  
         $this->genero_paciente = $rs->fields[17] ;  
         $this->fecha_nacimineto_paciente = $rs->fields[18] ;  
         $this->edad_paciente = $rs->fields[19] ;  
         $this->edad_paciente = (string)$this->edad_paciente;
         $this->acudiente_paciente = $rs->fields[20] ;  
         $this->telefono_acudiente_paciente = $rs->fields[21] ;  
         $this->id_ultima_gestion = $rs->fields[22] ;  
         $this->id_ultima_gestion = (string)$this->id_ultima_gestion;
         $this->usuario_creacion = $rs->fields[23] ;  
         $this->sc_proc_grid = true; 
         foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_pacientes']['field_order'] as $Cada_col)
         { 
            if (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off")
            { 
                $NM_func_exp = "NM_export_" . $Cada_col;
                $this->$NM_func_exp();
            } 
         } 
         $this->Texto_tag .= "</tr>\r\n";
         $rs->MoveNext();
      }
      $this->Texto_tag .= "</table>\r\n";

      $rs->Close();
   }
   //----- id_paciente
   function NM_export_id_paciente()
   {
         nmgp_Form_Num_Val($this->id_paciente, $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
         if (!NM_is_utf8($this->id_paciente))
         {
             $this->id_paciente = sc_convert_encoding($this->id_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->id_paciente = str_replace('<', '&lt;', $this->id_paciente);
         $this->id_paciente = str_replace('>', '&gt;', $this->id_paciente);
         $this->Texto_tag .= "<td>" . $this->id_paciente . "</td>\r\n";
   }
   //----- estado_paciente
   function NM_export_estado_paciente()
   {
         $this->estado_paciente = html_entity_decode($this->estado_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->estado_paciente = strip_tags($this->estado_paciente);
         if (!NM_is_utf8($this->estado_paciente))
         {
             $this->estado_paciente = sc_convert_encoding($this->estado_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->estado_paciente = str_replace('<', '&lt;', $this->estado_paciente);
         $this->estado_paciente = str_replace('>', '&gt;', $this->estado_paciente);
         $this->Texto_tag .= "<td>" . $this->estado_paciente . "</td>\r\n";
   }
   //----- fecha_activacion_paciente
   function NM_export_fecha_activacion_paciente()
   {
         $this->fecha_activacion_paciente = html_entity_decode($this->fecha_activacion_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->fecha_activacion_paciente = strip_tags($this->fecha_activacion_paciente);
         if (!NM_is_utf8($this->fecha_activacion_paciente))
         {
             $this->fecha_activacion_paciente = sc_convert_encoding($this->fecha_activacion_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->fecha_activacion_paciente = str_replace('<', '&lt;', $this->fecha_activacion_paciente);
         $this->fecha_activacion_paciente = str_replace('>', '&gt;', $this->fecha_activacion_paciente);
         $this->Texto_tag .= "<td>" . $this->fecha_activacion_paciente . "</td>\r\n";
   }
   //----- fecha_retiro_paciente
   function NM_export_fecha_retiro_paciente()
   {
         $this->fecha_retiro_paciente = html_entity_decode($this->fecha_retiro_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->fecha_retiro_paciente = strip_tags($this->fecha_retiro_paciente);
         if (!NM_is_utf8($this->fecha_retiro_paciente))
         {
             $this->fecha_retiro_paciente = sc_convert_encoding($this->fecha_retiro_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->fecha_retiro_paciente = str_replace('<', '&lt;', $this->fecha_retiro_paciente);
         $this->fecha_retiro_paciente = str_replace('>', '&gt;', $this->fecha_retiro_paciente);
         $this->Texto_tag .= "<td>" . $this->fecha_retiro_paciente . "</td>\r\n";
   }
   //----- motivo_retiro_paciente
   function NM_export_motivo_retiro_paciente()
   {
         $this->motivo_retiro_paciente = html_entity_decode($this->motivo_retiro_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->motivo_retiro_paciente = strip_tags($this->motivo_retiro_paciente);
         if (!NM_is_utf8($this->motivo_retiro_paciente))
         {
             $this->motivo_retiro_paciente = sc_convert_encoding($this->motivo_retiro_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->motivo_retiro_paciente = str_replace('<', '&lt;', $this->motivo_retiro_paciente);
         $this->motivo_retiro_paciente = str_replace('>', '&gt;', $this->motivo_retiro_paciente);
         $this->Texto_tag .= "<td>" . $this->motivo_retiro_paciente . "</td>\r\n";
   }
   //----- observacion_motivo_retiro_paciente
   function NM_export_observacion_motivo_retiro_paciente()
   {
         $this->observacion_motivo_retiro_paciente = html_entity_decode($this->observacion_motivo_retiro_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->observacion_motivo_retiro_paciente = strip_tags($this->observacion_motivo_retiro_paciente);
         if (!NM_is_utf8($this->observacion_motivo_retiro_paciente))
         {
             $this->observacion_motivo_retiro_paciente = sc_convert_encoding($this->observacion_motivo_retiro_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->observacion_motivo_retiro_paciente = str_replace('<', '&lt;', $this->observacion_motivo_retiro_paciente);
         $this->observacion_motivo_retiro_paciente = str_replace('>', '&gt;', $this->observacion_motivo_retiro_paciente);
         $this->Texto_tag .= "<td>" . $this->observacion_motivo_retiro_paciente . "</td>\r\n";
   }
   //----- identificacion_paciente
   function NM_export_identificacion_paciente()
   {
         $this->identificacion_paciente = html_entity_decode($this->identificacion_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->identificacion_paciente = strip_tags($this->identificacion_paciente);
         if (!NM_is_utf8($this->identificacion_paciente))
         {
             $this->identificacion_paciente = sc_convert_encoding($this->identificacion_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->identificacion_paciente = str_replace('<', '&lt;', $this->identificacion_paciente);
         $this->identificacion_paciente = str_replace('>', '&gt;', $this->identificacion_paciente);
         $this->Texto_tag .= "<td>" . $this->identificacion_paciente . "</td>\r\n";
   }
   //----- nombre_paciente
   function NM_export_nombre_paciente()
   {
         $this->nombre_paciente = html_entity_decode($this->nombre_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->nombre_paciente = strip_tags($this->nombre_paciente);
         if (!NM_is_utf8($this->nombre_paciente))
         {
             $this->nombre_paciente = sc_convert_encoding($this->nombre_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->nombre_paciente = str_replace('<', '&lt;', $this->nombre_paciente);
         $this->nombre_paciente = str_replace('>', '&gt;', $this->nombre_paciente);
         $this->Texto_tag .= "<td>" . $this->nombre_paciente . "</td>\r\n";
   }
   //----- apellido_paciente
   function NM_export_apellido_paciente()
   {
         $this->apellido_paciente = html_entity_decode($this->apellido_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->apellido_paciente = strip_tags($this->apellido_paciente);
         if (!NM_is_utf8($this->apellido_paciente))
         {
             $this->apellido_paciente = sc_convert_encoding($this->apellido_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->apellido_paciente = str_replace('<', '&lt;', $this->apellido_paciente);
         $this->apellido_paciente = str_replace('>', '&gt;', $this->apellido_paciente);
         $this->Texto_tag .= "<td>" . $this->apellido_paciente . "</td>\r\n";
   }
   //----- telefono_paciente
   function NM_export_telefono_paciente()
   {
         $this->telefono_paciente = html_entity_decode($this->telefono_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->telefono_paciente = strip_tags($this->telefono_paciente);
         if (!NM_is_utf8($this->telefono_paciente))
         {
             $this->telefono_paciente = sc_convert_encoding($this->telefono_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->telefono_paciente = str_replace('<', '&lt;', $this->telefono_paciente);
         $this->telefono_paciente = str_replace('>', '&gt;', $this->telefono_paciente);
         $this->Texto_tag .= "<td>" . $this->telefono_paciente . "</td>\r\n";
   }
   //----- telefono2_paciente
   function NM_export_telefono2_paciente()
   {
         $this->telefono2_paciente = html_entity_decode($this->telefono2_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->telefono2_paciente = strip_tags($this->telefono2_paciente);
         if (!NM_is_utf8($this->telefono2_paciente))
         {
             $this->telefono2_paciente = sc_convert_encoding($this->telefono2_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->telefono2_paciente = str_replace('<', '&lt;', $this->telefono2_paciente);
         $this->telefono2_paciente = str_replace('>', '&gt;', $this->telefono2_paciente);
         $this->Texto_tag .= "<td>" . $this->telefono2_paciente . "</td>\r\n";
   }
   //----- telefono3_paciente
   function NM_export_telefono3_paciente()
   {
         $this->telefono3_paciente = html_entity_decode($this->telefono3_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->telefono3_paciente = strip_tags($this->telefono3_paciente);
         if (!NM_is_utf8($this->telefono3_paciente))
         {
             $this->telefono3_paciente = sc_convert_encoding($this->telefono3_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->telefono3_paciente = str_replace('<', '&lt;', $this->telefono3_paciente);
         $this->telefono3_paciente = str_replace('>', '&gt;', $this->telefono3_paciente);
         $this->Texto_tag .= "<td>" . $this->telefono3_paciente . "</td>\r\n";
   }
   //----- correo_paciente
   function NM_export_correo_paciente()
   {
         $this->correo_paciente = html_entity_decode($this->correo_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->correo_paciente = strip_tags($this->correo_paciente);
         if (!NM_is_utf8($this->correo_paciente))
         {
             $this->correo_paciente = sc_convert_encoding($this->correo_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->correo_paciente = str_replace('<', '&lt;', $this->correo_paciente);
         $this->correo_paciente = str_replace('>', '&gt;', $this->correo_paciente);
         $this->Texto_tag .= "<td>" . $this->correo_paciente . "</td>\r\n";
   }
   //----- direccion_paciente
   function NM_export_direccion_paciente()
   {
         $this->direccion_paciente = html_entity_decode($this->direccion_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->direccion_paciente = strip_tags($this->direccion_paciente);
         if (!NM_is_utf8($this->direccion_paciente))
         {
             $this->direccion_paciente = sc_convert_encoding($this->direccion_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->direccion_paciente = str_replace('<', '&lt;', $this->direccion_paciente);
         $this->direccion_paciente = str_replace('>', '&gt;', $this->direccion_paciente);
         $this->Texto_tag .= "<td>" . $this->direccion_paciente . "</td>\r\n";
   }
   //----- barrio_paciente
   function NM_export_barrio_paciente()
   {
         $this->barrio_paciente = html_entity_decode($this->barrio_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->barrio_paciente = strip_tags($this->barrio_paciente);
         if (!NM_is_utf8($this->barrio_paciente))
         {
             $this->barrio_paciente = sc_convert_encoding($this->barrio_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->barrio_paciente = str_replace('<', '&lt;', $this->barrio_paciente);
         $this->barrio_paciente = str_replace('>', '&gt;', $this->barrio_paciente);
         $this->Texto_tag .= "<td>" . $this->barrio_paciente . "</td>\r\n";
   }
   //----- departamento_paciente
   function NM_export_departamento_paciente()
   {
         $this->departamento_paciente = html_entity_decode($this->departamento_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->departamento_paciente = strip_tags($this->departamento_paciente);
         if (!NM_is_utf8($this->departamento_paciente))
         {
             $this->departamento_paciente = sc_convert_encoding($this->departamento_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->departamento_paciente = str_replace('<', '&lt;', $this->departamento_paciente);
         $this->departamento_paciente = str_replace('>', '&gt;', $this->departamento_paciente);
         $this->Texto_tag .= "<td>" . $this->departamento_paciente . "</td>\r\n";
   }
   //----- ciudad_paciente
   function NM_export_ciudad_paciente()
   {
         $this->ciudad_paciente = html_entity_decode($this->ciudad_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->ciudad_paciente = strip_tags($this->ciudad_paciente);
         if (!NM_is_utf8($this->ciudad_paciente))
         {
             $this->ciudad_paciente = sc_convert_encoding($this->ciudad_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->ciudad_paciente = str_replace('<', '&lt;', $this->ciudad_paciente);
         $this->ciudad_paciente = str_replace('>', '&gt;', $this->ciudad_paciente);
         $this->Texto_tag .= "<td>" . $this->ciudad_paciente . "</td>\r\n";
   }
   //----- genero_paciente
   function NM_export_genero_paciente()
   {
         $this->genero_paciente = html_entity_decode($this->genero_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->genero_paciente = strip_tags($this->genero_paciente);
         if (!NM_is_utf8($this->genero_paciente))
         {
             $this->genero_paciente = sc_convert_encoding($this->genero_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->genero_paciente = str_replace('<', '&lt;', $this->genero_paciente);
         $this->genero_paciente = str_replace('>', '&gt;', $this->genero_paciente);
         $this->Texto_tag .= "<td>" . $this->genero_paciente . "</td>\r\n";
   }
   //----- fecha_nacimineto_paciente
   function NM_export_fecha_nacimineto_paciente()
   {
         $this->fecha_nacimineto_paciente = html_entity_decode($this->fecha_nacimineto_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->fecha_nacimineto_paciente = strip_tags($this->fecha_nacimineto_paciente);
         if (!NM_is_utf8($this->fecha_nacimineto_paciente))
         {
             $this->fecha_nacimineto_paciente = sc_convert_encoding($this->fecha_nacimineto_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->fecha_nacimineto_paciente = str_replace('<', '&lt;', $this->fecha_nacimineto_paciente);
         $this->fecha_nacimineto_paciente = str_replace('>', '&gt;', $this->fecha_nacimineto_paciente);
         $this->Texto_tag .= "<td>" . $this->fecha_nacimineto_paciente . "</td>\r\n";
   }
   //----- edad_paciente
   function NM_export_edad_paciente()
   {
         nmgp_Form_Num_Val($this->edad_paciente, $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
         if (!NM_is_utf8($this->edad_paciente))
         {
             $this->edad_paciente = sc_convert_encoding($this->edad_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->edad_paciente = str_replace('<', '&lt;', $this->edad_paciente);
         $this->edad_paciente = str_replace('>', '&gt;', $this->edad_paciente);
         $this->Texto_tag .= "<td>" . $this->edad_paciente . "</td>\r\n";
   }
   //----- acudiente_paciente
   function NM_export_acudiente_paciente()
   {
         $this->acudiente_paciente = html_entity_decode($this->acudiente_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->acudiente_paciente = strip_tags($this->acudiente_paciente);
         if (!NM_is_utf8($this->acudiente_paciente))
         {
             $this->acudiente_paciente = sc_convert_encoding($this->acudiente_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->acudiente_paciente = str_replace('<', '&lt;', $this->acudiente_paciente);
         $this->acudiente_paciente = str_replace('>', '&gt;', $this->acudiente_paciente);
         $this->Texto_tag .= "<td>" . $this->acudiente_paciente . "</td>\r\n";
   }
   //----- telefono_acudiente_paciente
   function NM_export_telefono_acudiente_paciente()
   {
         $this->telefono_acudiente_paciente = html_entity_decode($this->telefono_acudiente_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->telefono_acudiente_paciente = strip_tags($this->telefono_acudiente_paciente);
         if (!NM_is_utf8($this->telefono_acudiente_paciente))
         {
             $this->telefono_acudiente_paciente = sc_convert_encoding($this->telefono_acudiente_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->telefono_acudiente_paciente = str_replace('<', '&lt;', $this->telefono_acudiente_paciente);
         $this->telefono_acudiente_paciente = str_replace('>', '&gt;', $this->telefono_acudiente_paciente);
         $this->Texto_tag .= "<td>" . $this->telefono_acudiente_paciente . "</td>\r\n";
   }
   //----- id_ultima_gestion
   function NM_export_id_ultima_gestion()
   {
         nmgp_Form_Num_Val($this->id_ultima_gestion, $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
         if (!NM_is_utf8($this->id_ultima_gestion))
         {
             $this->id_ultima_gestion = sc_convert_encoding($this->id_ultima_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->id_ultima_gestion = str_replace('<', '&lt;', $this->id_ultima_gestion);
         $this->id_ultima_gestion = str_replace('>', '&gt;', $this->id_ultima_gestion);
         $this->Texto_tag .= "<td>" . $this->id_ultima_gestion . "</td>\r\n";
   }
   //----- usuario_creacion
   function NM_export_usuario_creacion()
   {
         $this->usuario_creacion = html_entity_decode($this->usuario_creacion, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->usuario_creacion = strip_tags($this->usuario_creacion);
         if (!NM_is_utf8($this->usuario_creacion))
         {
             $this->usuario_creacion = sc_convert_encoding($this->usuario_creacion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->usuario_creacion = str_replace('<', '&lt;', $this->usuario_creacion);
         $this->usuario_creacion = str_replace('>', '&gt;', $this->usuario_creacion);
         $this->Texto_tag .= "<td>" . $this->usuario_creacion . "</td>\r\n";
   }

   //----- 
   function grava_arquivo_rtf()
   {
      global $nm_lang, $doc_wrap;
      $rtf_f = fopen($this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo, "w");
      require_once($this->Ini->path_third      . "/rtf_new/document_generator/cl_xml2driver.php"); 
      $text_ok  =  "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\r\n"; 
      $text_ok .=  "<DOC config_file=\"" . $this->Ini->path_third . "/rtf_new/doc_config.inc\" >\r\n"; 
      $text_ok .=  $this->Texto_tag; 
      $text_ok .=  "</DOC>\r\n"; 
      $xml = new nDOCGEN($text_ok,"RTF"); 
      fwrite($rtf_f, $xml->get_result_file());
      fclose($rtf_f);
   }

   function nm_conv_data_db($dt_in, $form_in, $form_out)
   {
       $dt_out = $dt_in;
       if (strtoupper($form_in) == "DB_FORMAT")
       {
           if ($dt_out == "null" || $dt_out == "")
           {
               $dt_out = "";
               return $dt_out;
           }
           $form_in = "AAAA-MM-DD";
       }
       if (strtoupper($form_out) == "DB_FORMAT")
       {
           if (empty($dt_out))
           {
               $dt_out = "null";
               return $dt_out;
           }
           $form_out = "AAAA-MM-DD";
       }
       nm_conv_form_data($dt_out, $form_in, $form_out);
       return $dt_out;
   }
   //---- 
   function monta_html()
   {
      global $nm_url_saida, $nm_lang;
      include($this->Ini->path_btn . $this->Ini->Str_btn_grid);
      unset($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_pacientes']['rtf_file']);
      if (is_file($this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo))
      {
          $_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_pacientes']['rtf_file'] = $this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo;
      }
      $path_doc_md5 = md5($this->Ini->path_imag_temp . "/" . $this->Arquivo);
      $_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_pacientes'][$path_doc_md5][0] = $this->Ini->path_imag_temp . "/" . $this->Arquivo;
      $_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_pacientes'][$path_doc_md5][1] = $this->Tit_doc;
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
            "http://www.w3.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML<?php echo $_SESSION['scriptcase']['reg_conf']['html_dir'] ?>>
<HEAD>
 <TITLE><?php echo $this->Ini->Nm_lang['lang_othr_grid_titl'] ?> - bayer_pacientes :: RTF</TITLE>
 <META http-equiv="Content-Type" content="text/html; charset=<?php echo $_SESSION['scriptcase']['charset_html'] ?>" />
<?php
if ($_SESSION['scriptcase']['proc_mobile'])
{
?>
  <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<?php
}
?>
  <META http-equiv="Expires" content="Fri, Jan 01 1900 00:00:00 GMT"/>
  <META http-equiv="Last-Modified" content="<?php echo gmdate("D, d M Y H:i:s"); ?> GMT"/>
  <META http-equiv="Cache-Control" content="no-store, no-cache, must-revalidate"/>
  <META http-equiv="Cache-Control" content="post-check=0, pre-check=0"/>
  <META http-equiv="Pragma" content="no-cache"/>
  <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_all ?>_export.css" /> 
  <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_all ?>_export<?php echo $_SESSION['scriptcase']['reg_conf']['css_dir'] ?>.css" /> 
  <link rel="stylesheet" type="text/css" href="../_lib/buttons/<?php echo $this->Ini->Str_btn_css ?>" /> 
</HEAD>
<BODY class="scExportPage">
<?php echo $this->Ini->Ajax_result_set ?>
<table style="border-collapse: collapse; border-width: 0; height: 100%; width: 100%"><tr><td style="padding: 0; text-align: center; vertical-align: middle">
 <table class="scExportTable" align="center">
  <tr>
   <td class="scExportTitle" style="height: 25px">RTF</td>
  </tr>
  <tr>
   <td class="scExportLine" style="width: 100%">
    <table style="border-collapse: collapse; border-width: 0; width: 100%"><tr><td class="scExportLineFont" style="padding: 3px 0 0 0" id="idMessage">
    <?php echo $this->Ini->Nm_lang['lang_othr_file_msge'] ?>
    </td><td class="scExportLineFont" style="text-align:right; padding: 3px 0 0 0">
     <?php echo nmButtonOutput($this->arr_buttons, "bexportview", "document.Fview.submit()", "document.Fview.submit()", "idBtnView", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
     <?php echo nmButtonOutput($this->arr_buttons, "bdownload", "document.Fdown.submit()", "document.Fdown.submit()", "idBtnDown", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
     <?php echo nmButtonOutput($this->arr_buttons, "bvoltar", "document.F0.submit()", "document.F0.submit()", "idBtnBack", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
    </td></tr></table>
   </td>
  </tr>
 </table>
</td></tr></table>
<form name="Fview" method="get" action="<?php echo $this->Ini->path_imag_temp . "/" . $this->Arquivo ?>" target="_blank" style="display: none"> 
</form>
<form name="Fdown" method="get" action="grid_bayer_pacientes_download.php" target="_blank" style="display: none"> 
<input type="hidden" name="script_case_init" value="<?php echo NM_encode_input($this->Ini->sc_page); ?>"> 
<input type="hidden" name="nm_tit_doc" value="grid_bayer_pacientes"> 
<input type="hidden" name="nm_name_doc" value="<?php echo $path_doc_md5 ?>"> 
</form>
<FORM name="F0" method=post action="./"> 
<INPUT type="hidden" name="script_case_init" value="<?php echo NM_encode_input($this->Ini->sc_page); ?>"> 
<INPUT type="hidden" name="script_case_session" value="<?php echo NM_encode_input(session_id()); ?>"> 
<INPUT type="hidden" name="nmgp_opcao" value="volta_grid"> 
</FORM> 
</BODY>
</HTML>
<?php
   }
   function nm_gera_mask(&$nm_campo, $nm_mask)
   { 
      $trab_campo = $nm_campo;
      $trab_mask  = $nm_mask;
      $tam_campo  = strlen($nm_campo);
      $trab_saida = "";
      $mask_num = false;
      for ($x=0; $x < strlen($trab_mask); $x++)
      {
          if (substr($trab_mask, $x, 1) == "#")
          {
              $mask_num = true;
              break;
          }
      }
      if ($mask_num )
      {
          $ver_duas = explode(";", $trab_mask);
          if (isset($ver_duas[1]) && !empty($ver_duas[1]))
          {
              $cont1 = count(explode("#", $ver_duas[0])) - 1;
              $cont2 = count(explode("#", $ver_duas[1])) - 1;
              if ($cont2 >= $tam_campo)
              {
                  $trab_mask = $ver_duas[1];
              }
              else
              {
                  $trab_mask = $ver_duas[0];
              }
          }
          $tam_mask = strlen($trab_mask);
          $xdados = 0;
          for ($x=0; $x < $tam_mask; $x++)
          {
              if (substr($trab_mask, $x, 1) == "#" && $xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_campo, $xdados, 1);
                  $xdados++;
              }
              elseif ($xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_mask, $x, 1);
              }
          }
          if ($xdados < $tam_campo)
          {
              $trab_saida .= substr($trab_campo, $xdados);
          }
          $nm_campo = $trab_saida;
          return;
      }
      for ($ix = strlen($trab_mask); $ix > 0; $ix--)
      {
           $char_mask = substr($trab_mask, $ix - 1, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               $trab_saida = $char_mask . $trab_saida;
           }
           else
           {
               if ($tam_campo != 0)
               {
                   $trab_saida = substr($trab_campo, $tam_campo - 1, 1) . $trab_saida;
                   $tam_campo--;
               }
               else
               {
                   $trab_saida = "0" . $trab_saida;
               }
           }
      }
      if ($tam_campo != 0)
      {
          $trab_saida = substr($trab_campo, 0, $tam_campo) . $trab_saida;
          $trab_mask  = str_repeat("z", $tam_campo) . $trab_mask;
      }
   
      $iz = 0; 
      for ($ix = 0; $ix < strlen($trab_mask); $ix++)
      {
           $char_mask = substr($trab_mask, $ix, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               if ($char_mask == "." || $char_mask == ",")
               {
                   $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
               }
               else
               {
                   $iz++;
               }
           }
           elseif ($char_mask == "x" || substr($trab_saida, $iz, 1) != "0")
           {
               $ix = strlen($trab_mask) + 1;
           }
           else
           {
               $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
           }
      }
      $nm_campo = $trab_saida;
   } 
}

?>
