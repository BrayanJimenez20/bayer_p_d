<?php

class grid_bayer_gestiones_xml
{
   var $Db;
   var $Erro;
   var $Ini;
   var $Lookup;
   var $nm_data;

   var $Arquivo;
   var $Arquivo_view;
   var $Tit_doc;
   var $sc_proc_grid; 
   var $NM_cmp_hidden = array();

   //---- 
   function grid_bayer_gestiones_xml()
   {
      $this->nm_data   = new nm_data("es");
   }

   //---- 
   function monta_xml()
   {
      $this->inicializa_vars();
      $this->grava_arquivo();
      $this->monta_html();
   }

   //----- 
   function inicializa_vars()
   {
      global $nm_lang;
      $dir_raiz          = strrpos($_SERVER['PHP_SELF'],"/") ;  
      $dir_raiz          = substr($_SERVER['PHP_SELF'], 0, $dir_raiz + 1) ;  
      $this->nm_location = $this->Ini->sc_protocolo . $this->Ini->server . $dir_raiz; 
      $this->nm_data    = new nm_data("es");
      $this->Arquivo      = "sc_xml";
      $this->Arquivo     .= "_" . date("YmdHis") . "_" . rand(0, 1000);
      $this->Arquivo     .= "_grid_bayer_gestiones";
      $this->Arquivo_view = $this->Arquivo . "_view.xml";
      $this->Arquivo     .= ".xml";
      $this->Tit_doc      = "grid_bayer_gestiones.xml";
      $this->Grava_view   = false;
      if (strtolower($_SESSION['scriptcase']['charset']) != strtolower($_SESSION['scriptcase']['charset_html']))
      {
          $this->Grava_view = true;
      }
   }

   //----- 
   function grava_arquivo()
   {
      global $nm_lang;
      global
             $nm_nada, $nm_lang;

      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->sc_proc_grid = false; 
      $nm_raiz_img  = ""; 
      if (isset($_SESSION['scriptcase']['sc_apl_conf']['grid_bayer_gestiones']['field_display']) && !empty($_SESSION['scriptcase']['sc_apl_conf']['grid_bayer_gestiones']['field_display']))
      {
          foreach ($_SESSION['scriptcase']['sc_apl_conf']['grid_bayer_gestiones']['field_display'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_gestiones']['usr_cmp_sel']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_gestiones']['usr_cmp_sel']))
      {
          foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_gestiones']['usr_cmp_sel'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_gestiones']['php_cmp_sel']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_gestiones']['php_cmp_sel']))
      {
          foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_gestiones']['php_cmp_sel'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_gestiones']['campos_busca']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_gestiones']['campos_busca']))
      { 
          $Busca_temp = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_gestiones']['campos_busca'];
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
              $Busca_temp = NM_conv_charset($Busca_temp, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->id_gestion = $Busca_temp['id_gestion']; 
          $tmp_pos = strpos($this->id_gestion, "##@@");
          if ($tmp_pos !== false)
          {
              $this->id_gestion = substr($this->id_gestion, 0, $tmp_pos);
          }
          $this->id_gestion_2 = $Busca_temp['id_gestion_input_2']; 
          $this->motivo_comunicacion_gestion = $Busca_temp['motivo_comunicacion_gestion']; 
          $tmp_pos = strpos($this->motivo_comunicacion_gestion, "##@@");
          if ($tmp_pos !== false)
          {
              $this->motivo_comunicacion_gestion = substr($this->motivo_comunicacion_gestion, 0, $tmp_pos);
          }
          $this->medio_contacto_gestion = $Busca_temp['medio_contacto_gestion']; 
          $tmp_pos = strpos($this->medio_contacto_gestion, "##@@");
          if ($tmp_pos !== false)
          {
              $this->medio_contacto_gestion = substr($this->medio_contacto_gestion, 0, $tmp_pos);
          }
          $this->tipo_llamada_gestion = $Busca_temp['tipo_llamada_gestion']; 
          $tmp_pos = strpos($this->tipo_llamada_gestion, "##@@");
          if ($tmp_pos !== false)
          {
              $this->tipo_llamada_gestion = substr($this->tipo_llamada_gestion, 0, $tmp_pos);
          }
      } 
      $this->nm_field_dinamico = array();
      $this->nm_order_dinamico = array();
      $this->sc_where_orig   = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_gestiones']['where_orig'];
      $this->sc_where_atual  = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_gestiones']['where_pesq'];
      $this->sc_where_filtro = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_gestiones']['where_pesq_filtro'];
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_gestiones']['xml_name']))
      {
          $this->Arquivo = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_gestiones']['xml_name'];
          $this->Tit_doc = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_gestiones']['xml_name'];
          unset($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_gestiones']['xml_name']);
      }
      if (!$this->Grava_view)
      {
          $this->Arquivo_view = $this->Arquivo;
      }
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase))
      { 
          $nmgp_select = "SELECT ID_GESTION, MOTIVO_COMUNICACION_GESTION, MEDIO_CONTACTO_GESTION, TIPO_LLAMADA_GESTION, LOGRO_COMUNICACION_GESTION, MOTIVO_NO_COMUNICACION_GESTION, NUMERO_INTENTOS_GESTION, ESPERADO_GESTION, ESTADO_CTC_GESTION, DIFICULTAD_ACCESO_GESTION, TIPO_DIFICULTAD_GESTION, ENVIOS_GESTION, MEDICAMENTOS_GESTION, TIPO_ENVIO_GESTION, GENERA_SOLICITUD_GESTION, FECHA_PROXIMA_LLAMADA, MOTIVO_PROXIMA_LLAMADA, OBSERVACION_PROXIMA_LLAMADA, FECHA_RECLAMACION_GESTION, TIPO_ADJUNTO, CENTRO_DIAGNOSTICO, AUTOR_GESTION, NOTA, DESCRIPCION_COMUNICACION_GESTION, FECHA_PROGRAMADA_GESTION, USUARIO_ASIGANDO, ID_PACIENTE_FK2, FECHA_COMUNICACION, ESTADO_GESTION, AUTOR_MODIFICACION, FECHA_SUBIDO from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
      { 
          $nmgp_select = "SELECT ID_GESTION, MOTIVO_COMUNICACION_GESTION, MEDIO_CONTACTO_GESTION, TIPO_LLAMADA_GESTION, LOGRO_COMUNICACION_GESTION, MOTIVO_NO_COMUNICACION_GESTION, NUMERO_INTENTOS_GESTION, ESPERADO_GESTION, ESTADO_CTC_GESTION, DIFICULTAD_ACCESO_GESTION, TIPO_DIFICULTAD_GESTION, ENVIOS_GESTION, MEDICAMENTOS_GESTION, TIPO_ENVIO_GESTION, GENERA_SOLICITUD_GESTION, FECHA_PROXIMA_LLAMADA, MOTIVO_PROXIMA_LLAMADA, OBSERVACION_PROXIMA_LLAMADA, FECHA_RECLAMACION_GESTION, TIPO_ADJUNTO, CENTRO_DIAGNOSTICO, AUTOR_GESTION, NOTA, DESCRIPCION_COMUNICACION_GESTION, FECHA_PROGRAMADA_GESTION, USUARIO_ASIGANDO, ID_PACIENTE_FK2, FECHA_COMUNICACION, ESTADO_GESTION, AUTOR_MODIFICACION, FECHA_SUBIDO from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
      { 
       $nmgp_select = "SELECT ID_GESTION, MOTIVO_COMUNICACION_GESTION, MEDIO_CONTACTO_GESTION, TIPO_LLAMADA_GESTION, LOGRO_COMUNICACION_GESTION, MOTIVO_NO_COMUNICACION_GESTION, NUMERO_INTENTOS_GESTION, ESPERADO_GESTION, ESTADO_CTC_GESTION, DIFICULTAD_ACCESO_GESTION, TIPO_DIFICULTAD_GESTION, ENVIOS_GESTION, MEDICAMENTOS_GESTION, TIPO_ENVIO_GESTION, GENERA_SOLICITUD_GESTION, FECHA_PROXIMA_LLAMADA, MOTIVO_PROXIMA_LLAMADA, OBSERVACION_PROXIMA_LLAMADA, FECHA_RECLAMACION_GESTION, TIPO_ADJUNTO, CENTRO_DIAGNOSTICO, AUTOR_GESTION, NOTA, DESCRIPCION_COMUNICACION_GESTION, FECHA_PROGRAMADA_GESTION, USUARIO_ASIGANDO, ID_PACIENTE_FK2, FECHA_COMUNICACION, ESTADO_GESTION, AUTOR_MODIFICACION, FECHA_SUBIDO from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
      { 
          $nmgp_select = "SELECT ID_GESTION, MOTIVO_COMUNICACION_GESTION, MEDIO_CONTACTO_GESTION, TIPO_LLAMADA_GESTION, LOGRO_COMUNICACION_GESTION, MOTIVO_NO_COMUNICACION_GESTION, NUMERO_INTENTOS_GESTION, ESPERADO_GESTION, ESTADO_CTC_GESTION, DIFICULTAD_ACCESO_GESTION, TIPO_DIFICULTAD_GESTION, ENVIOS_GESTION, MEDICAMENTOS_GESTION, TIPO_ENVIO_GESTION, GENERA_SOLICITUD_GESTION, FECHA_PROXIMA_LLAMADA, MOTIVO_PROXIMA_LLAMADA, OBSERVACION_PROXIMA_LLAMADA, FECHA_RECLAMACION_GESTION, TIPO_ADJUNTO, CENTRO_DIAGNOSTICO, AUTOR_GESTION, NOTA, DESCRIPCION_COMUNICACION_GESTION, FECHA_PROGRAMADA_GESTION, USUARIO_ASIGANDO, ID_PACIENTE_FK2, FECHA_COMUNICACION, ESTADO_GESTION, AUTOR_MODIFICACION, FECHA_SUBIDO from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
      { 
          $nmgp_select = "SELECT ID_GESTION, MOTIVO_COMUNICACION_GESTION, MEDIO_CONTACTO_GESTION, TIPO_LLAMADA_GESTION, LOGRO_COMUNICACION_GESTION, MOTIVO_NO_COMUNICACION_GESTION, NUMERO_INTENTOS_GESTION, ESPERADO_GESTION, ESTADO_CTC_GESTION, DIFICULTAD_ACCESO_GESTION, TIPO_DIFICULTAD_GESTION, ENVIOS_GESTION, MEDICAMENTOS_GESTION, TIPO_ENVIO_GESTION, GENERA_SOLICITUD_GESTION, FECHA_PROXIMA_LLAMADA, MOTIVO_PROXIMA_LLAMADA, OBSERVACION_PROXIMA_LLAMADA, FECHA_RECLAMACION_GESTION, TIPO_ADJUNTO, CENTRO_DIAGNOSTICO, AUTOR_GESTION, NOTA, DESCRIPCION_COMUNICACION_GESTION, FECHA_PROGRAMADA_GESTION, USUARIO_ASIGANDO, ID_PACIENTE_FK2, FECHA_COMUNICACION, ESTADO_GESTION, AUTOR_MODIFICACION, FECHA_SUBIDO from " . $this->Ini->nm_tabela; 
      } 
      else 
      { 
          $nmgp_select = "SELECT ID_GESTION, MOTIVO_COMUNICACION_GESTION, MEDIO_CONTACTO_GESTION, TIPO_LLAMADA_GESTION, LOGRO_COMUNICACION_GESTION, MOTIVO_NO_COMUNICACION_GESTION, NUMERO_INTENTOS_GESTION, ESPERADO_GESTION, ESTADO_CTC_GESTION, DIFICULTAD_ACCESO_GESTION, TIPO_DIFICULTAD_GESTION, ENVIOS_GESTION, MEDICAMENTOS_GESTION, TIPO_ENVIO_GESTION, GENERA_SOLICITUD_GESTION, FECHA_PROXIMA_LLAMADA, MOTIVO_PROXIMA_LLAMADA, OBSERVACION_PROXIMA_LLAMADA, FECHA_RECLAMACION_GESTION, TIPO_ADJUNTO, CENTRO_DIAGNOSTICO, AUTOR_GESTION, NOTA, DESCRIPCION_COMUNICACION_GESTION, FECHA_PROGRAMADA_GESTION, USUARIO_ASIGANDO, ID_PACIENTE_FK2, FECHA_COMUNICACION, ESTADO_GESTION, AUTOR_MODIFICACION, FECHA_SUBIDO from " . $this->Ini->nm_tabela; 
      } 
      $nmgp_select .= " " . $_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_gestiones']['where_pesq'];
      $nmgp_order_by = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_gestiones']['order_grid'];
      $nmgp_select .= $nmgp_order_by; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nmgp_select;
      $rs = $this->Db->Execute($nmgp_select);
      if ($rs === false && !$rs->EOF && $GLOBALS["NM_ERRO_IBASE"] != 1)
      {
         $this->Erro->mensagem(__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg());
         exit;
      }

      $xml_charset = $_SESSION['scriptcase']['charset'];
      $xml_f = fopen($this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo, "w");
      fwrite($xml_f, "<?xml version=\"1.0\" encoding=\"$xml_charset\" ?>\r\n");
      fwrite($xml_f, "<root>\r\n");
      if ($this->Grava_view)
      {
          $xml_charset_v = $_SESSION['scriptcase']['charset_html'];
          $xml_v         = fopen($this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo_view, "w");
          fwrite($xml_v, "<?xml version=\"1.0\" encoding=\"$xml_charset_v\" ?>\r\n");
          fwrite($xml_v, "<root>\r\n");
      }
      while (!$rs->EOF)
      {
         $this->xml_registro = "<grid_bayer_gestiones";
         $this->id_gestion = $rs->fields[0] ;  
         $this->id_gestion = (string)$this->id_gestion;
         $this->motivo_comunicacion_gestion = $rs->fields[1] ;  
         $this->medio_contacto_gestion = $rs->fields[2] ;  
         $this->tipo_llamada_gestion = $rs->fields[3] ;  
         $this->logro_comunicacion_gestion = $rs->fields[4] ;  
         $this->motivo_no_comunicacion_gestion = $rs->fields[5] ;  
         $this->numero_intentos_gestion = $rs->fields[6] ;  
         $this->esperado_gestion = $rs->fields[7] ;  
         $this->estado_ctc_gestion = $rs->fields[8] ;  
         $this->dificultad_acceso_gestion = $rs->fields[9] ;  
         $this->tipo_dificultad_gestion = $rs->fields[10] ;  
         $this->envios_gestion = $rs->fields[11] ;  
         $this->medicamentos_gestion = $rs->fields[12] ;  
         $this->tipo_envio_gestion = $rs->fields[13] ;  
         $this->genera_solicitud_gestion = $rs->fields[14] ;  
         $this->fecha_proxima_llamada = $rs->fields[15] ;  
         $this->motivo_proxima_llamada = $rs->fields[16] ;  
         $this->observacion_proxima_llamada = $rs->fields[17] ;  
         $this->fecha_reclamacion_gestion = $rs->fields[18] ;  
         $this->tipo_adjunto = $rs->fields[19] ;  
         $this->centro_diagnostico = $rs->fields[20] ;  
         $this->autor_gestion = $rs->fields[21] ;  
         $this->nota = $rs->fields[22] ;  
         $this->descripcion_comunicacion_gestion = $rs->fields[23] ;  
         $this->fecha_programada_gestion = $rs->fields[24] ;  
         $this->usuario_asigando = $rs->fields[25] ;  
         $this->id_paciente_fk2 = $rs->fields[26] ;  
         $this->id_paciente_fk2 = (string)$this->id_paciente_fk2;
         $this->fecha_comunicacion = $rs->fields[27] ;  
         $this->estado_gestion = $rs->fields[28] ;  
         $this->autor_modificacion = $rs->fields[29] ;  
         $this->fecha_subido = $rs->fields[30] ;  
         $this->sc_proc_grid = true; 
         foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_gestiones']['field_order'] as $Cada_col)
         { 
            if (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off")
            { 
                $NM_func_exp = "NM_export_" . $Cada_col;
                $this->$NM_func_exp();
            } 
         } 
         $this->xml_registro .= " />\r\n";
         fwrite($xml_f, $this->xml_registro);
         if ($this->Grava_view)
         {
            fwrite($xml_v, $this->xml_registro);
         }
         $rs->MoveNext();
      }
      fwrite($xml_f, "</root>");
      fclose($xml_f);
      if ($this->Grava_view)
      {
         fwrite($xml_v, "</root>");
         fclose($xml_v);
      }

      $rs->Close();
   }
   //----- id_gestion
   function NM_export_id_gestion()
   {
         nmgp_Form_Num_Val($this->id_gestion, $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->id_gestion))
         {
             $this->id_gestion = sc_convert_encoding($this->id_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " id_gestion =\"" . $this->trata_dados($this->id_gestion) . "\"";
   }
   //----- motivo_comunicacion_gestion
   function NM_export_motivo_comunicacion_gestion()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->motivo_comunicacion_gestion))
         {
             $this->motivo_comunicacion_gestion = sc_convert_encoding($this->motivo_comunicacion_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " motivo_comunicacion_gestion =\"" . $this->trata_dados($this->motivo_comunicacion_gestion) . "\"";
   }
   //----- medio_contacto_gestion
   function NM_export_medio_contacto_gestion()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->medio_contacto_gestion))
         {
             $this->medio_contacto_gestion = sc_convert_encoding($this->medio_contacto_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " medio_contacto_gestion =\"" . $this->trata_dados($this->medio_contacto_gestion) . "\"";
   }
   //----- tipo_llamada_gestion
   function NM_export_tipo_llamada_gestion()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->tipo_llamada_gestion))
         {
             $this->tipo_llamada_gestion = sc_convert_encoding($this->tipo_llamada_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " tipo_llamada_gestion =\"" . $this->trata_dados($this->tipo_llamada_gestion) . "\"";
   }
   //----- logro_comunicacion_gestion
   function NM_export_logro_comunicacion_gestion()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->logro_comunicacion_gestion))
         {
             $this->logro_comunicacion_gestion = sc_convert_encoding($this->logro_comunicacion_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " logro_comunicacion_gestion =\"" . $this->trata_dados($this->logro_comunicacion_gestion) . "\"";
   }
   //----- motivo_no_comunicacion_gestion
   function NM_export_motivo_no_comunicacion_gestion()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->motivo_no_comunicacion_gestion))
         {
             $this->motivo_no_comunicacion_gestion = sc_convert_encoding($this->motivo_no_comunicacion_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " motivo_no_comunicacion_gestion =\"" . $this->trata_dados($this->motivo_no_comunicacion_gestion) . "\"";
   }
   //----- numero_intentos_gestion
   function NM_export_numero_intentos_gestion()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->numero_intentos_gestion))
         {
             $this->numero_intentos_gestion = sc_convert_encoding($this->numero_intentos_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " numero_intentos_gestion =\"" . $this->trata_dados($this->numero_intentos_gestion) . "\"";
   }
   //----- esperado_gestion
   function NM_export_esperado_gestion()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->esperado_gestion))
         {
             $this->esperado_gestion = sc_convert_encoding($this->esperado_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " esperado_gestion =\"" . $this->trata_dados($this->esperado_gestion) . "\"";
   }
   //----- estado_ctc_gestion
   function NM_export_estado_ctc_gestion()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->estado_ctc_gestion))
         {
             $this->estado_ctc_gestion = sc_convert_encoding($this->estado_ctc_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " estado_ctc_gestion =\"" . $this->trata_dados($this->estado_ctc_gestion) . "\"";
   }
   //----- dificultad_acceso_gestion
   function NM_export_dificultad_acceso_gestion()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->dificultad_acceso_gestion))
         {
             $this->dificultad_acceso_gestion = sc_convert_encoding($this->dificultad_acceso_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " dificultad_acceso_gestion =\"" . $this->trata_dados($this->dificultad_acceso_gestion) . "\"";
   }
   //----- tipo_dificultad_gestion
   function NM_export_tipo_dificultad_gestion()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->tipo_dificultad_gestion))
         {
             $this->tipo_dificultad_gestion = sc_convert_encoding($this->tipo_dificultad_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " tipo_dificultad_gestion =\"" . $this->trata_dados($this->tipo_dificultad_gestion) . "\"";
   }
   //----- envios_gestion
   function NM_export_envios_gestion()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->envios_gestion))
         {
             $this->envios_gestion = sc_convert_encoding($this->envios_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " envios_gestion =\"" . $this->trata_dados($this->envios_gestion) . "\"";
   }
   //----- medicamentos_gestion
   function NM_export_medicamentos_gestion()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->medicamentos_gestion))
         {
             $this->medicamentos_gestion = sc_convert_encoding($this->medicamentos_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " medicamentos_gestion =\"" . $this->trata_dados($this->medicamentos_gestion) . "\"";
   }
   //----- tipo_envio_gestion
   function NM_export_tipo_envio_gestion()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->tipo_envio_gestion))
         {
             $this->tipo_envio_gestion = sc_convert_encoding($this->tipo_envio_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " tipo_envio_gestion =\"" . $this->trata_dados($this->tipo_envio_gestion) . "\"";
   }
   //----- genera_solicitud_gestion
   function NM_export_genera_solicitud_gestion()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->genera_solicitud_gestion))
         {
             $this->genera_solicitud_gestion = sc_convert_encoding($this->genera_solicitud_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " genera_solicitud_gestion =\"" . $this->trata_dados($this->genera_solicitud_gestion) . "\"";
   }
   //----- fecha_proxima_llamada
   function NM_export_fecha_proxima_llamada()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->fecha_proxima_llamada))
         {
             $this->fecha_proxima_llamada = sc_convert_encoding($this->fecha_proxima_llamada, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " fecha_proxima_llamada =\"" . $this->trata_dados($this->fecha_proxima_llamada) . "\"";
   }
   //----- motivo_proxima_llamada
   function NM_export_motivo_proxima_llamada()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->motivo_proxima_llamada))
         {
             $this->motivo_proxima_llamada = sc_convert_encoding($this->motivo_proxima_llamada, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " motivo_proxima_llamada =\"" . $this->trata_dados($this->motivo_proxima_llamada) . "\"";
   }
   //----- observacion_proxima_llamada
   function NM_export_observacion_proxima_llamada()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->observacion_proxima_llamada))
         {
             $this->observacion_proxima_llamada = sc_convert_encoding($this->observacion_proxima_llamada, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " observacion_proxima_llamada =\"" . $this->trata_dados($this->observacion_proxima_llamada) . "\"";
   }
   //----- fecha_reclamacion_gestion
   function NM_export_fecha_reclamacion_gestion()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->fecha_reclamacion_gestion))
         {
             $this->fecha_reclamacion_gestion = sc_convert_encoding($this->fecha_reclamacion_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " fecha_reclamacion_gestion =\"" . $this->trata_dados($this->fecha_reclamacion_gestion) . "\"";
   }
   //----- tipo_adjunto
   function NM_export_tipo_adjunto()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->tipo_adjunto))
         {
             $this->tipo_adjunto = sc_convert_encoding($this->tipo_adjunto, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " tipo_adjunto =\"" . $this->trata_dados($this->tipo_adjunto) . "\"";
   }
   //----- centro_diagnostico
   function NM_export_centro_diagnostico()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->centro_diagnostico))
         {
             $this->centro_diagnostico = sc_convert_encoding($this->centro_diagnostico, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " centro_diagnostico =\"" . $this->trata_dados($this->centro_diagnostico) . "\"";
   }
   //----- autor_gestion
   function NM_export_autor_gestion()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->autor_gestion))
         {
             $this->autor_gestion = sc_convert_encoding($this->autor_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " autor_gestion =\"" . $this->trata_dados($this->autor_gestion) . "\"";
   }
   //----- nota
   function NM_export_nota()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->nota))
         {
             $this->nota = sc_convert_encoding($this->nota, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " nota =\"" . $this->trata_dados($this->nota) . "\"";
   }
   //----- descripcion_comunicacion_gestion
   function NM_export_descripcion_comunicacion_gestion()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->descripcion_comunicacion_gestion))
         {
             $this->descripcion_comunicacion_gestion = sc_convert_encoding($this->descripcion_comunicacion_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " descripcion_comunicacion_gestion =\"" . $this->trata_dados($this->descripcion_comunicacion_gestion) . "\"";
   }
   //----- fecha_programada_gestion
   function NM_export_fecha_programada_gestion()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->fecha_programada_gestion))
         {
             $this->fecha_programada_gestion = sc_convert_encoding($this->fecha_programada_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " fecha_programada_gestion =\"" . $this->trata_dados($this->fecha_programada_gestion) . "\"";
   }
   //----- usuario_asigando
   function NM_export_usuario_asigando()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->usuario_asigando))
         {
             $this->usuario_asigando = sc_convert_encoding($this->usuario_asigando, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " usuario_asigando =\"" . $this->trata_dados($this->usuario_asigando) . "\"";
   }
   //----- id_paciente_fk2
   function NM_export_id_paciente_fk2()
   {
         nmgp_Form_Num_Val($this->id_paciente_fk2, $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->id_paciente_fk2))
         {
             $this->id_paciente_fk2 = sc_convert_encoding($this->id_paciente_fk2, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " id_paciente_fk2 =\"" . $this->trata_dados($this->id_paciente_fk2) . "\"";
   }
   //----- fecha_comunicacion
   function NM_export_fecha_comunicacion()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->fecha_comunicacion))
         {
             $this->fecha_comunicacion = sc_convert_encoding($this->fecha_comunicacion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " fecha_comunicacion =\"" . $this->trata_dados($this->fecha_comunicacion) . "\"";
   }
   //----- estado_gestion
   function NM_export_estado_gestion()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->estado_gestion))
         {
             $this->estado_gestion = sc_convert_encoding($this->estado_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " estado_gestion =\"" . $this->trata_dados($this->estado_gestion) . "\"";
   }
   //----- autor_modificacion
   function NM_export_autor_modificacion()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->autor_modificacion))
         {
             $this->autor_modificacion = sc_convert_encoding($this->autor_modificacion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " autor_modificacion =\"" . $this->trata_dados($this->autor_modificacion) . "\"";
   }
   //----- fecha_subido
   function NM_export_fecha_subido()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->fecha_subido))
         {
             $this->fecha_subido = sc_convert_encoding($this->fecha_subido, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " fecha_subido =\"" . $this->trata_dados($this->fecha_subido) . "\"";
   }

   //----- 
   function trata_dados($conteudo)
   {
      $str_temp =  $conteudo;
      $str_temp =  str_replace("<br />", "",  $str_temp);
      $str_temp =  str_replace("&", "&amp;",  $str_temp);
      $str_temp =  str_replace("<", "&lt;",   $str_temp);
      $str_temp =  str_replace(">", "&gt;",   $str_temp);
      $str_temp =  str_replace("'", "&apos;", $str_temp);
      $str_temp =  str_replace('"', "&quot;",  $str_temp);
      $str_temp =  str_replace('(', "_",  $str_temp);
      $str_temp =  str_replace(')', "",  $str_temp);
      return ($str_temp);
   }

   function nm_conv_data_db($dt_in, $form_in, $form_out)
   {
       $dt_out = $dt_in;
       if (strtoupper($form_in) == "DB_FORMAT")
       {
           if ($dt_out == "null" || $dt_out == "")
           {
               $dt_out = "";
               return $dt_out;
           }
           $form_in = "AAAA-MM-DD";
       }
       if (strtoupper($form_out) == "DB_FORMAT")
       {
           if (empty($dt_out))
           {
               $dt_out = "null";
               return $dt_out;
           }
           $form_out = "AAAA-MM-DD";
       }
       nm_conv_form_data($dt_out, $form_in, $form_out);
       return $dt_out;
   }
   //---- 
   function monta_html()
   {
      global $nm_url_saida, $nm_lang;
      include($this->Ini->path_btn . $this->Ini->Str_btn_grid);
      unset($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_gestiones']['xml_file']);
      if (is_file($this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo))
      {
          $_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_gestiones']['xml_file'] = $this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo;
      }
      $path_doc_md5 = md5($this->Ini->path_imag_temp . "/" . $this->Arquivo);
      $_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_gestiones'][$path_doc_md5][0] = $this->Ini->path_imag_temp . "/" . $this->Arquivo;
      $_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_gestiones'][$path_doc_md5][1] = $this->Tit_doc;
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
            "http://www.w3.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML<?php echo $_SESSION['scriptcase']['reg_conf']['html_dir'] ?>>
<HEAD>
 <TITLE><?php echo $this->Ini->Nm_lang['lang_othr_grid_titl'] ?> - bayer_gestiones :: XML</TITLE>
 <META http-equiv="Content-Type" content="text/html; charset=<?php echo $_SESSION['scriptcase']['charset_html'] ?>" />
<?php
if ($_SESSION['scriptcase']['proc_mobile'])
{
?>
  <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<?php
}
?>
 <META http-equiv="Expires" content="Fri, Jan 01 1900 00:00:00 GMT"/>
 <META http-equiv="Last-Modified" content="<?php echo gmdate("D, d M Y H:i:s"); ?> GMT"/>
 <META http-equiv="Cache-Control" content="no-store, no-cache, must-revalidate"/>
 <META http-equiv="Cache-Control" content="post-check=0, pre-check=0"/>
 <META http-equiv="Pragma" content="no-cache"/>
  <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_all ?>_export.css" /> 
  <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_all ?>_export<?php echo $_SESSION['scriptcase']['reg_conf']['css_dir'] ?>.css" /> 
  <link rel="stylesheet" type="text/css" href="../_lib/buttons/<?php echo $this->Ini->Str_btn_css ?>" /> 
</HEAD>
<BODY class="scExportPage">
<?php echo $this->Ini->Ajax_result_set ?>
<table style="border-collapse: collapse; border-width: 0; height: 100%; width: 100%"><tr><td style="padding: 0; text-align: center; vertical-align: middle">
 <table class="scExportTable" align="center">
  <tr>
   <td class="scExportTitle" style="height: 25px">XML</td>
  </tr>
  <tr>
   <td class="scExportLine" style="width: 100%">
    <table style="border-collapse: collapse; border-width: 0; width: 100%"><tr><td class="scExportLineFont" style="padding: 3px 0 0 0" id="idMessage">
    <?php echo $this->Ini->Nm_lang['lang_othr_file_msge'] ?>
    </td><td class="scExportLineFont" style="text-align:right; padding: 3px 0 0 0">
     <?php echo nmButtonOutput($this->arr_buttons, "bexportview", "document.Fview.submit()", "document.Fview.submit()", "idBtnView", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
     <?php echo nmButtonOutput($this->arr_buttons, "bdownload", "document.Fdown.submit()", "document.Fdown.submit()", "idBtnDown", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
     <?php echo nmButtonOutput($this->arr_buttons, "bvoltar", "document.F0.submit()", "document.F0.submit()", "idBtnBack", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
    </td></tr></table>
   </td>
  </tr>
 </table>
</td></tr></table>
<form name="Fview" method="get" action="<?php echo $this->Ini->path_imag_temp . "/" . $this->Arquivo_view ?>" target="_blank" style="display: none"> 
</form>
<form name="Fdown" method="get" action="grid_bayer_gestiones_download.php" target="_blank" style="display: none"> 
<input type="hidden" name="script_case_init" value="<?php echo NM_encode_input($this->Ini->sc_page); ?>"> 
<input type="hidden" name="nm_tit_doc" value="grid_bayer_gestiones"> 
<input type="hidden" name="nm_name_doc" value="<?php echo $path_doc_md5 ?>"> 
</form>
<FORM name="F0" method=post action="./" style="display: none"> 
<INPUT type="hidden" name="script_case_init" value="<?php echo NM_encode_input($this->Ini->sc_page); ?>"> 
<INPUT type="hidden" name="script_case_session" value="<?php echo NM_encode_input(session_id()); ?>"> 
<INPUT type="hidden" name="nmgp_opcao" value="volta_grid"> 
</FORM> 
</BODY>
</HTML>
<?php
   }
   function nm_gera_mask(&$nm_campo, $nm_mask)
   { 
      $trab_campo = $nm_campo;
      $trab_mask  = $nm_mask;
      $tam_campo  = strlen($nm_campo);
      $trab_saida = "";
      $mask_num = false;
      for ($x=0; $x < strlen($trab_mask); $x++)
      {
          if (substr($trab_mask, $x, 1) == "#")
          {
              $mask_num = true;
              break;
          }
      }
      if ($mask_num )
      {
          $ver_duas = explode(";", $trab_mask);
          if (isset($ver_duas[1]) && !empty($ver_duas[1]))
          {
              $cont1 = count(explode("#", $ver_duas[0])) - 1;
              $cont2 = count(explode("#", $ver_duas[1])) - 1;
              if ($cont2 >= $tam_campo)
              {
                  $trab_mask = $ver_duas[1];
              }
              else
              {
                  $trab_mask = $ver_duas[0];
              }
          }
          $tam_mask = strlen($trab_mask);
          $xdados = 0;
          for ($x=0; $x < $tam_mask; $x++)
          {
              if (substr($trab_mask, $x, 1) == "#" && $xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_campo, $xdados, 1);
                  $xdados++;
              }
              elseif ($xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_mask, $x, 1);
              }
          }
          if ($xdados < $tam_campo)
          {
              $trab_saida .= substr($trab_campo, $xdados);
          }
          $nm_campo = $trab_saida;
          return;
      }
      for ($ix = strlen($trab_mask); $ix > 0; $ix--)
      {
           $char_mask = substr($trab_mask, $ix - 1, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               $trab_saida = $char_mask . $trab_saida;
           }
           else
           {
               if ($tam_campo != 0)
               {
                   $trab_saida = substr($trab_campo, $tam_campo - 1, 1) . $trab_saida;
                   $tam_campo--;
               }
               else
               {
                   $trab_saida = "0" . $trab_saida;
               }
           }
      }
      if ($tam_campo != 0)
      {
          $trab_saida = substr($trab_campo, 0, $tam_campo) . $trab_saida;
          $trab_mask  = str_repeat("z", $tam_campo) . $trab_mask;
      }
   
      $iz = 0; 
      for ($ix = 0; $ix < strlen($trab_mask); $ix++)
      {
           $char_mask = substr($trab_mask, $ix, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               if ($char_mask == "." || $char_mask == ",")
               {
                   $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
               }
               else
               {
                   $iz++;
               }
           }
           elseif ($char_mask == "x" || substr($trab_saida, $iz, 1) != "0")
           {
               $ix = strlen($trab_mask) + 1;
           }
           else
           {
               $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
           }
      }
      $nm_campo = $trab_saida;
   } 
}

?>
