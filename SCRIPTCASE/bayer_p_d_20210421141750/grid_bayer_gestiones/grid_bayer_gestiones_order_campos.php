<?php
   include_once('grid_bayer_gestiones_session.php');
   session_start();
   if (!function_exists("NM_is_utf8"))
   {
       include_once("../_lib/lib/php/nm_utf8.php");
   }
    $Ord_Cmp = new grid_bayer_gestiones_Ord_cmp(); 
    $Ord_Cmp->Ord_cmp_init();
   
class grid_bayer_gestiones_Ord_cmp
{
function Ord_cmp_init()
{
  global $sc_init, $path_img, $path_btn, $tab_ger_campos, $tab_def_campos, $tab_converte, $tab_labels, $embbed, $tbar_pos, $_POST, $_GET;
   if (isset($_POST['script_case_init']))
   {
       $sc_init    = $_POST['script_case_init'];
       $path_img   = $_POST['path_img'];
       $path_btn   = $_POST['path_btn'];
       $use_alias  = (isset($_POST['use_alias']))  ? $_POST['use_alias']  : "S";
       $fsel_ok    = (isset($_POST['fsel_ok']))    ? $_POST['fsel_ok']    : "";
       $campos_sel = (isset($_POST['campos_sel'])) ? $_POST['campos_sel'] : "";
       $sel_regra  = (isset($_POST['sel_regra']))  ? $_POST['sel_regra']  : "";
       $embbed     = isset($_POST['embbed_groupby']) && 'Y' == $_POST['embbed_groupby'];
       $tbar_pos   = isset($_POST['toolbar_pos']) ? $_POST['toolbar_pos'] : '';
   }
   elseif (isset($_GET['script_case_init']))
   {
       $sc_init    = $_GET['script_case_init'];
       $path_img   = $_GET['path_img'];
       $path_btn   = $_GET['path_btn'];
       $use_alias  = (isset($_GET['use_alias']))  ? $_GET['use_alias']  : "S";
       $fsel_ok    = (isset($_GET['fsel_ok']))    ? $_GET['fsel_ok']    : "";
       $campos_sel = (isset($_GET['campos_sel'])) ? $_GET['campos_sel'] : "";
       $sel_regra  = (isset($_GET['sel_regra']))  ? $_GET['sel_regra']  : "";
       $embbed     = isset($_GET['embbed_groupby']) && 'Y' == $_GET['embbed_groupby'];
       $tbar_pos   = isset($_GET['toolbar_pos']) ? $_GET['toolbar_pos'] : '';
   }
   $STR_lang    = (isset($_SESSION['scriptcase']['str_lang']) && !empty($_SESSION['scriptcase']['str_lang'])) ? $_SESSION['scriptcase']['str_lang'] : "es";
   $NM_arq_lang = "../_lib/lang/" . $STR_lang . ".lang.php";
   $this->Nm_lang = array();
   if (is_file($NM_arq_lang))
   {
       include_once($NM_arq_lang);
   }
   
   $tab_ger_campos = array();
   $tab_def_campos = array();
   $tab_labels     = array();
   $tab_ger_campos['id_gestion'] = "on";
   $tab_def_campos['id_gestion'] = "ID_GESTION";
   $tab_converte["ID_GESTION"]   = "id_gestion";
   $tab_labels["id_gestion"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["id_gestion"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["id_gestion"] : "ID GESTION";
   $tab_ger_campos['motivo_comunicacion_gestion'] = "on";
   $tab_def_campos['motivo_comunicacion_gestion'] = "MOTIVO_COMUNICACION_GESTION";
   $tab_converte["MOTIVO_COMUNICACION_GESTION"]   = "motivo_comunicacion_gestion";
   $tab_labels["motivo_comunicacion_gestion"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["motivo_comunicacion_gestion"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["motivo_comunicacion_gestion"] : "MOTIVO COMUNICACION GESTION";
   $tab_ger_campos['medio_contacto_gestion'] = "on";
   $tab_def_campos['medio_contacto_gestion'] = "MEDIO_CONTACTO_GESTION";
   $tab_converte["MEDIO_CONTACTO_GESTION"]   = "medio_contacto_gestion";
   $tab_labels["medio_contacto_gestion"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["medio_contacto_gestion"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["medio_contacto_gestion"] : "MEDIO CONTACTO GESTION";
   $tab_ger_campos['tipo_llamada_gestion'] = "on";
   $tab_def_campos['tipo_llamada_gestion'] = "TIPO_LLAMADA_GESTION";
   $tab_converte["TIPO_LLAMADA_GESTION"]   = "tipo_llamada_gestion";
   $tab_labels["tipo_llamada_gestion"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["tipo_llamada_gestion"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["tipo_llamada_gestion"] : "TIPO LLAMADA GESTION";
   $tab_ger_campos['logro_comunicacion_gestion'] = "on";
   $tab_def_campos['logro_comunicacion_gestion'] = "LOGRO_COMUNICACION_GESTION";
   $tab_converte["LOGRO_COMUNICACION_GESTION"]   = "logro_comunicacion_gestion";
   $tab_labels["logro_comunicacion_gestion"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["logro_comunicacion_gestion"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["logro_comunicacion_gestion"] : "LOGRO COMUNICACION GESTION";
   $tab_ger_campos['motivo_no_comunicacion_gestion'] = "on";
   $tab_def_campos['motivo_no_comunicacion_gestion'] = "MOTIVO_NO_COMUNICACION_GESTION";
   $tab_converte["MOTIVO_NO_COMUNICACION_GESTION"]   = "motivo_no_comunicacion_gestion";
   $tab_labels["motivo_no_comunicacion_gestion"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["motivo_no_comunicacion_gestion"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["motivo_no_comunicacion_gestion"] : "MOTIVO NO COMUNICACION GESTION";
   $tab_ger_campos['numero_intentos_gestion'] = "on";
   $tab_def_campos['numero_intentos_gestion'] = "NUMERO_INTENTOS_GESTION";
   $tab_converte["NUMERO_INTENTOS_GESTION"]   = "numero_intentos_gestion";
   $tab_labels["numero_intentos_gestion"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["numero_intentos_gestion"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["numero_intentos_gestion"] : "NUMERO INTENTOS GESTION";
   $tab_ger_campos['esperado_gestion'] = "on";
   $tab_def_campos['esperado_gestion'] = "ESPERADO_GESTION";
   $tab_converte["ESPERADO_GESTION"]   = "esperado_gestion";
   $tab_labels["esperado_gestion"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["esperado_gestion"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["esperado_gestion"] : "ESPERADO GESTION";
   $tab_ger_campos['estado_ctc_gestion'] = "on";
   $tab_def_campos['estado_ctc_gestion'] = "ESTADO_CTC_GESTION";
   $tab_converte["ESTADO_CTC_GESTION"]   = "estado_ctc_gestion";
   $tab_labels["estado_ctc_gestion"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["estado_ctc_gestion"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["estado_ctc_gestion"] : "ESTADO CTC GESTION";
   $tab_ger_campos['dificultad_acceso_gestion'] = "on";
   $tab_def_campos['dificultad_acceso_gestion'] = "DIFICULTAD_ACCESO_GESTION";
   $tab_converte["DIFICULTAD_ACCESO_GESTION"]   = "dificultad_acceso_gestion";
   $tab_labels["dificultad_acceso_gestion"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["dificultad_acceso_gestion"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["dificultad_acceso_gestion"] : "DIFICULTAD ACCESO GESTION";
   $tab_ger_campos['tipo_dificultad_gestion'] = "on";
   $tab_def_campos['tipo_dificultad_gestion'] = "TIPO_DIFICULTAD_GESTION";
   $tab_converte["TIPO_DIFICULTAD_GESTION"]   = "tipo_dificultad_gestion";
   $tab_labels["tipo_dificultad_gestion"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["tipo_dificultad_gestion"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["tipo_dificultad_gestion"] : "TIPO DIFICULTAD GESTION";
   $tab_ger_campos['envios_gestion'] = "on";
   $tab_def_campos['envios_gestion'] = "ENVIOS_GESTION";
   $tab_converte["ENVIOS_GESTION"]   = "envios_gestion";
   $tab_labels["envios_gestion"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["envios_gestion"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["envios_gestion"] : "ENVIOS GESTION";
   $tab_ger_campos['medicamentos_gestion'] = "on";
   $tab_def_campos['medicamentos_gestion'] = "MEDICAMENTOS_GESTION";
   $tab_converte["MEDICAMENTOS_GESTION"]   = "medicamentos_gestion";
   $tab_labels["medicamentos_gestion"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["medicamentos_gestion"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["medicamentos_gestion"] : "MEDICAMENTOS GESTION";
   $tab_ger_campos['tipo_envio_gestion'] = "on";
   $tab_def_campos['tipo_envio_gestion'] = "TIPO_ENVIO_GESTION";
   $tab_converte["TIPO_ENVIO_GESTION"]   = "tipo_envio_gestion";
   $tab_labels["tipo_envio_gestion"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["tipo_envio_gestion"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["tipo_envio_gestion"] : "TIPO ENVIO GESTION";
   $tab_ger_campos['genera_solicitud_gestion'] = "on";
   $tab_def_campos['genera_solicitud_gestion'] = "GENERA_SOLICITUD_GESTION";
   $tab_converte["GENERA_SOLICITUD_GESTION"]   = "genera_solicitud_gestion";
   $tab_labels["genera_solicitud_gestion"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["genera_solicitud_gestion"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["genera_solicitud_gestion"] : "GENERA SOLICITUD GESTION";
   $tab_ger_campos['fecha_proxima_llamada'] = "on";
   $tab_def_campos['fecha_proxima_llamada'] = "FECHA_PROXIMA_LLAMADA";
   $tab_converte["FECHA_PROXIMA_LLAMADA"]   = "fecha_proxima_llamada";
   $tab_labels["fecha_proxima_llamada"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["fecha_proxima_llamada"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["fecha_proxima_llamada"] : "FECHA PROXIMA LLAMADA";
   $tab_ger_campos['motivo_proxima_llamada'] = "on";
   $tab_def_campos['motivo_proxima_llamada'] = "MOTIVO_PROXIMA_LLAMADA";
   $tab_converte["MOTIVO_PROXIMA_LLAMADA"]   = "motivo_proxima_llamada";
   $tab_labels["motivo_proxima_llamada"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["motivo_proxima_llamada"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["motivo_proxima_llamada"] : "MOTIVO PROXIMA LLAMADA";
   $tab_ger_campos['observacion_proxima_llamada'] = "on";
   $tab_def_campos['observacion_proxima_llamada'] = "OBSERVACION_PROXIMA_LLAMADA";
   $tab_converte["OBSERVACION_PROXIMA_LLAMADA"]   = "observacion_proxima_llamada";
   $tab_labels["observacion_proxima_llamada"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["observacion_proxima_llamada"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["observacion_proxima_llamada"] : "OBSERVACION PROXIMA LLAMADA";
   $tab_ger_campos['fecha_reclamacion_gestion'] = "on";
   $tab_def_campos['fecha_reclamacion_gestion'] = "FECHA_RECLAMACION_GESTION";
   $tab_converte["FECHA_RECLAMACION_GESTION"]   = "fecha_reclamacion_gestion";
   $tab_labels["fecha_reclamacion_gestion"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["fecha_reclamacion_gestion"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["fecha_reclamacion_gestion"] : "FECHA RECLAMACION GESTION";
   $tab_ger_campos['tipo_adjunto'] = "on";
   $tab_def_campos['tipo_adjunto'] = "TIPO_ADJUNTO";
   $tab_converte["TIPO_ADJUNTO"]   = "tipo_adjunto";
   $tab_labels["tipo_adjunto"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["tipo_adjunto"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["tipo_adjunto"] : "TIPO ADJUNTO";
   $tab_ger_campos['centro_diagnostico'] = "on";
   $tab_def_campos['centro_diagnostico'] = "CENTRO_DIAGNOSTICO";
   $tab_converte["CENTRO_DIAGNOSTICO"]   = "centro_diagnostico";
   $tab_labels["centro_diagnostico"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["centro_diagnostico"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["centro_diagnostico"] : "CENTRO DIAGNOSTICO";
   $tab_ger_campos['autor_gestion'] = "on";
   $tab_def_campos['autor_gestion'] = "AUTOR_GESTION";
   $tab_converte["AUTOR_GESTION"]   = "autor_gestion";
   $tab_labels["autor_gestion"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["autor_gestion"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["autor_gestion"] : "AUTOR GESTION";
   $tab_ger_campos['nota'] = "on";
   $tab_def_campos['nota'] = "NOTA";
   $tab_converte["NOTA"]   = "nota";
   $tab_labels["nota"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["nota"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["nota"] : "NOTA";
   $tab_ger_campos['descripcion_comunicacion_gestion'] = "on";
   $tab_def_campos['descripcion_comunicacion_gestion'] = "DESCRIPCION_COMUNICACION_GESTION";
   $tab_converte["DESCRIPCION_COMUNICACION_GESTION"]   = "descripcion_comunicacion_gestion";
   $tab_labels["descripcion_comunicacion_gestion"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["descripcion_comunicacion_gestion"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["descripcion_comunicacion_gestion"] : "DESCRIPCION COMUNICACION GESTION";
   $tab_ger_campos['fecha_programada_gestion'] = "on";
   $tab_def_campos['fecha_programada_gestion'] = "FECHA_PROGRAMADA_GESTION";
   $tab_converte["FECHA_PROGRAMADA_GESTION"]   = "fecha_programada_gestion";
   $tab_labels["fecha_programada_gestion"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["fecha_programada_gestion"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["fecha_programada_gestion"] : "FECHA PROGRAMADA GESTION";
   $tab_ger_campos['usuario_asigando'] = "on";
   $tab_def_campos['usuario_asigando'] = "USUARIO_ASIGANDO";
   $tab_converte["USUARIO_ASIGANDO"]   = "usuario_asigando";
   $tab_labels["usuario_asigando"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["usuario_asigando"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["usuario_asigando"] : "USUARIO ASIGANDO";
   $tab_ger_campos['id_paciente_fk2'] = "on";
   $tab_def_campos['id_paciente_fk2'] = "ID_PACIENTE_FK2";
   $tab_converte["ID_PACIENTE_FK2"]   = "id_paciente_fk2";
   $tab_labels["id_paciente_fk2"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["id_paciente_fk2"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["id_paciente_fk2"] : "ID PACIENTE FK2";
   $tab_ger_campos['fecha_comunicacion'] = "on";
   $tab_def_campos['fecha_comunicacion'] = "FECHA_COMUNICACION";
   $tab_converte["FECHA_COMUNICACION"]   = "fecha_comunicacion";
   $tab_labels["fecha_comunicacion"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["fecha_comunicacion"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["fecha_comunicacion"] : "FECHA COMUNICACION";
   $tab_ger_campos['estado_gestion'] = "on";
   $tab_def_campos['estado_gestion'] = "ESTADO_GESTION";
   $tab_converte["ESTADO_GESTION"]   = "estado_gestion";
   $tab_labels["estado_gestion"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["estado_gestion"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["estado_gestion"] : "ESTADO GESTION";
   $tab_ger_campos['autor_modificacion'] = "on";
   $tab_def_campos['autor_modificacion'] = "AUTOR_MODIFICACION";
   $tab_converte["AUTOR_MODIFICACION"]   = "autor_modificacion";
   $tab_labels["autor_modificacion"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["autor_modificacion"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["autor_modificacion"] : "AUTOR MODIFICACION";
   $tab_ger_campos['fecha_subido'] = "on";
   $tab_def_campos['fecha_subido'] = "FECHA_SUBIDO";
   $tab_converte["FECHA_SUBIDO"]   = "fecha_subido";
   $tab_labels["fecha_subido"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["fecha_subido"])) ? $_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['labels']["fecha_subido"] : "FECHA SUBIDO";
   if (isset($_SESSION['scriptcase']['sc_apl_conf']['grid_bayer_gestiones']['field_display']) && !empty($_SESSION['scriptcase']['sc_apl_conf']['grid_bayer_gestiones']['field_display']))
   {
       foreach ($_SESSION['scriptcase']['sc_apl_conf']['grid_bayer_gestiones']['field_display'] as $NM_cada_field => $NM_cada_opc)
       {
           if ($NM_cada_opc == "off")
           {
              $tab_ger_campos[$NM_cada_field] = "none";
           }
       }
   }
   if (isset($_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['php_cmp_sel']) && !empty($_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['php_cmp_sel']))
   {
       foreach ($_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['php_cmp_sel'] as $NM_cada_field => $NM_cada_opc)
       {
           if ($NM_cada_opc == "off")
           {
              $tab_ger_campos[$NM_cada_field] = "none";
           }
       }
   }
   if (!isset($_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['ordem_select']))
   {
       $_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['ordem_select'] = array();
   }
   
   if ($fsel_ok == "cmp")
   {
       $this->Sel_processa_out_sel($campos_sel);
   }
   else
   {
       if ($embbed)
       {
           ob_start();
           $this->Sel_processa_form();
           $Temp = ob_get_clean();
           echo NM_charset_to_utf8($Temp);
       }
       else
       {
           $this->Sel_processa_form();
       }
   }
   exit;
   
}
function Sel_processa_out_sel($campos_sel)
{
   global $tab_ger_campos, $sc_init, $tab_def_campos, $tab_converte, $embbed;
   $arr_temp = array();
   $campos_sel = explode("@?@", $campos_sel);
   $_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['ordem_select'] = array();
   $_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['ordem_grid']   = "";
   $_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['ordem_cmp']    = "";
   foreach ($campos_sel as $campo_sort)
   {
       $ordem = (substr($campo_sort, 0, 1) == "+") ? "asc" : "desc";
       $campo = substr($campo_sort, 1);
       if (isset($tab_converte[$campo]))
       {
           $_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['ordem_select'][$campo] = $ordem;
       }
   }
?>
    <script language="javascript"> 
<?php
   if (!$embbed)
   {
?>
      self.parent.tb_remove(); 
      parent.nm_gp_submit_ajax('inicio', ''); 
<?php
   }
   else
   {
?>
      nm_gp_submit_ajax('inicio', ''); 
<?php
   }
?>
   </script>
<?php
}
   
function Sel_processa_form()
{
  global $sc_init, $path_img, $path_btn, $tab_ger_campos, $tab_def_campos, $tab_converte, $tab_labels, $embbed, $tbar_pos;
   $size = 10;
   $_SESSION['scriptcase']['charset']  = (isset($this->Nm_lang['Nm_charset']) && !empty($this->Nm_lang['Nm_charset'])) ? $this->Nm_lang['Nm_charset'] : "ISO-8859-1";
   foreach ($this->Nm_lang as $ind => $dados)
   {
      if ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($ind))
      {
          $ind = sc_convert_encoding($ind, $_SESSION['scriptcase']['charset'], "UTF-8");
          $this->Nm_lang[$ind] = $dados;
      }
      if ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($dados))
      {
          $this->Nm_lang[$ind] = sc_convert_encoding($dados, $_SESSION['scriptcase']['charset'], "UTF-8");
      }
   }
   $str_schema_all = (isset($_SESSION['scriptcase']['str_schema_all']) && !empty($_SESSION['scriptcase']['str_schema_all'])) ? $_SESSION['scriptcase']['str_schema_all'] : "Sc5_Green/Sc5_Green";
   include("../_lib/css/" . $str_schema_all . "_grid.php");
   $Str_btn_grid = trim($str_button) . "/" . trim($str_button) . $_SESSION['scriptcase']['reg_conf']['css_dir'] . ".php";
   include("../_lib/buttons/" . $Str_btn_grid);
   if (!function_exists("nmButtonOutput"))
   {
       include_once("../_lib/lib/php/nm_gp_config_btn.php");
   }
   if (!$embbed)
   {
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
            "http://www.w3.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML<?php echo $_SESSION['scriptcase']['reg_conf']['html_dir'] ?>>
<HEAD>
 <TITLE><?php echo $this->Nm_lang['lang_othr_grid_titl'] ?> - bayer_gestiones</TITLE>
 <META http-equiv="Content-Type" content="text/html; charset=<?php echo $_SESSION['scriptcase']['charset_html'] ?>" />
 <META http-equiv="Expires" content="Fri, Jan 01 1900 00:00:00 GMT"/>
 <META http-equiv="Last-Modified" content="<?php echo gmdate("D, d M Y H:i:s"); ?> GMT"/>
 <META http-equiv="Cache-Control" content="no-store, no-cache, must-revalidate"/>
 <META http-equiv="Cache-Control" content="post-check=0, pre-check=0"/>
 <META http-equiv="Pragma" content="no-cache"/>
<?php
if ($_SESSION['scriptcase']['proc_mobile'])
{
?>
   <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<?php
}
?>
 <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $_SESSION['scriptcase']['css_popup'] ?>" /> 
 <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $_SESSION['scriptcase']['css_popup_dir'] ?>" /> 
 <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $_SESSION['scriptcase']['css_popup_div'] ?>" /> 
 <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $_SESSION['scriptcase']['css_popup_div_dir'] ?>" /> 
 <link rel="stylesheet" type="text/css" href="../_lib/buttons/<?php echo $_SESSION['scriptcase']['css_btn_popup'] ?>" /> 
</HEAD>
<BODY class="scGridPage" style="margin: 0px; overflow-x: hidden">
<script language="javascript" type="text/javascript" src="<?php echo $_SESSION['sc_session']['path_third'] ?>/jquery/js/jquery.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo $_SESSION['sc_session']['path_third'] ?>/jquery/js/jquery-ui.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo $_SESSION['sc_session']['path_third'] ?>/jquery_plugin/touch_punch/jquery.ui.touch-punch.min.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo $_SESSION['sc_session']['path_third'] ?>/tigra_color_picker/picker.js"></script>
<?php
   }
?>
<script language="javascript"> 
<?php
if ($embbed)
{
?>
  function scSubmitOrderCampos(sPos, sType) {
    $("#id_fsel_ok_sel_ord").val(sType);
    if(sType == 'cmp')
    {
       scPackSelectedOrd();
    }
   $.ajax({
    type: "POST",
    url: "grid_bayer_gestiones_order_campos.php",
    data: {
     script_case_init: $("#id_script_case_init_sel_ord").val(),
     script_case_session: $("#id_script_case_session_sel_ord").val(),
     path_img: $("#id_path_img_sel_ord").val(),
     path_btn: $("#id_path_btn_sel_ord").val(),
     campos_sel: $("#id_campos_sel_sel_ord").val(),
     sel_regra: $("#id_sel_regra_sel_ord").val(),
     fsel_ok: $("#id_fsel_ok_sel_ord").val(),
     embbed_groupby: 'Y'
    }
   }).success(function(data) {
    $("#sc_id_order_campos_placeholder_" + sPos).find("td").html(data);
    scBtnOrderCamposHide(sPos);
   });
  }
<?php
}
?>
 // Submeter o formularior
 //-------------------------------------
 function submit_form_Fsel_ord()
 {
     scPackSelectedOrd();
      document.Fsel_ord.submit();
 }
 function scPackSelectedOrd() {
  var fieldList, fieldName, i, selectedFields = new Array;
 fieldList = $("#sc_id_fldord_selected").sortable("toArray");
 for (i = 0; i < fieldList.length; i++) {
  fieldName  = fieldList[i].substr(14);
  selectedFields.push($("#sc_id_class_" + fieldName).val() + fieldName);
 }
 $("#id_campos_sel_sel_ord").val( selectedFields.join("@?@") );
 }
 </script>
<FORM name="Fsel_ord" method="POST">
  <INPUT type="hidden" name="script_case_init"    id="id_script_case_init_sel_ord"    value="<?php echo NM_encode_input($sc_init); ?>"> 
  <INPUT type="hidden" name="script_case_session" id="id_script_case_session_sel_ord" value="<?php echo NM_encode_input(session_id()); ?>"> 
  <INPUT type="hidden" name="path_img"            id="id_path_img_sel_ord"            value="<?php echo NM_encode_input($path_img); ?>"> 
  <INPUT type="hidden" name="path_btn"            id="id_path_btn_sel_ord"            value="<?php echo NM_encode_input($path_btn); ?>"> 
  <INPUT type="hidden" name="fsel_ok"             id="id_fsel_ok_sel_ord"             value=""> 
<?php
if ($embbed)
{
    echo "<div class='scAppDivMoldura'>";
    echo "<table id=\"main_table\" style=\"width: 100%\" cellspacing=0 cellpadding=0>";
}
elseif ($_SESSION['scriptcase']['reg_conf']['html_dir'] == " DIR='RTL'")
{
    echo "<table id=\"main_table\" style=\"position: relative; top: 20px; right: 20px\">";
}
else
{
    echo "<table id=\"main_table\" style=\"position: relative; top: 20px; left: 20px\">";
}
?>
<?php
if (!$embbed)
{
?>
<tr>
<td>
<div class="scGridBorder">
<table width='100%' cellspacing=0 cellpadding=0>
<?php
}
?>
 <tr>
  <td class="<?php echo ($embbed)? 'scAppDivHeader scAppDivHeaderText':'scGridLabelVert'; ?>">
   <?php echo $this->Nm_lang['lang_btns_sort_hint']; ?>
  </td>
 </tr>
 <tr>
  <td class="<?php echo ($embbed)? 'scAppDivContent css_scAppDivContentText':'scGridTabelaTd'; ?>">
   <table class="<?php echo ($embbed)? '':'scGridTabela'; ?>" style="border-width: 0; border-collapse: collapse; width:100%;" cellspacing=0 cellpadding=0>
    <tr class="<?php echo ($embbed)? '':'scGridFieldOddVert'; ?>">
     <td style="vertical-align: top">
     <table>
   <tr><td style="vertical-align: top">
 <script language="javascript" type="text/javascript">
  $(function() {
   $(".sc_ui_litem").mouseover(function() {
    $(this).css("cursor", "all-scroll");
   });
   $("#sc_id_fldord_available").sortable({
    connectWith: ".sc_ui_fldord_selected",
    placeholder: "scAppDivSelectFieldsPlaceholder",
    remove: function(event, ui) {
     var fieldName = $(ui.item[0]).find("select").attr("id");
     $("#" + fieldName).show();
     $('#f_sel_sub').css('display', 'inline-block');
    }
   }).disableSelection();
   $("#sc_id_fldord_selected").sortable({
    connectWith: ".sc_ui_fldord_available",
    placeholder: "scAppDivSelectFieldsPlaceholder",
    remove: function(event, ui) {
     var fieldName = $(ui.item[0]).find("select").attr("id");
     $("#" + fieldName).hide();
     $('#f_sel_sub').css('display', 'inline-block');
    }
   });
   scUpdateListHeight();
  });
  function scUpdateListHeight() {
   $("#sc_id_fldord_available").css("min-height", "<?php echo sizeof($tab_ger_campos) * 35 ?>px");
   $("#sc_id_fldord_selected").css("min-height", "<?php echo sizeof($tab_ger_campos) * 35 ?>px");
  }
 </script>
 <style type="text/css">
  .sc_ui_sortable_ord {
   list-style-type: none;
   margin: 0;
   min-width: 225px;
  }
  .sc_ui_sortable_ord li {
   margin: 0 3px 3px 3px;
   padding: 1px 3px 1px 15px;
   min-height: 28px;
  }
  .sc_ui_sortable_ord li span {
   position: absolute;
   margin-left: -1.3em;
  }
 </style>
    <ul class="sc_ui_sort_groupby sc_ui_sortable_ord sc_ui_fldord_available scAppDivSelectFields" id="sc_id_fldord_available">
<?php
   foreach ($tab_ger_campos as $NM_cada_field => $NM_cada_opc)
   {
       if ($NM_cada_opc != "none")
       {
           if (!isset($_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['ordem_select'][$tab_def_campos[$NM_cada_field]]))
           {
?>
     <li class="sc_ui_litem scAppDivSelectFieldsEnabled" id="sc_id_itemord_<?php echo NM_encode_input($tab_def_campos[$NM_cada_field]); ?>">
      <?php echo $tab_labels[$NM_cada_field]; ?>
      <select id="sc_id_class_<?php echo NM_encode_input($tab_def_campos[$NM_cada_field]); ?>" class="scAppDivToolbarInput" style="display: none">
       <option value="+">Asc</option>
       <option value="-">Desc</option>
      </select><br/>
     </li>
<?php
           }
       }
   }
?>
    </ul>
   </td>
   <td style="vertical-align: top">
    <ul class="sc_ui_sort_groupby sc_ui_sortable_ord sc_ui_fldord_selected scAppDivSelectFields" id="sc_id_fldord_selected">
<?php
   foreach ($_SESSION['sc_session'][$sc_init]['grid_bayer_gestiones']['ordem_select'] as $NM_cada_field => $NM_cada_opc)
   {
       if (isset($tab_converte[$NM_cada_field]))
       {
           $sAscSelected  = " selected";
           $sDescSelected = "";
           if ($NM_cada_opc == "desc")
           {
               $sAscSelected  = "";
               $sDescSelected = " selected";
           }
?>
     <li class="sc_ui_litem scAppDivSelectFieldsEnabled" id="sc_id_itemord_<?php echo $NM_cada_field; ?>">
      <?php echo $tab_labels[$tab_converte[$NM_cada_field]]; ?>
      <select id="sc_id_class_<?php echo NM_encode_input($tab_def_campos[ $tab_converte[$NM_cada_field] ]); ?>" class="scAppDivToolbarInput" onchange="$('#f_sel_sub').css('display', 'inline-block');">
       <option value="+"<?php echo $sAscSelected; ?>>Asc</option>
       <option value="-"<?php echo $sDescSelected; ?>>Desc</option>
      </select>
     </li>
<?php
       }
   }
?>
    </ul>
    <input type="hidden" name="campos_sel" id="id_campos_sel_sel_ord" value="">
   </td>
   </tr>
   </table>
   </td>
   </tr>
   </table>
  </td>
 </tr>
   <tr><td class="<?php echo ($embbed)? 'scAppDivToolbar':'scGridToolbar'; ?>">
<?php
   if (!$embbed)
   {
?>
   <?php echo nmButtonOutput($this->arr_buttons, "bok", "document.Fsel_ord.fsel_ok.value='cmp';submit_form_Fsel_ord()", "document.Fsel_ord.fsel_ok.value='cmp';submit_form_Fsel_ord()", "f_sel_sub", "", "", "", "absmiddle", "", "0px", $path_btn, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
<?php
   }
   else
   {
?>
   <?php echo nmButtonOutput($this->arr_buttons, "bapply", "scSubmitOrderCampos('" . NM_encode_input($tbar_pos) . "', 'cmp')", "scSubmitOrderCampos('" . NM_encode_input($tbar_pos) . "', 'cmp')", "f_sel_sub", "", "", "display: none;", "absmiddle", "", "0px", $path_btn, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
<?php
   }
?>
  &nbsp;&nbsp;&nbsp;
<?php
   if (!$embbed)
   {
?>
   <?php echo nmButtonOutput($this->arr_buttons, "bsair", "self.parent.tb_remove()", "self.parent.tb_remove()", "Bsair", "", "", "", "absmiddle", "", "0px", $path_btn, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
<?php
   }
   else
   {
?>
   <?php echo nmButtonOutput($this->arr_buttons, "bcancelar", "scBtnOrderCamposHide('" . NM_encode_input($tbar_pos) . "')", "scBtnOrderCamposHide('" . NM_encode_input($tbar_pos) . "')", "Bsair", "", "", "", "absmiddle", "", "0px", $path_btn, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
<?php
   }
?>
   </td>
   </tr>
<?php
if (!$embbed)
{
?>
</table>
</div>
</td>
</tr>
<?php
}
?>
</table>
<?php
if ($embbed)
{
?>
    </div>
<?php
}
?>
</FORM>
<script language="javascript"> 
var bFixed = false;
function ajusta_window_Fsel_ord()
{
<?php
   if ($embbed)
   {
?>
  return false;
<?php
   }
?>
  var mt = $(document.getElementById("main_table"));
  if (0 == mt.width() || 0 == mt.height())
  {
    setTimeout("ajusta_window_Fsel_ord()", 50);
    return;
  }
  else if(!bFixed)
  {
    var oOrig = $(document.Fsel_ord.sel_orig),
        oDest = $(document.Fsel_ord.sel_dest),
        mHeight = Math.max(oOrig.height(), oDest.height()),
        mWidth = Math.max(oOrig.width() + 5, oDest.width() + 5);
    oOrig.height(mHeight);
    oOrig.width(mWidth);
    oDest.height(mHeight);
    oDest.width(mWidth + 15);
    bFixed = true;
    if (navigator.userAgent.indexOf("Chrome/") > 0)
    {
      strMaxHeight = Math.min(($(window.parent).height()-80), mt.height());
      self.parent.tb_resize(strMaxHeight + 40, mt.width() + 40);
      setTimeout("ajusta_window_Fsel_ord()", 50);
      return;
    }
  }
  strMaxHeight = Math.min(($(window.parent).height()-80), mt.height());
  self.parent.tb_resize(strMaxHeight + 40, mt.width() + 40);
}
$( document ).ready(function() {
  ajusta_window_Fsel_ord();
});
</script>
<script>
    ajusta_window_Fsel_ord();
</script>
</BODY>
</HTML>
<?php
}
}
