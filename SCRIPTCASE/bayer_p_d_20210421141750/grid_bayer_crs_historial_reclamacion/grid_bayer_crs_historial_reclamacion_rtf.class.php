<?php

class grid_bayer_crs_historial_reclamacion_rtf
{
   var $Db;
   var $Erro;
   var $Ini;
   var $Lookup;
   var $nm_data;
   var $Texto_tag;
   var $Arquivo;
   var $Tit_doc;
   var $sc_proc_grid; 
   var $NM_cmp_hidden = array();

   //---- 
   function grid_bayer_crs_historial_reclamacion_rtf()
   {
      $this->nm_data   = new nm_data("es");
      $this->Texto_tag = "";
   }

   //---- 
   function monta_rtf()
   {
      $this->inicializa_vars();
      $this->gera_texto_tag();
      $this->grava_arquivo_rtf();
      $this->monta_html();
   }

   //----- 
   function inicializa_vars()
   {
      global $nm_lang;
      $dir_raiz          = strrpos($_SERVER['PHP_SELF'],"/") ;  
      $dir_raiz          = substr($_SERVER['PHP_SELF'], 0, $dir_raiz + 1) ;  
      $this->nm_location = $this->Ini->sc_protocolo . $this->Ini->server . $dir_raiz; 
      $this->Arquivo    = "sc_rtf";
      $this->Arquivo   .= "_" . date("YmdHis") . "_" . rand(0, 1000);
      $this->Arquivo   .= "_grid_bayer_crs_historial_reclamacion";
      $this->Arquivo   .= ".rtf";
      $this->Tit_doc    = "grid_bayer_crs_historial_reclamacion.rtf";
   }

   //----- 
   function gera_texto_tag()
   {
     global $nm_lang;
      global
             $nm_nada, $nm_lang;

      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->sc_proc_grid = false; 
      $nm_raiz_img  = ""; 
      if (isset($_SESSION['scriptcase']['sc_apl_conf']['grid_bayer_crs_historial_reclamacion']['field_display']) && !empty($_SESSION['scriptcase']['sc_apl_conf']['grid_bayer_crs_historial_reclamacion']['field_display']))
      {
          foreach ($_SESSION['scriptcase']['sc_apl_conf']['grid_bayer_crs_historial_reclamacion']['field_display'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_crs_historial_reclamacion']['usr_cmp_sel']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_crs_historial_reclamacion']['usr_cmp_sel']))
      {
          foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_crs_historial_reclamacion']['usr_cmp_sel'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_crs_historial_reclamacion']['php_cmp_sel']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_crs_historial_reclamacion']['php_cmp_sel']))
      {
          foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_crs_historial_reclamacion']['php_cmp_sel'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_crs_historial_reclamacion']['campos_busca']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_crs_historial_reclamacion']['campos_busca']))
      { 
          $Busca_temp = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_crs_historial_reclamacion']['campos_busca'];
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
              $Busca_temp = NM_conv_charset($Busca_temp, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->id_paciente = $Busca_temp['id_paciente']; 
          $tmp_pos = strpos($this->id_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->id_paciente = substr($this->id_paciente, 0, $tmp_pos);
          }
          $this->id_paciente_2 = $Busca_temp['id_paciente_input_2']; 
          $this->logro_comunicacion_gestion = $Busca_temp['logro_comunicacion_gestion']; 
          $tmp_pos = strpos($this->logro_comunicacion_gestion, "##@@");
          if ($tmp_pos !== false)
          {
              $this->logro_comunicacion_gestion = substr($this->logro_comunicacion_gestion, 0, $tmp_pos);
          }
          $this->fecha_comunicacion = $Busca_temp['fecha_comunicacion']; 
          $tmp_pos = strpos($this->fecha_comunicacion, "##@@");
          if ($tmp_pos !== false)
          {
              $this->fecha_comunicacion = substr($this->fecha_comunicacion, 0, $tmp_pos);
          }
          $this->autor_gestion = $Busca_temp['autor_gestion']; 
          $tmp_pos = strpos($this->autor_gestion, "##@@");
          if ($tmp_pos !== false)
          {
              $this->autor_gestion = substr($this->autor_gestion, 0, $tmp_pos);
          }
      } 
      $this->nm_field_dinamico = array();
      $this->nm_order_dinamico = array();
      $this->sc_where_orig   = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_crs_historial_reclamacion']['where_orig'];
      $this->sc_where_atual  = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_crs_historial_reclamacion']['where_pesq'];
      $this->sc_where_filtro = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_crs_historial_reclamacion']['where_pesq_filtro'];
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_crs_historial_reclamacion']['rtf_name']))
      {
          $this->Arquivo = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_crs_historial_reclamacion']['rtf_name'];
          $this->Tit_doc = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_crs_historial_reclamacion']['rtf_name'];
          unset($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_crs_historial_reclamacion']['rtf_name']);
      }
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase))
      { 
          $nmgp_select = "SELECT ID_PACIENTE, LOGRO_COMUNICACION_GESTION, FECHA_COMUNICACION, AUTOR_GESTION, ESTADO_PACIENTE, FECHA_ACTIVACION_PACIENTE, PRODUCTO_TRATAMIENTO, NOMBRE_REFERENCIA, ASEGURADOR_TRATAMIENTO, OPERADOR_LOGISTICO_TRATAMIENTO, FECHA_RECLAMACION_GESTION, ID_HISTORIAL_RECLAMACION, ANIO_HISTORIAL_RECLAMACION, MES1, RECLAMO1, FECHA_RECLAMACION1, MOTIVO_NO_RECLAMACION1, MES2, RECLAMO2, FECHA_RECLAMACION2, MOTIVO_NO_RECLAMACION2, MES3, RECLAMO3, FECHA_RECLAMACION3, MOTIVO_NO_RECLAMACION3, MES4, RECLAMO4, FECHA_RECLAMACION4, MOTIVO_NO_RECLAMACION4, MES5, RECLAMO5, FECHA_RECLAMACION5, MOTIVO_NO_RECLAMACION5, MES6, RECLAMO6, FECHA_RECLAMACION6, MOTIVO_NO_RECLAMACION6, MES7, RECLAMO7, FECHA_RECLAMACION7, MOTIVO_NO_RECLAMACION7, MES8, RECLAMO8, FECHA_RECLAMACION8, MOTIVO_NO_RECLAMACION8, MES9, RECLAMO9, FECHA_RECLAMACION9, MOTIVO_NO_RECLAMACION9, MES10, RECLAMO10, FECHA_RECLAMACION10, MOTIVO_NO_RECLAMACION10, MES11, RECLAMO11, FECHA_RECLAMACION11, MOTIVO_NO_RECLAMACION11, MES12, RECLAMO12, FECHA_RECLAMACION12, MOTIVO_NO_RECLAMACION12 from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.ESTADO_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bg.FECHA_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT  FECHA_COMUNICACION FROM bayer_gestiones WHERE ID_PACIENTE_FK2=ID_PACIENTE ORDER BY FECHA_COMUNICACION DESC LIMIT 1) ) nm_sel_esp"; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
      { 
          $nmgp_select = "SELECT ID_PACIENTE, LOGRO_COMUNICACION_GESTION, FECHA_COMUNICACION, AUTOR_GESTION, ESTADO_PACIENTE, FECHA_ACTIVACION_PACIENTE, PRODUCTO_TRATAMIENTO, NOMBRE_REFERENCIA, ASEGURADOR_TRATAMIENTO, OPERADOR_LOGISTICO_TRATAMIENTO, FECHA_RECLAMACION_GESTION, ID_HISTORIAL_RECLAMACION, ANIO_HISTORIAL_RECLAMACION, MES1, RECLAMO1, FECHA_RECLAMACION1, MOTIVO_NO_RECLAMACION1, MES2, RECLAMO2, FECHA_RECLAMACION2, MOTIVO_NO_RECLAMACION2, MES3, RECLAMO3, FECHA_RECLAMACION3, MOTIVO_NO_RECLAMACION3, MES4, RECLAMO4, FECHA_RECLAMACION4, MOTIVO_NO_RECLAMACION4, MES5, RECLAMO5, FECHA_RECLAMACION5, MOTIVO_NO_RECLAMACION5, MES6, RECLAMO6, FECHA_RECLAMACION6, MOTIVO_NO_RECLAMACION6, MES7, RECLAMO7, FECHA_RECLAMACION7, MOTIVO_NO_RECLAMACION7, MES8, RECLAMO8, FECHA_RECLAMACION8, MOTIVO_NO_RECLAMACION8, MES9, RECLAMO9, FECHA_RECLAMACION9, MOTIVO_NO_RECLAMACION9, MES10, RECLAMO10, FECHA_RECLAMACION10, MOTIVO_NO_RECLAMACION10, MES11, RECLAMO11, FECHA_RECLAMACION11, MOTIVO_NO_RECLAMACION11, MES12, RECLAMO12, FECHA_RECLAMACION12, MOTIVO_NO_RECLAMACION12 from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.ESTADO_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bg.FECHA_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT  FECHA_COMUNICACION FROM bayer_gestiones WHERE ID_PACIENTE_FK2=ID_PACIENTE ORDER BY FECHA_COMUNICACION DESC LIMIT 1) ) nm_sel_esp"; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
      { 
          $nmgp_select = "SELECT ID_PACIENTE, LOGRO_COMUNICACION_GESTION, FECHA_COMUNICACION, AUTOR_GESTION, ESTADO_PACIENTE, FECHA_ACTIVACION_PACIENTE, PRODUCTO_TRATAMIENTO, NOMBRE_REFERENCIA, ASEGURADOR_TRATAMIENTO, OPERADOR_LOGISTICO_TRATAMIENTO, FECHA_RECLAMACION_GESTION, ID_HISTORIAL_RECLAMACION, ANIO_HISTORIAL_RECLAMACION, MES1, RECLAMO1, FECHA_RECLAMACION1, MOTIVO_NO_RECLAMACION1, MES2, RECLAMO2, FECHA_RECLAMACION2, MOTIVO_NO_RECLAMACION2, MES3, RECLAMO3, FECHA_RECLAMACION3, MOTIVO_NO_RECLAMACION3, MES4, RECLAMO4, FECHA_RECLAMACION4, MOTIVO_NO_RECLAMACION4, MES5, RECLAMO5, FECHA_RECLAMACION5, MOTIVO_NO_RECLAMACION5, MES6, RECLAMO6, FECHA_RECLAMACION6, MOTIVO_NO_RECLAMACION6, MES7, RECLAMO7, FECHA_RECLAMACION7, MOTIVO_NO_RECLAMACION7, MES8, RECLAMO8, FECHA_RECLAMACION8, MOTIVO_NO_RECLAMACION8, MES9, RECLAMO9, FECHA_RECLAMACION9, MOTIVO_NO_RECLAMACION9, MES10, RECLAMO10, FECHA_RECLAMACION10, MOTIVO_NO_RECLAMACION10, MES11, RECLAMO11, FECHA_RECLAMACION11, MOTIVO_NO_RECLAMACION11, MES12, RECLAMO12, FECHA_RECLAMACION12, MOTIVO_NO_RECLAMACION12 from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.ESTADO_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bg.FECHA_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT  FECHA_COMUNICACION FROM bayer_gestiones WHERE ID_PACIENTE_FK2=ID_PACIENTE ORDER BY FECHA_COMUNICACION DESC LIMIT 1) ) nm_sel_esp"; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
      { 
          $nmgp_select = "SELECT ID_PACIENTE, LOGRO_COMUNICACION_GESTION, FECHA_COMUNICACION, AUTOR_GESTION, ESTADO_PACIENTE, FECHA_ACTIVACION_PACIENTE, PRODUCTO_TRATAMIENTO, NOMBRE_REFERENCIA, ASEGURADOR_TRATAMIENTO, OPERADOR_LOGISTICO_TRATAMIENTO, FECHA_RECLAMACION_GESTION, ID_HISTORIAL_RECLAMACION, ANIO_HISTORIAL_RECLAMACION, MES1, RECLAMO1, FECHA_RECLAMACION1, MOTIVO_NO_RECLAMACION1, MES2, RECLAMO2, FECHA_RECLAMACION2, MOTIVO_NO_RECLAMACION2, MES3, RECLAMO3, FECHA_RECLAMACION3, MOTIVO_NO_RECLAMACION3, MES4, RECLAMO4, FECHA_RECLAMACION4, MOTIVO_NO_RECLAMACION4, MES5, RECLAMO5, FECHA_RECLAMACION5, MOTIVO_NO_RECLAMACION5, MES6, RECLAMO6, FECHA_RECLAMACION6, MOTIVO_NO_RECLAMACION6, MES7, RECLAMO7, FECHA_RECLAMACION7, MOTIVO_NO_RECLAMACION7, MES8, RECLAMO8, FECHA_RECLAMACION8, MOTIVO_NO_RECLAMACION8, MES9, RECLAMO9, FECHA_RECLAMACION9, MOTIVO_NO_RECLAMACION9, MES10, RECLAMO10, FECHA_RECLAMACION10, MOTIVO_NO_RECLAMACION10, MES11, RECLAMO11, FECHA_RECLAMACION11, MOTIVO_NO_RECLAMACION11, MES12, RECLAMO12, FECHA_RECLAMACION12, MOTIVO_NO_RECLAMACION12 from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.ESTADO_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bg.FECHA_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT  FECHA_COMUNICACION FROM bayer_gestiones WHERE ID_PACIENTE_FK2=ID_PACIENTE ORDER BY FECHA_COMUNICACION DESC LIMIT 1) ) nm_sel_esp"; 
       } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
      { 
          $nmgp_select = "SELECT ID_PACIENTE, LOGRO_COMUNICACION_GESTION, FECHA_COMUNICACION, AUTOR_GESTION, ESTADO_PACIENTE, FECHA_ACTIVACION_PACIENTE, PRODUCTO_TRATAMIENTO, NOMBRE_REFERENCIA, ASEGURADOR_TRATAMIENTO, OPERADOR_LOGISTICO_TRATAMIENTO, FECHA_RECLAMACION_GESTION, ID_HISTORIAL_RECLAMACION, ANIO_HISTORIAL_RECLAMACION, MES1, RECLAMO1, FECHA_RECLAMACION1, MOTIVO_NO_RECLAMACION1, MES2, RECLAMO2, FECHA_RECLAMACION2, MOTIVO_NO_RECLAMACION2, MES3, RECLAMO3, FECHA_RECLAMACION3, MOTIVO_NO_RECLAMACION3, MES4, RECLAMO4, FECHA_RECLAMACION4, MOTIVO_NO_RECLAMACION4, MES5, RECLAMO5, FECHA_RECLAMACION5, MOTIVO_NO_RECLAMACION5, MES6, RECLAMO6, FECHA_RECLAMACION6, MOTIVO_NO_RECLAMACION6, MES7, RECLAMO7, FECHA_RECLAMACION7, MOTIVO_NO_RECLAMACION7, MES8, RECLAMO8, FECHA_RECLAMACION8, MOTIVO_NO_RECLAMACION8, MES9, RECLAMO9, FECHA_RECLAMACION9, MOTIVO_NO_RECLAMACION9, MES10, RECLAMO10, FECHA_RECLAMACION10, MOTIVO_NO_RECLAMACION10, MES11, RECLAMO11, FECHA_RECLAMACION11, MOTIVO_NO_RECLAMACION11, MES12, RECLAMO12, FECHA_RECLAMACION12, MOTIVO_NO_RECLAMACION12 from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.ESTADO_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bg.FECHA_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT  FECHA_COMUNICACION FROM bayer_gestiones WHERE ID_PACIENTE_FK2=ID_PACIENTE ORDER BY FECHA_COMUNICACION DESC LIMIT 1) ) nm_sel_esp"; 
       } 
      else 
      { 
          $nmgp_select = "SELECT ID_PACIENTE, LOGRO_COMUNICACION_GESTION, FECHA_COMUNICACION, AUTOR_GESTION, ESTADO_PACIENTE, FECHA_ACTIVACION_PACIENTE, PRODUCTO_TRATAMIENTO, NOMBRE_REFERENCIA, ASEGURADOR_TRATAMIENTO, OPERADOR_LOGISTICO_TRATAMIENTO, FECHA_RECLAMACION_GESTION, ID_HISTORIAL_RECLAMACION, ANIO_HISTORIAL_RECLAMACION, MES1, RECLAMO1, FECHA_RECLAMACION1, MOTIVO_NO_RECLAMACION1, MES2, RECLAMO2, FECHA_RECLAMACION2, MOTIVO_NO_RECLAMACION2, MES3, RECLAMO3, FECHA_RECLAMACION3, MOTIVO_NO_RECLAMACION3, MES4, RECLAMO4, FECHA_RECLAMACION4, MOTIVO_NO_RECLAMACION4, MES5, RECLAMO5, FECHA_RECLAMACION5, MOTIVO_NO_RECLAMACION5, MES6, RECLAMO6, FECHA_RECLAMACION6, MOTIVO_NO_RECLAMACION6, MES7, RECLAMO7, FECHA_RECLAMACION7, MOTIVO_NO_RECLAMACION7, MES8, RECLAMO8, FECHA_RECLAMACION8, MOTIVO_NO_RECLAMACION8, MES9, RECLAMO9, FECHA_RECLAMACION9, MOTIVO_NO_RECLAMACION9, MES10, RECLAMO10, FECHA_RECLAMACION10, MOTIVO_NO_RECLAMACION10, MES11, RECLAMO11, FECHA_RECLAMACION11, MOTIVO_NO_RECLAMACION11, MES12, RECLAMO12, FECHA_RECLAMACION12, MOTIVO_NO_RECLAMACION12 from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.ESTADO_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bg.FECHA_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT  FECHA_COMUNICACION FROM bayer_gestiones WHERE ID_PACIENTE_FK2=ID_PACIENTE ORDER BY FECHA_COMUNICACION DESC LIMIT 1) ) nm_sel_esp"; 
      } 
      $nmgp_select .= " " . $_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_crs_historial_reclamacion']['where_pesq'];
      $nmgp_order_by = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_crs_historial_reclamacion']['order_grid'];
      $nmgp_select .= $nmgp_order_by; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nmgp_select;
      $rs = $this->Db->Execute($nmgp_select);
      if ($rs === false && !$rs->EOF && $GLOBALS["NM_ERRO_IBASE"] != 1)
      {
         $this->Erro->mensagem(__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg());
         exit;
      }

      $this->Texto_tag .= "<table>\r\n";
      $this->Texto_tag .= "<tr>\r\n";
      foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_crs_historial_reclamacion']['field_order'] as $Cada_col)
      { 
          $SC_Label = (isset($this->New_label['id_paciente'])) ? $this->New_label['id_paciente'] : "ID PACIENTE"; 
          if ($Cada_col == "id_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['logro_comunicacion_gestion'])) ? $this->New_label['logro_comunicacion_gestion'] : "LOGRO COMUNICACION GESTION"; 
          if ($Cada_col == "logro_comunicacion_gestion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['fecha_comunicacion'])) ? $this->New_label['fecha_comunicacion'] : "FECHA COMUNICACION"; 
          if ($Cada_col == "fecha_comunicacion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['autor_gestion'])) ? $this->New_label['autor_gestion'] : "AUTOR GESTION"; 
          if ($Cada_col == "autor_gestion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['estado_paciente'])) ? $this->New_label['estado_paciente'] : "ESTADO PACIENTE"; 
          if ($Cada_col == "estado_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['fecha_activacion_paciente'])) ? $this->New_label['fecha_activacion_paciente'] : "FECHA ACTIVACION PACIENTE"; 
          if ($Cada_col == "fecha_activacion_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['producto_tratamiento'])) ? $this->New_label['producto_tratamiento'] : "PRODUCTO TRATAMIENTO"; 
          if ($Cada_col == "producto_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['nombre_referencia'])) ? $this->New_label['nombre_referencia'] : "NOMBRE REFERENCIA"; 
          if ($Cada_col == "nombre_referencia" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['asegurador_tratamiento'])) ? $this->New_label['asegurador_tratamiento'] : "ASEGURADOR TRATAMIENTO"; 
          if ($Cada_col == "asegurador_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['operador_logistico_tratamiento'])) ? $this->New_label['operador_logistico_tratamiento'] : "OPERADOR LOGISTICO TRATAMIENTO"; 
          if ($Cada_col == "operador_logistico_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['fecha_reclamacion_gestion'])) ? $this->New_label['fecha_reclamacion_gestion'] : "FECHA RECLAMACION GESTION"; 
          if ($Cada_col == "fecha_reclamacion_gestion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['id_historial_reclamacion'])) ? $this->New_label['id_historial_reclamacion'] : "ID HISTORIAL RECLAMACION"; 
          if ($Cada_col == "id_historial_reclamacion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['anio_historial_reclamacion'])) ? $this->New_label['anio_historial_reclamacion'] : "ANIO HISTORIAL RECLAMACION"; 
          if ($Cada_col == "anio_historial_reclamacion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['mes1'])) ? $this->New_label['mes1'] : "MES1"; 
          if ($Cada_col == "mes1" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['reclamo1'])) ? $this->New_label['reclamo1'] : "RECLAMO1"; 
          if ($Cada_col == "reclamo1" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['fecha_reclamacion1'])) ? $this->New_label['fecha_reclamacion1'] : "FECHA RECLAMACION1"; 
          if ($Cada_col == "fecha_reclamacion1" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['motivo_no_reclamacion1'])) ? $this->New_label['motivo_no_reclamacion1'] : "MOTIVO NO RECLAMACION1"; 
          if ($Cada_col == "motivo_no_reclamacion1" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['mes2'])) ? $this->New_label['mes2'] : "MES2"; 
          if ($Cada_col == "mes2" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['reclamo2'])) ? $this->New_label['reclamo2'] : "RECLAMO2"; 
          if ($Cada_col == "reclamo2" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['fecha_reclamacion2'])) ? $this->New_label['fecha_reclamacion2'] : "FECHA RECLAMACION2"; 
          if ($Cada_col == "fecha_reclamacion2" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['motivo_no_reclamacion2'])) ? $this->New_label['motivo_no_reclamacion2'] : "MOTIVO NO RECLAMACION2"; 
          if ($Cada_col == "motivo_no_reclamacion2" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['mes3'])) ? $this->New_label['mes3'] : "MES3"; 
          if ($Cada_col == "mes3" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['reclamo3'])) ? $this->New_label['reclamo3'] : "RECLAMO3"; 
          if ($Cada_col == "reclamo3" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['fecha_reclamacion3'])) ? $this->New_label['fecha_reclamacion3'] : "FECHA RECLAMACION3"; 
          if ($Cada_col == "fecha_reclamacion3" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['motivo_no_reclamacion3'])) ? $this->New_label['motivo_no_reclamacion3'] : "MOTIVO NO RECLAMACION3"; 
          if ($Cada_col == "motivo_no_reclamacion3" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['mes4'])) ? $this->New_label['mes4'] : "MES4"; 
          if ($Cada_col == "mes4" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['reclamo4'])) ? $this->New_label['reclamo4'] : "RECLAMO4"; 
          if ($Cada_col == "reclamo4" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['fecha_reclamacion4'])) ? $this->New_label['fecha_reclamacion4'] : "FECHA RECLAMACION4"; 
          if ($Cada_col == "fecha_reclamacion4" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['motivo_no_reclamacion4'])) ? $this->New_label['motivo_no_reclamacion4'] : "MOTIVO NO RECLAMACION4"; 
          if ($Cada_col == "motivo_no_reclamacion4" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['mes5'])) ? $this->New_label['mes5'] : "MES5"; 
          if ($Cada_col == "mes5" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['reclamo5'])) ? $this->New_label['reclamo5'] : "RECLAMO5"; 
          if ($Cada_col == "reclamo5" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['fecha_reclamacion5'])) ? $this->New_label['fecha_reclamacion5'] : "FECHA RECLAMACION5"; 
          if ($Cada_col == "fecha_reclamacion5" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['motivo_no_reclamacion5'])) ? $this->New_label['motivo_no_reclamacion5'] : "MOTIVO NO RECLAMACION5"; 
          if ($Cada_col == "motivo_no_reclamacion5" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['mes6'])) ? $this->New_label['mes6'] : "MES6"; 
          if ($Cada_col == "mes6" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['reclamo6'])) ? $this->New_label['reclamo6'] : "RECLAMO6"; 
          if ($Cada_col == "reclamo6" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['fecha_reclamacion6'])) ? $this->New_label['fecha_reclamacion6'] : "FECHA RECLAMACION6"; 
          if ($Cada_col == "fecha_reclamacion6" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['motivo_no_reclamacion6'])) ? $this->New_label['motivo_no_reclamacion6'] : "MOTIVO NO RECLAMACION6"; 
          if ($Cada_col == "motivo_no_reclamacion6" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['mes7'])) ? $this->New_label['mes7'] : "MES7"; 
          if ($Cada_col == "mes7" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['reclamo7'])) ? $this->New_label['reclamo7'] : "RECLAMO7"; 
          if ($Cada_col == "reclamo7" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['fecha_reclamacion7'])) ? $this->New_label['fecha_reclamacion7'] : "FECHA RECLAMACION7"; 
          if ($Cada_col == "fecha_reclamacion7" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['motivo_no_reclamacion7'])) ? $this->New_label['motivo_no_reclamacion7'] : "MOTIVO NO RECLAMACION7"; 
          if ($Cada_col == "motivo_no_reclamacion7" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['mes8'])) ? $this->New_label['mes8'] : "MES8"; 
          if ($Cada_col == "mes8" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['reclamo8'])) ? $this->New_label['reclamo8'] : "RECLAMO8"; 
          if ($Cada_col == "reclamo8" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['fecha_reclamacion8'])) ? $this->New_label['fecha_reclamacion8'] : "FECHA RECLAMACION8"; 
          if ($Cada_col == "fecha_reclamacion8" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['motivo_no_reclamacion8'])) ? $this->New_label['motivo_no_reclamacion8'] : "MOTIVO NO RECLAMACION8"; 
          if ($Cada_col == "motivo_no_reclamacion8" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['mes9'])) ? $this->New_label['mes9'] : "MES9"; 
          if ($Cada_col == "mes9" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['reclamo9'])) ? $this->New_label['reclamo9'] : "RECLAMO9"; 
          if ($Cada_col == "reclamo9" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['fecha_reclamacion9'])) ? $this->New_label['fecha_reclamacion9'] : "FECHA RECLAMACION9"; 
          if ($Cada_col == "fecha_reclamacion9" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['motivo_no_reclamacion9'])) ? $this->New_label['motivo_no_reclamacion9'] : "MOTIVO NO RECLAMACION9"; 
          if ($Cada_col == "motivo_no_reclamacion9" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['mes10'])) ? $this->New_label['mes10'] : "MES10"; 
          if ($Cada_col == "mes10" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['reclamo10'])) ? $this->New_label['reclamo10'] : "RECLAMO10"; 
          if ($Cada_col == "reclamo10" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['fecha_reclamacion10'])) ? $this->New_label['fecha_reclamacion10'] : "FECHA RECLAMACION10"; 
          if ($Cada_col == "fecha_reclamacion10" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['motivo_no_reclamacion10'])) ? $this->New_label['motivo_no_reclamacion10'] : "MOTIVO NO RECLAMACION10"; 
          if ($Cada_col == "motivo_no_reclamacion10" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['mes11'])) ? $this->New_label['mes11'] : "MES11"; 
          if ($Cada_col == "mes11" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['reclamo11'])) ? $this->New_label['reclamo11'] : "RECLAMO11"; 
          if ($Cada_col == "reclamo11" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['fecha_reclamacion11'])) ? $this->New_label['fecha_reclamacion11'] : "FECHA RECLAMACION11"; 
          if ($Cada_col == "fecha_reclamacion11" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['motivo_no_reclamacion11'])) ? $this->New_label['motivo_no_reclamacion11'] : "MOTIVO NO RECLAMACION11"; 
          if ($Cada_col == "motivo_no_reclamacion11" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['mes12'])) ? $this->New_label['mes12'] : "MES12"; 
          if ($Cada_col == "mes12" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['reclamo12'])) ? $this->New_label['reclamo12'] : "RECLAMO12"; 
          if ($Cada_col == "reclamo12" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['fecha_reclamacion12'])) ? $this->New_label['fecha_reclamacion12'] : "FECHA RECLAMACION12"; 
          if ($Cada_col == "fecha_reclamacion12" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['motivo_no_reclamacion12'])) ? $this->New_label['motivo_no_reclamacion12'] : "MOTIVO NO RECLAMACION12"; 
          if ($Cada_col == "motivo_no_reclamacion12" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
      } 
      $this->Texto_tag .= "</tr>\r\n";
      while (!$rs->EOF)
      {
         $this->Texto_tag .= "<tr>\r\n";
         $this->id_paciente = $rs->fields[0] ;  
         $this->id_paciente = (string)$this->id_paciente;
         $this->logro_comunicacion_gestion = $rs->fields[1] ;  
         $this->fecha_comunicacion = $rs->fields[2] ;  
         $this->autor_gestion = $rs->fields[3] ;  
         $this->estado_paciente = $rs->fields[4] ;  
         $this->fecha_activacion_paciente = $rs->fields[5] ;  
         $this->producto_tratamiento = $rs->fields[6] ;  
         $this->nombre_referencia = $rs->fields[7] ;  
         $this->asegurador_tratamiento = $rs->fields[8] ;  
         $this->operador_logistico_tratamiento = $rs->fields[9] ;  
         $this->fecha_reclamacion_gestion = $rs->fields[10] ;  
         $this->id_historial_reclamacion = $rs->fields[11] ;  
         $this->id_historial_reclamacion = (string)$this->id_historial_reclamacion;
         $this->anio_historial_reclamacion = $rs->fields[12] ;  
         $this->anio_historial_reclamacion = (string)$this->anio_historial_reclamacion;
         $this->mes1 = $rs->fields[13] ;  
         $this->reclamo1 = $rs->fields[14] ;  
         $this->fecha_reclamacion1 = $rs->fields[15] ;  
         $this->motivo_no_reclamacion1 = $rs->fields[16] ;  
         $this->mes2 = $rs->fields[17] ;  
         $this->reclamo2 = $rs->fields[18] ;  
         $this->fecha_reclamacion2 = $rs->fields[19] ;  
         $this->motivo_no_reclamacion2 = $rs->fields[20] ;  
         $this->mes3 = $rs->fields[21] ;  
         $this->reclamo3 = $rs->fields[22] ;  
         $this->fecha_reclamacion3 = $rs->fields[23] ;  
         $this->motivo_no_reclamacion3 = $rs->fields[24] ;  
         $this->mes4 = $rs->fields[25] ;  
         $this->reclamo4 = $rs->fields[26] ;  
         $this->fecha_reclamacion4 = $rs->fields[27] ;  
         $this->motivo_no_reclamacion4 = $rs->fields[28] ;  
         $this->mes5 = $rs->fields[29] ;  
         $this->reclamo5 = $rs->fields[30] ;  
         $this->fecha_reclamacion5 = $rs->fields[31] ;  
         $this->motivo_no_reclamacion5 = $rs->fields[32] ;  
         $this->mes6 = $rs->fields[33] ;  
         $this->reclamo6 = $rs->fields[34] ;  
         $this->fecha_reclamacion6 = $rs->fields[35] ;  
         $this->motivo_no_reclamacion6 = $rs->fields[36] ;  
         $this->mes7 = $rs->fields[37] ;  
         $this->reclamo7 = $rs->fields[38] ;  
         $this->fecha_reclamacion7 = $rs->fields[39] ;  
         $this->motivo_no_reclamacion7 = $rs->fields[40] ;  
         $this->mes8 = $rs->fields[41] ;  
         $this->reclamo8 = $rs->fields[42] ;  
         $this->fecha_reclamacion8 = $rs->fields[43] ;  
         $this->motivo_no_reclamacion8 = $rs->fields[44] ;  
         $this->mes9 = $rs->fields[45] ;  
         $this->reclamo9 = $rs->fields[46] ;  
         $this->fecha_reclamacion9 = $rs->fields[47] ;  
         $this->motivo_no_reclamacion9 = $rs->fields[48] ;  
         $this->mes10 = $rs->fields[49] ;  
         $this->reclamo10 = $rs->fields[50] ;  
         $this->fecha_reclamacion10 = $rs->fields[51] ;  
         $this->motivo_no_reclamacion10 = $rs->fields[52] ;  
         $this->mes11 = $rs->fields[53] ;  
         $this->reclamo11 = $rs->fields[54] ;  
         $this->fecha_reclamacion11 = $rs->fields[55] ;  
         $this->motivo_no_reclamacion11 = $rs->fields[56] ;  
         $this->mes12 = $rs->fields[57] ;  
         $this->reclamo12 = $rs->fields[58] ;  
         $this->fecha_reclamacion12 = $rs->fields[59] ;  
         $this->motivo_no_reclamacion12 = $rs->fields[60] ;  
         $this->sc_proc_grid = true; 
         foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_crs_historial_reclamacion']['field_order'] as $Cada_col)
         { 
            if (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off")
            { 
                $NM_func_exp = "NM_export_" . $Cada_col;
                $this->$NM_func_exp();
            } 
         } 
         $this->Texto_tag .= "</tr>\r\n";
         $rs->MoveNext();
      }
      $this->Texto_tag .= "</table>\r\n";

      $rs->Close();
   }
   //----- id_paciente
   function NM_export_id_paciente()
   {
         nmgp_Form_Num_Val($this->id_paciente, $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
         if (!NM_is_utf8($this->id_paciente))
         {
             $this->id_paciente = sc_convert_encoding($this->id_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->id_paciente = str_replace('<', '&lt;', $this->id_paciente);
         $this->id_paciente = str_replace('>', '&gt;', $this->id_paciente);
         $this->Texto_tag .= "<td>" . $this->id_paciente . "</td>\r\n";
   }
   //----- logro_comunicacion_gestion
   function NM_export_logro_comunicacion_gestion()
   {
         $this->logro_comunicacion_gestion = html_entity_decode($this->logro_comunicacion_gestion, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->logro_comunicacion_gestion = strip_tags($this->logro_comunicacion_gestion);
         if (!NM_is_utf8($this->logro_comunicacion_gestion))
         {
             $this->logro_comunicacion_gestion = sc_convert_encoding($this->logro_comunicacion_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->logro_comunicacion_gestion = str_replace('<', '&lt;', $this->logro_comunicacion_gestion);
         $this->logro_comunicacion_gestion = str_replace('>', '&gt;', $this->logro_comunicacion_gestion);
         $this->Texto_tag .= "<td>" . $this->logro_comunicacion_gestion . "</td>\r\n";
   }
   //----- fecha_comunicacion
   function NM_export_fecha_comunicacion()
   {
         $this->fecha_comunicacion = html_entity_decode($this->fecha_comunicacion, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->fecha_comunicacion = strip_tags($this->fecha_comunicacion);
         if (!NM_is_utf8($this->fecha_comunicacion))
         {
             $this->fecha_comunicacion = sc_convert_encoding($this->fecha_comunicacion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->fecha_comunicacion = str_replace('<', '&lt;', $this->fecha_comunicacion);
         $this->fecha_comunicacion = str_replace('>', '&gt;', $this->fecha_comunicacion);
         $this->Texto_tag .= "<td>" . $this->fecha_comunicacion . "</td>\r\n";
   }
   //----- autor_gestion
   function NM_export_autor_gestion()
   {
         $this->autor_gestion = html_entity_decode($this->autor_gestion, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->autor_gestion = strip_tags($this->autor_gestion);
         if (!NM_is_utf8($this->autor_gestion))
         {
             $this->autor_gestion = sc_convert_encoding($this->autor_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->autor_gestion = str_replace('<', '&lt;', $this->autor_gestion);
         $this->autor_gestion = str_replace('>', '&gt;', $this->autor_gestion);
         $this->Texto_tag .= "<td>" . $this->autor_gestion . "</td>\r\n";
   }
   //----- estado_paciente
   function NM_export_estado_paciente()
   {
         $this->estado_paciente = html_entity_decode($this->estado_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->estado_paciente = strip_tags($this->estado_paciente);
         if (!NM_is_utf8($this->estado_paciente))
         {
             $this->estado_paciente = sc_convert_encoding($this->estado_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->estado_paciente = str_replace('<', '&lt;', $this->estado_paciente);
         $this->estado_paciente = str_replace('>', '&gt;', $this->estado_paciente);
         $this->Texto_tag .= "<td>" . $this->estado_paciente . "</td>\r\n";
   }
   //----- fecha_activacion_paciente
   function NM_export_fecha_activacion_paciente()
   {
         $this->fecha_activacion_paciente = html_entity_decode($this->fecha_activacion_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->fecha_activacion_paciente = strip_tags($this->fecha_activacion_paciente);
         if (!NM_is_utf8($this->fecha_activacion_paciente))
         {
             $this->fecha_activacion_paciente = sc_convert_encoding($this->fecha_activacion_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->fecha_activacion_paciente = str_replace('<', '&lt;', $this->fecha_activacion_paciente);
         $this->fecha_activacion_paciente = str_replace('>', '&gt;', $this->fecha_activacion_paciente);
         $this->Texto_tag .= "<td>" . $this->fecha_activacion_paciente . "</td>\r\n";
   }
   //----- producto_tratamiento
   function NM_export_producto_tratamiento()
   {
         $this->producto_tratamiento = html_entity_decode($this->producto_tratamiento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->producto_tratamiento = strip_tags($this->producto_tratamiento);
         if (!NM_is_utf8($this->producto_tratamiento))
         {
             $this->producto_tratamiento = sc_convert_encoding($this->producto_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->producto_tratamiento = str_replace('<', '&lt;', $this->producto_tratamiento);
         $this->producto_tratamiento = str_replace('>', '&gt;', $this->producto_tratamiento);
         $this->Texto_tag .= "<td>" . $this->producto_tratamiento . "</td>\r\n";
   }
   //----- nombre_referencia
   function NM_export_nombre_referencia()
   {
         $this->nombre_referencia = html_entity_decode($this->nombre_referencia, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->nombre_referencia = strip_tags($this->nombre_referencia);
         if (!NM_is_utf8($this->nombre_referencia))
         {
             $this->nombre_referencia = sc_convert_encoding($this->nombre_referencia, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->nombre_referencia = str_replace('<', '&lt;', $this->nombre_referencia);
         $this->nombre_referencia = str_replace('>', '&gt;', $this->nombre_referencia);
         $this->Texto_tag .= "<td>" . $this->nombre_referencia . "</td>\r\n";
   }
   //----- asegurador_tratamiento
   function NM_export_asegurador_tratamiento()
   {
         $this->asegurador_tratamiento = html_entity_decode($this->asegurador_tratamiento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->asegurador_tratamiento = strip_tags($this->asegurador_tratamiento);
         if (!NM_is_utf8($this->asegurador_tratamiento))
         {
             $this->asegurador_tratamiento = sc_convert_encoding($this->asegurador_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->asegurador_tratamiento = str_replace('<', '&lt;', $this->asegurador_tratamiento);
         $this->asegurador_tratamiento = str_replace('>', '&gt;', $this->asegurador_tratamiento);
         $this->Texto_tag .= "<td>" . $this->asegurador_tratamiento . "</td>\r\n";
   }
   //----- operador_logistico_tratamiento
   function NM_export_operador_logistico_tratamiento()
   {
         $this->operador_logistico_tratamiento = html_entity_decode($this->operador_logistico_tratamiento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->operador_logistico_tratamiento = strip_tags($this->operador_logistico_tratamiento);
         if (!NM_is_utf8($this->operador_logistico_tratamiento))
         {
             $this->operador_logistico_tratamiento = sc_convert_encoding($this->operador_logistico_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->operador_logistico_tratamiento = str_replace('<', '&lt;', $this->operador_logistico_tratamiento);
         $this->operador_logistico_tratamiento = str_replace('>', '&gt;', $this->operador_logistico_tratamiento);
         $this->Texto_tag .= "<td>" . $this->operador_logistico_tratamiento . "</td>\r\n";
   }
   //----- fecha_reclamacion_gestion
   function NM_export_fecha_reclamacion_gestion()
   {
         $this->fecha_reclamacion_gestion = html_entity_decode($this->fecha_reclamacion_gestion, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->fecha_reclamacion_gestion = strip_tags($this->fecha_reclamacion_gestion);
         if (!NM_is_utf8($this->fecha_reclamacion_gestion))
         {
             $this->fecha_reclamacion_gestion = sc_convert_encoding($this->fecha_reclamacion_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->fecha_reclamacion_gestion = str_replace('<', '&lt;', $this->fecha_reclamacion_gestion);
         $this->fecha_reclamacion_gestion = str_replace('>', '&gt;', $this->fecha_reclamacion_gestion);
         $this->Texto_tag .= "<td>" . $this->fecha_reclamacion_gestion . "</td>\r\n";
   }
   //----- id_historial_reclamacion
   function NM_export_id_historial_reclamacion()
   {
         nmgp_Form_Num_Val($this->id_historial_reclamacion, $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
         if (!NM_is_utf8($this->id_historial_reclamacion))
         {
             $this->id_historial_reclamacion = sc_convert_encoding($this->id_historial_reclamacion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->id_historial_reclamacion = str_replace('<', '&lt;', $this->id_historial_reclamacion);
         $this->id_historial_reclamacion = str_replace('>', '&gt;', $this->id_historial_reclamacion);
         $this->Texto_tag .= "<td>" . $this->id_historial_reclamacion . "</td>\r\n";
   }
   //----- anio_historial_reclamacion
   function NM_export_anio_historial_reclamacion()
   {
         nmgp_Form_Num_Val($this->anio_historial_reclamacion, $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
         if (!NM_is_utf8($this->anio_historial_reclamacion))
         {
             $this->anio_historial_reclamacion = sc_convert_encoding($this->anio_historial_reclamacion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->anio_historial_reclamacion = str_replace('<', '&lt;', $this->anio_historial_reclamacion);
         $this->anio_historial_reclamacion = str_replace('>', '&gt;', $this->anio_historial_reclamacion);
         $this->Texto_tag .= "<td>" . $this->anio_historial_reclamacion . "</td>\r\n";
   }
   //----- mes1
   function NM_export_mes1()
   {
         $this->mes1 = html_entity_decode($this->mes1, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->mes1 = strip_tags($this->mes1);
         if (!NM_is_utf8($this->mes1))
         {
             $this->mes1 = sc_convert_encoding($this->mes1, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->mes1 = str_replace('<', '&lt;', $this->mes1);
         $this->mes1 = str_replace('>', '&gt;', $this->mes1);
         $this->Texto_tag .= "<td>" . $this->mes1 . "</td>\r\n";
   }
   //----- reclamo1
   function NM_export_reclamo1()
   {
         $this->reclamo1 = html_entity_decode($this->reclamo1, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->reclamo1 = strip_tags($this->reclamo1);
         if (!NM_is_utf8($this->reclamo1))
         {
             $this->reclamo1 = sc_convert_encoding($this->reclamo1, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->reclamo1 = str_replace('<', '&lt;', $this->reclamo1);
         $this->reclamo1 = str_replace('>', '&gt;', $this->reclamo1);
         $this->Texto_tag .= "<td>" . $this->reclamo1 . "</td>\r\n";
   }
   //----- fecha_reclamacion1
   function NM_export_fecha_reclamacion1()
   {
         $this->fecha_reclamacion1 = html_entity_decode($this->fecha_reclamacion1, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->fecha_reclamacion1 = strip_tags($this->fecha_reclamacion1);
         if (!NM_is_utf8($this->fecha_reclamacion1))
         {
             $this->fecha_reclamacion1 = sc_convert_encoding($this->fecha_reclamacion1, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->fecha_reclamacion1 = str_replace('<', '&lt;', $this->fecha_reclamacion1);
         $this->fecha_reclamacion1 = str_replace('>', '&gt;', $this->fecha_reclamacion1);
         $this->Texto_tag .= "<td>" . $this->fecha_reclamacion1 . "</td>\r\n";
   }
   //----- motivo_no_reclamacion1
   function NM_export_motivo_no_reclamacion1()
   {
         $this->motivo_no_reclamacion1 = html_entity_decode($this->motivo_no_reclamacion1, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->motivo_no_reclamacion1 = strip_tags($this->motivo_no_reclamacion1);
         if (!NM_is_utf8($this->motivo_no_reclamacion1))
         {
             $this->motivo_no_reclamacion1 = sc_convert_encoding($this->motivo_no_reclamacion1, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->motivo_no_reclamacion1 = str_replace('<', '&lt;', $this->motivo_no_reclamacion1);
         $this->motivo_no_reclamacion1 = str_replace('>', '&gt;', $this->motivo_no_reclamacion1);
         $this->Texto_tag .= "<td>" . $this->motivo_no_reclamacion1 . "</td>\r\n";
   }
   //----- mes2
   function NM_export_mes2()
   {
         $this->mes2 = html_entity_decode($this->mes2, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->mes2 = strip_tags($this->mes2);
         if (!NM_is_utf8($this->mes2))
         {
             $this->mes2 = sc_convert_encoding($this->mes2, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->mes2 = str_replace('<', '&lt;', $this->mes2);
         $this->mes2 = str_replace('>', '&gt;', $this->mes2);
         $this->Texto_tag .= "<td>" . $this->mes2 . "</td>\r\n";
   }
   //----- reclamo2
   function NM_export_reclamo2()
   {
         $this->reclamo2 = html_entity_decode($this->reclamo2, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->reclamo2 = strip_tags($this->reclamo2);
         if (!NM_is_utf8($this->reclamo2))
         {
             $this->reclamo2 = sc_convert_encoding($this->reclamo2, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->reclamo2 = str_replace('<', '&lt;', $this->reclamo2);
         $this->reclamo2 = str_replace('>', '&gt;', $this->reclamo2);
         $this->Texto_tag .= "<td>" . $this->reclamo2 . "</td>\r\n";
   }
   //----- fecha_reclamacion2
   function NM_export_fecha_reclamacion2()
   {
         $this->fecha_reclamacion2 = html_entity_decode($this->fecha_reclamacion2, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->fecha_reclamacion2 = strip_tags($this->fecha_reclamacion2);
         if (!NM_is_utf8($this->fecha_reclamacion2))
         {
             $this->fecha_reclamacion2 = sc_convert_encoding($this->fecha_reclamacion2, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->fecha_reclamacion2 = str_replace('<', '&lt;', $this->fecha_reclamacion2);
         $this->fecha_reclamacion2 = str_replace('>', '&gt;', $this->fecha_reclamacion2);
         $this->Texto_tag .= "<td>" . $this->fecha_reclamacion2 . "</td>\r\n";
   }
   //----- motivo_no_reclamacion2
   function NM_export_motivo_no_reclamacion2()
   {
         $this->motivo_no_reclamacion2 = html_entity_decode($this->motivo_no_reclamacion2, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->motivo_no_reclamacion2 = strip_tags($this->motivo_no_reclamacion2);
         if (!NM_is_utf8($this->motivo_no_reclamacion2))
         {
             $this->motivo_no_reclamacion2 = sc_convert_encoding($this->motivo_no_reclamacion2, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->motivo_no_reclamacion2 = str_replace('<', '&lt;', $this->motivo_no_reclamacion2);
         $this->motivo_no_reclamacion2 = str_replace('>', '&gt;', $this->motivo_no_reclamacion2);
         $this->Texto_tag .= "<td>" . $this->motivo_no_reclamacion2 . "</td>\r\n";
   }
   //----- mes3
   function NM_export_mes3()
   {
         $this->mes3 = html_entity_decode($this->mes3, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->mes3 = strip_tags($this->mes3);
         if (!NM_is_utf8($this->mes3))
         {
             $this->mes3 = sc_convert_encoding($this->mes3, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->mes3 = str_replace('<', '&lt;', $this->mes3);
         $this->mes3 = str_replace('>', '&gt;', $this->mes3);
         $this->Texto_tag .= "<td>" . $this->mes3 . "</td>\r\n";
   }
   //----- reclamo3
   function NM_export_reclamo3()
   {
         $this->reclamo3 = html_entity_decode($this->reclamo3, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->reclamo3 = strip_tags($this->reclamo3);
         if (!NM_is_utf8($this->reclamo3))
         {
             $this->reclamo3 = sc_convert_encoding($this->reclamo3, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->reclamo3 = str_replace('<', '&lt;', $this->reclamo3);
         $this->reclamo3 = str_replace('>', '&gt;', $this->reclamo3);
         $this->Texto_tag .= "<td>" . $this->reclamo3 . "</td>\r\n";
   }
   //----- fecha_reclamacion3
   function NM_export_fecha_reclamacion3()
   {
         $this->fecha_reclamacion3 = html_entity_decode($this->fecha_reclamacion3, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->fecha_reclamacion3 = strip_tags($this->fecha_reclamacion3);
         if (!NM_is_utf8($this->fecha_reclamacion3))
         {
             $this->fecha_reclamacion3 = sc_convert_encoding($this->fecha_reclamacion3, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->fecha_reclamacion3 = str_replace('<', '&lt;', $this->fecha_reclamacion3);
         $this->fecha_reclamacion3 = str_replace('>', '&gt;', $this->fecha_reclamacion3);
         $this->Texto_tag .= "<td>" . $this->fecha_reclamacion3 . "</td>\r\n";
   }
   //----- motivo_no_reclamacion3
   function NM_export_motivo_no_reclamacion3()
   {
         $this->motivo_no_reclamacion3 = html_entity_decode($this->motivo_no_reclamacion3, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->motivo_no_reclamacion3 = strip_tags($this->motivo_no_reclamacion3);
         if (!NM_is_utf8($this->motivo_no_reclamacion3))
         {
             $this->motivo_no_reclamacion3 = sc_convert_encoding($this->motivo_no_reclamacion3, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->motivo_no_reclamacion3 = str_replace('<', '&lt;', $this->motivo_no_reclamacion3);
         $this->motivo_no_reclamacion3 = str_replace('>', '&gt;', $this->motivo_no_reclamacion3);
         $this->Texto_tag .= "<td>" . $this->motivo_no_reclamacion3 . "</td>\r\n";
   }
   //----- mes4
   function NM_export_mes4()
   {
         $this->mes4 = html_entity_decode($this->mes4, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->mes4 = strip_tags($this->mes4);
         if (!NM_is_utf8($this->mes4))
         {
             $this->mes4 = sc_convert_encoding($this->mes4, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->mes4 = str_replace('<', '&lt;', $this->mes4);
         $this->mes4 = str_replace('>', '&gt;', $this->mes4);
         $this->Texto_tag .= "<td>" . $this->mes4 . "</td>\r\n";
   }
   //----- reclamo4
   function NM_export_reclamo4()
   {
         $this->reclamo4 = html_entity_decode($this->reclamo4, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->reclamo4 = strip_tags($this->reclamo4);
         if (!NM_is_utf8($this->reclamo4))
         {
             $this->reclamo4 = sc_convert_encoding($this->reclamo4, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->reclamo4 = str_replace('<', '&lt;', $this->reclamo4);
         $this->reclamo4 = str_replace('>', '&gt;', $this->reclamo4);
         $this->Texto_tag .= "<td>" . $this->reclamo4 . "</td>\r\n";
   }
   //----- fecha_reclamacion4
   function NM_export_fecha_reclamacion4()
   {
         $this->fecha_reclamacion4 = html_entity_decode($this->fecha_reclamacion4, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->fecha_reclamacion4 = strip_tags($this->fecha_reclamacion4);
         if (!NM_is_utf8($this->fecha_reclamacion4))
         {
             $this->fecha_reclamacion4 = sc_convert_encoding($this->fecha_reclamacion4, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->fecha_reclamacion4 = str_replace('<', '&lt;', $this->fecha_reclamacion4);
         $this->fecha_reclamacion4 = str_replace('>', '&gt;', $this->fecha_reclamacion4);
         $this->Texto_tag .= "<td>" . $this->fecha_reclamacion4 . "</td>\r\n";
   }
   //----- motivo_no_reclamacion4
   function NM_export_motivo_no_reclamacion4()
   {
         $this->motivo_no_reclamacion4 = html_entity_decode($this->motivo_no_reclamacion4, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->motivo_no_reclamacion4 = strip_tags($this->motivo_no_reclamacion4);
         if (!NM_is_utf8($this->motivo_no_reclamacion4))
         {
             $this->motivo_no_reclamacion4 = sc_convert_encoding($this->motivo_no_reclamacion4, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->motivo_no_reclamacion4 = str_replace('<', '&lt;', $this->motivo_no_reclamacion4);
         $this->motivo_no_reclamacion4 = str_replace('>', '&gt;', $this->motivo_no_reclamacion4);
         $this->Texto_tag .= "<td>" . $this->motivo_no_reclamacion4 . "</td>\r\n";
   }
   //----- mes5
   function NM_export_mes5()
   {
         $this->mes5 = html_entity_decode($this->mes5, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->mes5 = strip_tags($this->mes5);
         if (!NM_is_utf8($this->mes5))
         {
             $this->mes5 = sc_convert_encoding($this->mes5, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->mes5 = str_replace('<', '&lt;', $this->mes5);
         $this->mes5 = str_replace('>', '&gt;', $this->mes5);
         $this->Texto_tag .= "<td>" . $this->mes5 . "</td>\r\n";
   }
   //----- reclamo5
   function NM_export_reclamo5()
   {
         $this->reclamo5 = html_entity_decode($this->reclamo5, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->reclamo5 = strip_tags($this->reclamo5);
         if (!NM_is_utf8($this->reclamo5))
         {
             $this->reclamo5 = sc_convert_encoding($this->reclamo5, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->reclamo5 = str_replace('<', '&lt;', $this->reclamo5);
         $this->reclamo5 = str_replace('>', '&gt;', $this->reclamo5);
         $this->Texto_tag .= "<td>" . $this->reclamo5 . "</td>\r\n";
   }
   //----- fecha_reclamacion5
   function NM_export_fecha_reclamacion5()
   {
         $this->fecha_reclamacion5 = html_entity_decode($this->fecha_reclamacion5, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->fecha_reclamacion5 = strip_tags($this->fecha_reclamacion5);
         if (!NM_is_utf8($this->fecha_reclamacion5))
         {
             $this->fecha_reclamacion5 = sc_convert_encoding($this->fecha_reclamacion5, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->fecha_reclamacion5 = str_replace('<', '&lt;', $this->fecha_reclamacion5);
         $this->fecha_reclamacion5 = str_replace('>', '&gt;', $this->fecha_reclamacion5);
         $this->Texto_tag .= "<td>" . $this->fecha_reclamacion5 . "</td>\r\n";
   }
   //----- motivo_no_reclamacion5
   function NM_export_motivo_no_reclamacion5()
   {
         $this->motivo_no_reclamacion5 = html_entity_decode($this->motivo_no_reclamacion5, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->motivo_no_reclamacion5 = strip_tags($this->motivo_no_reclamacion5);
         if (!NM_is_utf8($this->motivo_no_reclamacion5))
         {
             $this->motivo_no_reclamacion5 = sc_convert_encoding($this->motivo_no_reclamacion5, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->motivo_no_reclamacion5 = str_replace('<', '&lt;', $this->motivo_no_reclamacion5);
         $this->motivo_no_reclamacion5 = str_replace('>', '&gt;', $this->motivo_no_reclamacion5);
         $this->Texto_tag .= "<td>" . $this->motivo_no_reclamacion5 . "</td>\r\n";
   }
   //----- mes6
   function NM_export_mes6()
   {
         $this->mes6 = html_entity_decode($this->mes6, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->mes6 = strip_tags($this->mes6);
         if (!NM_is_utf8($this->mes6))
         {
             $this->mes6 = sc_convert_encoding($this->mes6, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->mes6 = str_replace('<', '&lt;', $this->mes6);
         $this->mes6 = str_replace('>', '&gt;', $this->mes6);
         $this->Texto_tag .= "<td>" . $this->mes6 . "</td>\r\n";
   }
   //----- reclamo6
   function NM_export_reclamo6()
   {
         $this->reclamo6 = html_entity_decode($this->reclamo6, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->reclamo6 = strip_tags($this->reclamo6);
         if (!NM_is_utf8($this->reclamo6))
         {
             $this->reclamo6 = sc_convert_encoding($this->reclamo6, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->reclamo6 = str_replace('<', '&lt;', $this->reclamo6);
         $this->reclamo6 = str_replace('>', '&gt;', $this->reclamo6);
         $this->Texto_tag .= "<td>" . $this->reclamo6 . "</td>\r\n";
   }
   //----- fecha_reclamacion6
   function NM_export_fecha_reclamacion6()
   {
         $this->fecha_reclamacion6 = html_entity_decode($this->fecha_reclamacion6, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->fecha_reclamacion6 = strip_tags($this->fecha_reclamacion6);
         if (!NM_is_utf8($this->fecha_reclamacion6))
         {
             $this->fecha_reclamacion6 = sc_convert_encoding($this->fecha_reclamacion6, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->fecha_reclamacion6 = str_replace('<', '&lt;', $this->fecha_reclamacion6);
         $this->fecha_reclamacion6 = str_replace('>', '&gt;', $this->fecha_reclamacion6);
         $this->Texto_tag .= "<td>" . $this->fecha_reclamacion6 . "</td>\r\n";
   }
   //----- motivo_no_reclamacion6
   function NM_export_motivo_no_reclamacion6()
   {
         $this->motivo_no_reclamacion6 = html_entity_decode($this->motivo_no_reclamacion6, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->motivo_no_reclamacion6 = strip_tags($this->motivo_no_reclamacion6);
         if (!NM_is_utf8($this->motivo_no_reclamacion6))
         {
             $this->motivo_no_reclamacion6 = sc_convert_encoding($this->motivo_no_reclamacion6, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->motivo_no_reclamacion6 = str_replace('<', '&lt;', $this->motivo_no_reclamacion6);
         $this->motivo_no_reclamacion6 = str_replace('>', '&gt;', $this->motivo_no_reclamacion6);
         $this->Texto_tag .= "<td>" . $this->motivo_no_reclamacion6 . "</td>\r\n";
   }
   //----- mes7
   function NM_export_mes7()
   {
         $this->mes7 = html_entity_decode($this->mes7, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->mes7 = strip_tags($this->mes7);
         if (!NM_is_utf8($this->mes7))
         {
             $this->mes7 = sc_convert_encoding($this->mes7, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->mes7 = str_replace('<', '&lt;', $this->mes7);
         $this->mes7 = str_replace('>', '&gt;', $this->mes7);
         $this->Texto_tag .= "<td>" . $this->mes7 . "</td>\r\n";
   }
   //----- reclamo7
   function NM_export_reclamo7()
   {
         $this->reclamo7 = html_entity_decode($this->reclamo7, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->reclamo7 = strip_tags($this->reclamo7);
         if (!NM_is_utf8($this->reclamo7))
         {
             $this->reclamo7 = sc_convert_encoding($this->reclamo7, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->reclamo7 = str_replace('<', '&lt;', $this->reclamo7);
         $this->reclamo7 = str_replace('>', '&gt;', $this->reclamo7);
         $this->Texto_tag .= "<td>" . $this->reclamo7 . "</td>\r\n";
   }
   //----- fecha_reclamacion7
   function NM_export_fecha_reclamacion7()
   {
         $this->fecha_reclamacion7 = html_entity_decode($this->fecha_reclamacion7, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->fecha_reclamacion7 = strip_tags($this->fecha_reclamacion7);
         if (!NM_is_utf8($this->fecha_reclamacion7))
         {
             $this->fecha_reclamacion7 = sc_convert_encoding($this->fecha_reclamacion7, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->fecha_reclamacion7 = str_replace('<', '&lt;', $this->fecha_reclamacion7);
         $this->fecha_reclamacion7 = str_replace('>', '&gt;', $this->fecha_reclamacion7);
         $this->Texto_tag .= "<td>" . $this->fecha_reclamacion7 . "</td>\r\n";
   }
   //----- motivo_no_reclamacion7
   function NM_export_motivo_no_reclamacion7()
   {
         $this->motivo_no_reclamacion7 = html_entity_decode($this->motivo_no_reclamacion7, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->motivo_no_reclamacion7 = strip_tags($this->motivo_no_reclamacion7);
         if (!NM_is_utf8($this->motivo_no_reclamacion7))
         {
             $this->motivo_no_reclamacion7 = sc_convert_encoding($this->motivo_no_reclamacion7, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->motivo_no_reclamacion7 = str_replace('<', '&lt;', $this->motivo_no_reclamacion7);
         $this->motivo_no_reclamacion7 = str_replace('>', '&gt;', $this->motivo_no_reclamacion7);
         $this->Texto_tag .= "<td>" . $this->motivo_no_reclamacion7 . "</td>\r\n";
   }
   //----- mes8
   function NM_export_mes8()
   {
         $this->mes8 = html_entity_decode($this->mes8, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->mes8 = strip_tags($this->mes8);
         if (!NM_is_utf8($this->mes8))
         {
             $this->mes8 = sc_convert_encoding($this->mes8, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->mes8 = str_replace('<', '&lt;', $this->mes8);
         $this->mes8 = str_replace('>', '&gt;', $this->mes8);
         $this->Texto_tag .= "<td>" . $this->mes8 . "</td>\r\n";
   }
   //----- reclamo8
   function NM_export_reclamo8()
   {
         $this->reclamo8 = html_entity_decode($this->reclamo8, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->reclamo8 = strip_tags($this->reclamo8);
         if (!NM_is_utf8($this->reclamo8))
         {
             $this->reclamo8 = sc_convert_encoding($this->reclamo8, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->reclamo8 = str_replace('<', '&lt;', $this->reclamo8);
         $this->reclamo8 = str_replace('>', '&gt;', $this->reclamo8);
         $this->Texto_tag .= "<td>" . $this->reclamo8 . "</td>\r\n";
   }
   //----- fecha_reclamacion8
   function NM_export_fecha_reclamacion8()
   {
         $this->fecha_reclamacion8 = html_entity_decode($this->fecha_reclamacion8, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->fecha_reclamacion8 = strip_tags($this->fecha_reclamacion8);
         if (!NM_is_utf8($this->fecha_reclamacion8))
         {
             $this->fecha_reclamacion8 = sc_convert_encoding($this->fecha_reclamacion8, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->fecha_reclamacion8 = str_replace('<', '&lt;', $this->fecha_reclamacion8);
         $this->fecha_reclamacion8 = str_replace('>', '&gt;', $this->fecha_reclamacion8);
         $this->Texto_tag .= "<td>" . $this->fecha_reclamacion8 . "</td>\r\n";
   }
   //----- motivo_no_reclamacion8
   function NM_export_motivo_no_reclamacion8()
   {
         $this->motivo_no_reclamacion8 = html_entity_decode($this->motivo_no_reclamacion8, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->motivo_no_reclamacion8 = strip_tags($this->motivo_no_reclamacion8);
         if (!NM_is_utf8($this->motivo_no_reclamacion8))
         {
             $this->motivo_no_reclamacion8 = sc_convert_encoding($this->motivo_no_reclamacion8, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->motivo_no_reclamacion8 = str_replace('<', '&lt;', $this->motivo_no_reclamacion8);
         $this->motivo_no_reclamacion8 = str_replace('>', '&gt;', $this->motivo_no_reclamacion8);
         $this->Texto_tag .= "<td>" . $this->motivo_no_reclamacion8 . "</td>\r\n";
   }
   //----- mes9
   function NM_export_mes9()
   {
         $this->mes9 = html_entity_decode($this->mes9, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->mes9 = strip_tags($this->mes9);
         if (!NM_is_utf8($this->mes9))
         {
             $this->mes9 = sc_convert_encoding($this->mes9, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->mes9 = str_replace('<', '&lt;', $this->mes9);
         $this->mes9 = str_replace('>', '&gt;', $this->mes9);
         $this->Texto_tag .= "<td>" . $this->mes9 . "</td>\r\n";
   }
   //----- reclamo9
   function NM_export_reclamo9()
   {
         $this->reclamo9 = html_entity_decode($this->reclamo9, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->reclamo9 = strip_tags($this->reclamo9);
         if (!NM_is_utf8($this->reclamo9))
         {
             $this->reclamo9 = sc_convert_encoding($this->reclamo9, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->reclamo9 = str_replace('<', '&lt;', $this->reclamo9);
         $this->reclamo9 = str_replace('>', '&gt;', $this->reclamo9);
         $this->Texto_tag .= "<td>" . $this->reclamo9 . "</td>\r\n";
   }
   //----- fecha_reclamacion9
   function NM_export_fecha_reclamacion9()
   {
         $this->fecha_reclamacion9 = html_entity_decode($this->fecha_reclamacion9, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->fecha_reclamacion9 = strip_tags($this->fecha_reclamacion9);
         if (!NM_is_utf8($this->fecha_reclamacion9))
         {
             $this->fecha_reclamacion9 = sc_convert_encoding($this->fecha_reclamacion9, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->fecha_reclamacion9 = str_replace('<', '&lt;', $this->fecha_reclamacion9);
         $this->fecha_reclamacion9 = str_replace('>', '&gt;', $this->fecha_reclamacion9);
         $this->Texto_tag .= "<td>" . $this->fecha_reclamacion9 . "</td>\r\n";
   }
   //----- motivo_no_reclamacion9
   function NM_export_motivo_no_reclamacion9()
   {
         $this->motivo_no_reclamacion9 = html_entity_decode($this->motivo_no_reclamacion9, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->motivo_no_reclamacion9 = strip_tags($this->motivo_no_reclamacion9);
         if (!NM_is_utf8($this->motivo_no_reclamacion9))
         {
             $this->motivo_no_reclamacion9 = sc_convert_encoding($this->motivo_no_reclamacion9, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->motivo_no_reclamacion9 = str_replace('<', '&lt;', $this->motivo_no_reclamacion9);
         $this->motivo_no_reclamacion9 = str_replace('>', '&gt;', $this->motivo_no_reclamacion9);
         $this->Texto_tag .= "<td>" . $this->motivo_no_reclamacion9 . "</td>\r\n";
   }
   //----- mes10
   function NM_export_mes10()
   {
         $this->mes10 = html_entity_decode($this->mes10, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->mes10 = strip_tags($this->mes10);
         if (!NM_is_utf8($this->mes10))
         {
             $this->mes10 = sc_convert_encoding($this->mes10, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->mes10 = str_replace('<', '&lt;', $this->mes10);
         $this->mes10 = str_replace('>', '&gt;', $this->mes10);
         $this->Texto_tag .= "<td>" . $this->mes10 . "</td>\r\n";
   }
   //----- reclamo10
   function NM_export_reclamo10()
   {
         $this->reclamo10 = html_entity_decode($this->reclamo10, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->reclamo10 = strip_tags($this->reclamo10);
         if (!NM_is_utf8($this->reclamo10))
         {
             $this->reclamo10 = sc_convert_encoding($this->reclamo10, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->reclamo10 = str_replace('<', '&lt;', $this->reclamo10);
         $this->reclamo10 = str_replace('>', '&gt;', $this->reclamo10);
         $this->Texto_tag .= "<td>" . $this->reclamo10 . "</td>\r\n";
   }
   //----- fecha_reclamacion10
   function NM_export_fecha_reclamacion10()
   {
         $this->fecha_reclamacion10 = html_entity_decode($this->fecha_reclamacion10, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->fecha_reclamacion10 = strip_tags($this->fecha_reclamacion10);
         if (!NM_is_utf8($this->fecha_reclamacion10))
         {
             $this->fecha_reclamacion10 = sc_convert_encoding($this->fecha_reclamacion10, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->fecha_reclamacion10 = str_replace('<', '&lt;', $this->fecha_reclamacion10);
         $this->fecha_reclamacion10 = str_replace('>', '&gt;', $this->fecha_reclamacion10);
         $this->Texto_tag .= "<td>" . $this->fecha_reclamacion10 . "</td>\r\n";
   }
   //----- motivo_no_reclamacion10
   function NM_export_motivo_no_reclamacion10()
   {
         $this->motivo_no_reclamacion10 = html_entity_decode($this->motivo_no_reclamacion10, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->motivo_no_reclamacion10 = strip_tags($this->motivo_no_reclamacion10);
         if (!NM_is_utf8($this->motivo_no_reclamacion10))
         {
             $this->motivo_no_reclamacion10 = sc_convert_encoding($this->motivo_no_reclamacion10, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->motivo_no_reclamacion10 = str_replace('<', '&lt;', $this->motivo_no_reclamacion10);
         $this->motivo_no_reclamacion10 = str_replace('>', '&gt;', $this->motivo_no_reclamacion10);
         $this->Texto_tag .= "<td>" . $this->motivo_no_reclamacion10 . "</td>\r\n";
   }
   //----- mes11
   function NM_export_mes11()
   {
         $this->mes11 = html_entity_decode($this->mes11, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->mes11 = strip_tags($this->mes11);
         if (!NM_is_utf8($this->mes11))
         {
             $this->mes11 = sc_convert_encoding($this->mes11, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->mes11 = str_replace('<', '&lt;', $this->mes11);
         $this->mes11 = str_replace('>', '&gt;', $this->mes11);
         $this->Texto_tag .= "<td>" . $this->mes11 . "</td>\r\n";
   }
   //----- reclamo11
   function NM_export_reclamo11()
   {
         $this->reclamo11 = html_entity_decode($this->reclamo11, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->reclamo11 = strip_tags($this->reclamo11);
         if (!NM_is_utf8($this->reclamo11))
         {
             $this->reclamo11 = sc_convert_encoding($this->reclamo11, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->reclamo11 = str_replace('<', '&lt;', $this->reclamo11);
         $this->reclamo11 = str_replace('>', '&gt;', $this->reclamo11);
         $this->Texto_tag .= "<td>" . $this->reclamo11 . "</td>\r\n";
   }
   //----- fecha_reclamacion11
   function NM_export_fecha_reclamacion11()
   {
         $this->fecha_reclamacion11 = html_entity_decode($this->fecha_reclamacion11, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->fecha_reclamacion11 = strip_tags($this->fecha_reclamacion11);
         if (!NM_is_utf8($this->fecha_reclamacion11))
         {
             $this->fecha_reclamacion11 = sc_convert_encoding($this->fecha_reclamacion11, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->fecha_reclamacion11 = str_replace('<', '&lt;', $this->fecha_reclamacion11);
         $this->fecha_reclamacion11 = str_replace('>', '&gt;', $this->fecha_reclamacion11);
         $this->Texto_tag .= "<td>" . $this->fecha_reclamacion11 . "</td>\r\n";
   }
   //----- motivo_no_reclamacion11
   function NM_export_motivo_no_reclamacion11()
   {
         $this->motivo_no_reclamacion11 = html_entity_decode($this->motivo_no_reclamacion11, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->motivo_no_reclamacion11 = strip_tags($this->motivo_no_reclamacion11);
         if (!NM_is_utf8($this->motivo_no_reclamacion11))
         {
             $this->motivo_no_reclamacion11 = sc_convert_encoding($this->motivo_no_reclamacion11, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->motivo_no_reclamacion11 = str_replace('<', '&lt;', $this->motivo_no_reclamacion11);
         $this->motivo_no_reclamacion11 = str_replace('>', '&gt;', $this->motivo_no_reclamacion11);
         $this->Texto_tag .= "<td>" . $this->motivo_no_reclamacion11 . "</td>\r\n";
   }
   //----- mes12
   function NM_export_mes12()
   {
         $this->mes12 = html_entity_decode($this->mes12, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->mes12 = strip_tags($this->mes12);
         if (!NM_is_utf8($this->mes12))
         {
             $this->mes12 = sc_convert_encoding($this->mes12, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->mes12 = str_replace('<', '&lt;', $this->mes12);
         $this->mes12 = str_replace('>', '&gt;', $this->mes12);
         $this->Texto_tag .= "<td>" . $this->mes12 . "</td>\r\n";
   }
   //----- reclamo12
   function NM_export_reclamo12()
   {
         $this->reclamo12 = html_entity_decode($this->reclamo12, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->reclamo12 = strip_tags($this->reclamo12);
         if (!NM_is_utf8($this->reclamo12))
         {
             $this->reclamo12 = sc_convert_encoding($this->reclamo12, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->reclamo12 = str_replace('<', '&lt;', $this->reclamo12);
         $this->reclamo12 = str_replace('>', '&gt;', $this->reclamo12);
         $this->Texto_tag .= "<td>" . $this->reclamo12 . "</td>\r\n";
   }
   //----- fecha_reclamacion12
   function NM_export_fecha_reclamacion12()
   {
         $this->fecha_reclamacion12 = html_entity_decode($this->fecha_reclamacion12, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->fecha_reclamacion12 = strip_tags($this->fecha_reclamacion12);
         if (!NM_is_utf8($this->fecha_reclamacion12))
         {
             $this->fecha_reclamacion12 = sc_convert_encoding($this->fecha_reclamacion12, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->fecha_reclamacion12 = str_replace('<', '&lt;', $this->fecha_reclamacion12);
         $this->fecha_reclamacion12 = str_replace('>', '&gt;', $this->fecha_reclamacion12);
         $this->Texto_tag .= "<td>" . $this->fecha_reclamacion12 . "</td>\r\n";
   }
   //----- motivo_no_reclamacion12
   function NM_export_motivo_no_reclamacion12()
   {
         $this->motivo_no_reclamacion12 = html_entity_decode($this->motivo_no_reclamacion12, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->motivo_no_reclamacion12 = strip_tags($this->motivo_no_reclamacion12);
         if (!NM_is_utf8($this->motivo_no_reclamacion12))
         {
             $this->motivo_no_reclamacion12 = sc_convert_encoding($this->motivo_no_reclamacion12, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->motivo_no_reclamacion12 = str_replace('<', '&lt;', $this->motivo_no_reclamacion12);
         $this->motivo_no_reclamacion12 = str_replace('>', '&gt;', $this->motivo_no_reclamacion12);
         $this->Texto_tag .= "<td>" . $this->motivo_no_reclamacion12 . "</td>\r\n";
   }

   //----- 
   function grava_arquivo_rtf()
   {
      global $nm_lang, $doc_wrap;
      $rtf_f = fopen($this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo, "w");
      require_once($this->Ini->path_third      . "/rtf_new/document_generator/cl_xml2driver.php"); 
      $text_ok  =  "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\r\n"; 
      $text_ok .=  "<DOC config_file=\"" . $this->Ini->path_third . "/rtf_new/doc_config.inc\" >\r\n"; 
      $text_ok .=  $this->Texto_tag; 
      $text_ok .=  "</DOC>\r\n"; 
      $xml = new nDOCGEN($text_ok,"RTF"); 
      fwrite($rtf_f, $xml->get_result_file());
      fclose($rtf_f);
   }

   function nm_conv_data_db($dt_in, $form_in, $form_out)
   {
       $dt_out = $dt_in;
       if (strtoupper($form_in) == "DB_FORMAT")
       {
           if ($dt_out == "null" || $dt_out == "")
           {
               $dt_out = "";
               return $dt_out;
           }
           $form_in = "AAAA-MM-DD";
       }
       if (strtoupper($form_out) == "DB_FORMAT")
       {
           if (empty($dt_out))
           {
               $dt_out = "null";
               return $dt_out;
           }
           $form_out = "AAAA-MM-DD";
       }
       nm_conv_form_data($dt_out, $form_in, $form_out);
       return $dt_out;
   }
   //---- 
   function monta_html()
   {
      global $nm_url_saida, $nm_lang;
      include($this->Ini->path_btn . $this->Ini->Str_btn_grid);
      unset($_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_crs_historial_reclamacion']['rtf_file']);
      if (is_file($this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo))
      {
          $_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_crs_historial_reclamacion']['rtf_file'] = $this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo;
      }
      $path_doc_md5 = md5($this->Ini->path_imag_temp . "/" . $this->Arquivo);
      $_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_crs_historial_reclamacion'][$path_doc_md5][0] = $this->Ini->path_imag_temp . "/" . $this->Arquivo;
      $_SESSION['sc_session'][$this->Ini->sc_page]['grid_bayer_crs_historial_reclamacion'][$path_doc_md5][1] = $this->Tit_doc;
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
            "http://www.w3.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML<?php echo $_SESSION['scriptcase']['reg_conf']['html_dir'] ?>>
<HEAD>
 <TITLE><?php echo $this->Ini->Nm_lang['lang_othr_grid_titl'] ?> -  :: RTF</TITLE>
 <META http-equiv="Content-Type" content="text/html; charset=<?php echo $_SESSION['scriptcase']['charset_html'] ?>" />
<?php
if ($_SESSION['scriptcase']['proc_mobile'])
{
?>
  <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<?php
}
?>
  <META http-equiv="Expires" content="Fri, Jan 01 1900 00:00:00 GMT"/>
  <META http-equiv="Last-Modified" content="<?php echo gmdate("D, d M Y H:i:s"); ?> GMT"/>
  <META http-equiv="Cache-Control" content="no-store, no-cache, must-revalidate"/>
  <META http-equiv="Cache-Control" content="post-check=0, pre-check=0"/>
  <META http-equiv="Pragma" content="no-cache"/>
  <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_all ?>_export.css" /> 
  <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_all ?>_export<?php echo $_SESSION['scriptcase']['reg_conf']['css_dir'] ?>.css" /> 
  <link rel="stylesheet" type="text/css" href="../_lib/buttons/<?php echo $this->Ini->Str_btn_css ?>" /> 
</HEAD>
<BODY class="scExportPage">
<?php echo $this->Ini->Ajax_result_set ?>
<table style="border-collapse: collapse; border-width: 0; height: 100%; width: 100%"><tr><td style="padding: 0; text-align: center; vertical-align: middle">
 <table class="scExportTable" align="center">
  <tr>
   <td class="scExportTitle" style="height: 25px">RTF</td>
  </tr>
  <tr>
   <td class="scExportLine" style="width: 100%">
    <table style="border-collapse: collapse; border-width: 0; width: 100%"><tr><td class="scExportLineFont" style="padding: 3px 0 0 0" id="idMessage">
    <?php echo $this->Ini->Nm_lang['lang_othr_file_msge'] ?>
    </td><td class="scExportLineFont" style="text-align:right; padding: 3px 0 0 0">
     <?php echo nmButtonOutput($this->arr_buttons, "bexportview", "document.Fview.submit()", "document.Fview.submit()", "idBtnView", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
     <?php echo nmButtonOutput($this->arr_buttons, "bdownload", "document.Fdown.submit()", "document.Fdown.submit()", "idBtnDown", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
     <?php echo nmButtonOutput($this->arr_buttons, "bvoltar", "document.F0.submit()", "document.F0.submit()", "idBtnBack", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
    </td></tr></table>
   </td>
  </tr>
 </table>
</td></tr></table>
<form name="Fview" method="get" action="<?php echo $this->Ini->path_imag_temp . "/" . $this->Arquivo ?>" target="_blank" style="display: none"> 
</form>
<form name="Fdown" method="get" action="grid_bayer_crs_historial_reclamacion_download.php" target="_blank" style="display: none"> 
<input type="hidden" name="script_case_init" value="<?php echo NM_encode_input($this->Ini->sc_page); ?>"> 
<input type="hidden" name="nm_tit_doc" value="grid_bayer_crs_historial_reclamacion"> 
<input type="hidden" name="nm_name_doc" value="<?php echo $path_doc_md5 ?>"> 
</form>
<FORM name="F0" method=post action="./"> 
<INPUT type="hidden" name="script_case_init" value="<?php echo NM_encode_input($this->Ini->sc_page); ?>"> 
<INPUT type="hidden" name="script_case_session" value="<?php echo NM_encode_input(session_id()); ?>"> 
<INPUT type="hidden" name="nmgp_opcao" value="volta_grid"> 
</FORM> 
</BODY>
</HTML>
<?php
   }
   function nm_gera_mask(&$nm_campo, $nm_mask)
   { 
      $trab_campo = $nm_campo;
      $trab_mask  = $nm_mask;
      $tam_campo  = strlen($nm_campo);
      $trab_saida = "";
      $mask_num = false;
      for ($x=0; $x < strlen($trab_mask); $x++)
      {
          if (substr($trab_mask, $x, 1) == "#")
          {
              $mask_num = true;
              break;
          }
      }
      if ($mask_num )
      {
          $ver_duas = explode(";", $trab_mask);
          if (isset($ver_duas[1]) && !empty($ver_duas[1]))
          {
              $cont1 = count(explode("#", $ver_duas[0])) - 1;
              $cont2 = count(explode("#", $ver_duas[1])) - 1;
              if ($cont2 >= $tam_campo)
              {
                  $trab_mask = $ver_duas[1];
              }
              else
              {
                  $trab_mask = $ver_duas[0];
              }
          }
          $tam_mask = strlen($trab_mask);
          $xdados = 0;
          for ($x=0; $x < $tam_mask; $x++)
          {
              if (substr($trab_mask, $x, 1) == "#" && $xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_campo, $xdados, 1);
                  $xdados++;
              }
              elseif ($xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_mask, $x, 1);
              }
          }
          if ($xdados < $tam_campo)
          {
              $trab_saida .= substr($trab_campo, $xdados);
          }
          $nm_campo = $trab_saida;
          return;
      }
      for ($ix = strlen($trab_mask); $ix > 0; $ix--)
      {
           $char_mask = substr($trab_mask, $ix - 1, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               $trab_saida = $char_mask . $trab_saida;
           }
           else
           {
               if ($tam_campo != 0)
               {
                   $trab_saida = substr($trab_campo, $tam_campo - 1, 1) . $trab_saida;
                   $tam_campo--;
               }
               else
               {
                   $trab_saida = "0" . $trab_saida;
               }
           }
      }
      if ($tam_campo != 0)
      {
          $trab_saida = substr($trab_campo, 0, $tam_campo) . $trab_saida;
          $trab_mask  = str_repeat("z", $tam_campo) . $trab_mask;
      }
   
      $iz = 0; 
      for ($ix = 0; $ix < strlen($trab_mask); $ix++)
      {
           $char_mask = substr($trab_mask, $ix, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               if ($char_mask == "." || $char_mask == ",")
               {
                   $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
               }
               else
               {
                   $iz++;
               }
           }
           elseif ($char_mask == "x" || substr($trab_saida, $iz, 1) != "0")
           {
               $ix = strlen($trab_mask) + 1;
           }
           else
           {
               $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
           }
      }
      $nm_campo = $trab_saida;
   } 
}

?>
