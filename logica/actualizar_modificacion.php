<?php
include('../logica/session.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Documento sin titulo</title>
	<style>
		.aviso3 {
			font-size: 130%;
			font-weight: bold;
			color: #11a9e3;
			text-transform: uppercase;
			background-color: transparent;
			text-align: center;
			padding: 10px;
		}

		.error {
			font-size: 130%;
			font-weight: bold;
			color: #fb8305;
			text-transform: uppercase;
			background-color: transparent;
			text-align: center;
			padding: 10px;
		}
	</style>
</head>

<body>
	<?php
	$string_intro = getenv("QUERY_STRING");
	parse_str($string_intro);
	require('../datos/conex.php');

	if (isset($_POST['gestion'])) {
		$gestion = $_POST['gestion'];
	} else {
		$gestion = '';
	}

	$id_gestion_ult = $_POST['id_gestion_ult'];
	$codigo_usuario2 = $_POST['codigo_usuario2'];
	$estado_paciente = $_POST['estado_paciente'];
	$status_paciente = 'N/A';
	$fecha_activacion = $_POST['fecha_activacion'];
	$fecha_retiro = $_POST['fecha_retiro'];
	$observacion_retiro = $_POST['observacion_retiro'];
	$examen = $_POST['examen_p'];
	$asegurador =  $_POST['asegurador'];
	$nombre = $_POST['nombre_medico'];
	$apellidos = $_POST['apellidos_medico'];
	$ini_pas = $_POST['ini_pas'];
	$correo = $_POST['correo_medico'];
	$identificacion = $_POST['identificacion_pa'];
	$telefono1 = $_POST['telefono1'];
	$pais = $_POST['pais'];
	$departamento = $_POST['departamento'];
	$ciudad = $_POST['ciudad'];
	$fecha_nacimiento = $_POST['fecha_nacimiento'];
	$fecha_proxima_llamada = $_POST['fecha_proxima_llamada'];
	$fecha_proxima_llamada_ant = $_POST['fecha_proxima_llamada_ant'];
	$descripcion_comunicacion = $_POST['descripcion_comunicacion'];
	$id_gestion_ult = $_POST['id_gestion_ult'];
	$tipo_identificacion = $_POST['tipo_documento_p'];
	$identificacion = $_POST['identificacion_pa'];
	$clasificacion = $_POST['clasificacion_patologica'];
	$tipo_pad = $_POST['tipo_pad'];
	$fecha_resultado = $_POST['fecha_resultado'];
	$fecha_entrega_bloque = $_POST['fecha_entrega_bloque'];
	$caso = $_POST['caso'];
	$documentos_recibidos_p = $_POST['documentos_recibidos_p'];

	if ($fecha_proxima_llamada < $fecha_proxima_llamada_ant) {
		$fecha_proxima_llamada;
		$fecha_proxima_llamada;
		$id_gestion_ult = $_POST['id_gestion_ult'];
		$sql = mysqli_query($conex, "UPDATE bayer_gestiones SET FECHA_PROXIMA_LLAMADA='" . $fecha_proxima_llamada . "',FECHA_PROGRAMADA_GESTION='" . $fecha_proxima_llamada . "' WHERE ID_GESTION='" . $id_gestion_ult . "'");
		echo mysqli_error($conex);
	}
	$id_gestion_ult;
	if (isset($_POST['registrar'])) {
		$select_historial = mysqli_query($conex, "SELECT * FROM bayer_historial_reclamacion WHERE ID_PACIENTE_FK='$codigo_usuario2'");
		echo mysqli_error($conex);
		$reg_hist = mysqli_num_rows($select_historial);

		$sql = mysqli_query($conex, "UPDATE bayer_pacientes SET ESTADO_PACIENTE='" . $estado_paciente . "', FECHA_ACTIVACION_PACIENTE='" . $fecha_activacion . "', FECHA_RETIRO_PACIENTE='" . $fecha_retiro . "', OBSERVACION_MOTIVO_RETIRO_PACIENTE='" . $observacion_retiro . "', IDENTIFICACION_PACIENTE='" . $identificacion . "' ,NOMBRE_PACIENTE ='" . $nombre . "',APELLIDO_PACIENTE='" . $apellidos . "',TELEFONO_PACIENTE='" . $telefono1 . "',INICIALES_PACIENTE='" . $ini_pas . "',CORREO_MEDICO='" . $correo . "',PAIS_PACIENTE='" . $pais . "', DEPARTAMENTO_PACIENTE='" . $departamento . "',CIUDAD_PACIENTE='" . $ciudad . "',TIPO_DE_IDENTIFICACION='" . $tipo_identificacion . "',FECHA_NACIMINETO_PACIENTE='" . $fecha_nacimiento . "' WHERE ID_PACIENTE='" . $codigo_usuario2 . "'");
		echo mysqli_error($conex);

		$sql = mysqli_query($conex, "UPDATE bayer_tratamiento SET CLASIFICACION_PATOLOGICA_TRATAMIENTO='" . $clasificacion . "',ASEGURADOR_TRATAMIENTO='" . $asegurador . "', EXAMEN='" . $examen . "' WHERE ID_PACIENTE_FK='" . $codigo_usuario2 . "'");
		echo mysqli_error($conex);

		$sql = mysqli_query($conex, "UPDATE bayer_gestiones SET FECHA_PROXIMA_LLAMADA='" . $fecha_proxima_llamada . "',FECHA_PROGRAMADA_GESTION='" . $fecha_proxima_llamada . "', ID_PACIENTE_FK2='" . $codigo_usuario2 . "',TIPO_PAD='" . $tipo_pad . "',FECHA_RESULTADO='" . $fecha_resultado . "',FECHA_ENTREGA_BLOQUES='" . $fecha_entrega_bloque . "',DOCUMENTOS_RECIBIDOS='" . $documentos_recibidos_p . "',CASO='" . $caso . "' WHERE ID_GESTION='" . $id_gestion_ult . "'");
		echo mysqli_error($conex);

		$crear = $_POST['crear'];
		$descripcion_nuevo_comunicacion = $_POST['descripcion_nuevo_comunicacion'];
		if ($crear == 'NO' || $crear == '') {
			$sql = mysqli_query($conex, "UPDATE bayer_gestiones SET DESCRIPCION_COMUNICACION_GESTION='" . $descripcion_comunicacion . "' WHERE ID_GESTION='" . $id_gestion_ult . "'");
			echo mysqli_error($conex);
		}
		if ($crear == 'SI') {
			$insert_gestion = mysqli_query($conex, "INSERT INTO bayer_gestiones (MOTIVO_COMUNICACION_GESTION,AUTOR_GESTION,NOTA,DESCRIPCION_COMUNICACION_GESTION,ID_PACIENTE_FK2,FECHA_COMUNICACION)VALUES('CREACION GESTION FUNDEM',	'" . $usuname_peru . "','CREACION GESTION FUNDEM','" . $descripcion_nuevo_comunicacion . "','" . $codigo_usuario2 . "',CURRENT_TIMESTAMP)");
		}

		if ($sql) {
	?>
			<span style="margin-top:5%;">
				<center>
					<img src="../presentacion/imagenes/chulo.png" width="118" height="117" style="width:100px; margin-top:100px;margin-top:5%;" />
				</center>
			</span>
			<p class="aviso3" style=" width:68.9%; margin:auto auto;">SE ACTUALIZARON LOS DATOS SATISFACTORIAMENTE.</p>
			<br />
			<br />
			<center>
				<a href="../presentacion/form_paciente_seguimiento.php" target="info" class="btn_continuar"><img src="../presentacion/imagenes/BTN_CONTINUAR2.png" style="width:152px; height:37px" /></a>
			</center>
			<br />
		<?php
		} else {
		?>
			<span style="margin-top:5%;">
				<center>
					<img src="../presentacion/imagenes/advertencia.png" width="118" height="117" style="width:100px; margin-top:100px;margin-top:5%;" />
				</center>
			</span>
			<p class="error" style=" width:68.9%; margin:auto auto;">NO SE HAN ACTUALIZADO LOS DATOS.</p>
			<br />
			<br />
			<center>
				<a href="javascript:history.go(-1)" target="info" class="btn_continuar"><img src="../presentacion/imagenes/BOTON_REGISTRAR_NARANJA.png" style="width:152px; height:37px" /></a>
			</center>
			<br />
	<?php
		}
	}
