<?php
error_reporting(0);
require_once('session.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>insertar</title>
	<style>
		.aviso3 {
			font-size: 130%;
			font-weight: bold;
			color: #11a9e3;
			text-transform: uppercase;
			background-color: transparent;
			text-align: center;
			padding: 10px;
		}

		.error {
			font-size: 130%;
			font-weight: bold;
			color: #fb8305;
			text-transform: uppercase;
			background-color: transparent;
			text-align: center;
			padding: 10px;
		}

		.btn_continuar {
			padding-top: 7px;
			width: 152px;
			height: 37px;
			color: transparent;
			background-color: transparent;
			border-radius: 5px;
			border: 1px solid transparent;
		}

		.btn_continuar:active {
			box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.2);
			box-shadow: 0px 0px 30px rgba(0, 0, 0, 0.3),
				inset 0px 0px 20px #EEECEC;
		}

		.btn_continuar:hover {
			box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.2);
			box-shadow: 0px 0px 30px rgba(0, 0, 0, 0.3),
				inset 0px 0px 20px #EEECEC;
		}
	</style>
</head>

<body>
	<?PHP
	$string_intro = getenv("QUERY_STRING");
	parse_str($string_intro);

	require_once("../datos/conex.php");
	mysqli_query($conex, "SET NAMES utf8");
	if (isset($_POST['registrar'])) {
		$estado_paciente = $_POST['estado_paciente'];
		$status_paciente = 'N/A';
		$fecha_activacion = $_POST['fecha_activacion'];
		$nombre = $_POST['nombre_medico'];
		$apellidos = $_POST['apellidos_medico'];
		$identificacion = $_POST['identificacion_pa'];
		$telefono1 = $_POST['telefono'];
		$ini_pas = $_POST['ini_pas'];
		$correo = $_POST['correo_medico'];
		$tipo_documento_p = $_POST['tipo_documento_p'];
		$fecha_nacimiento = $_POST['fecha_nacimiento_pa'];
		$ips_atiende = 'ips_atiende_pa';
		$especialidad = $_POST['especialidad'];
		$nota = $_POST['nota'];
		$pais = $_POST['pais'];
		$departamento = $_POST['departamento'];
		$ciudad = $_POST['ciudad'];
		$clasificacion_patologica = $_POST['clasificacion_patologica'];
		$consentimiento = $_POST['consentimiento'];
		$regimen = $_POST['regimen'];
		$examen = $_POST['examen_p'];
		$tipo_adjunto = $_POST['tipo_adjunto'];
		$asegurador = $_POST['asegurador'];
		$usuname = $_POST['usuname'];
		$laboratorio_diagnostico = $_POST['laboratorio_diagnostico'];
		$laboratorio_patologico = $_POST['laboratorio_patologico'];
		$fecha_proxima_llamada = $_POST['fecha_proxima_llamada'];
		$tipo_pad = $_POST['tipo_pad'];
		$fecha_resultado = $_POST['fecha_resultado'];
		$fecha_entrega_bloque = $_POST['fecha_entrega_bloque'];
		$caso = $_POST['caso'];
		$documentos_recibidos_p = $_POST['documentos_recibidos_p'];
		echo $examen;
		echo $documentos_recibidos_p;
		$insertar = mysqli_query($conex, "INSERT INTO bayer_pacientes(ESTADO_PACIENTE,STATUS_PACIENTE,FECHA_ACTIVACION_PACIENTE,IDENTIFICACION_PACIENTE,NOMBRE_PACIENTE,
		APELLIDO_PACIENTE,TELEFONO_PACIENTE,TELEFONO2_PACIENTE,INICIALES_PACIENTE,CORREO_MEDICO,DIRECCION_PACIENTE,PAIS_PACIENTE,DEPARTAMENTO_PACIENTE,CIUDAD_PACIENTE,TIPO_DE_IDENTIFICACION,FECHA_NACIMINETO_PACIENTE,ACUDIENTE_PACIENTE,TELEFONO_ACUDIENTE_PACIENTE,USUARIO_CREACION)
		VALUES ('" . $estado_paciente . "','" . $status_paciente . "','" . $fecha_activacion . "','" . $identificacion . "','" . $nombre . "','" . $apellidos . "','" . $telefono1 . "','" . $telefono2 . "','" . $ini_pas . "','" . $correo . "','" . $direccion . "','" . $pais . "','" . $departamento . "','" . $ciudad . "','" . $tipo_documento_p . "','" . $fecha_nacimiento . "','" . $acudiente . "','" . $telefono_acudiente . "','" . $usuname . "')");
		echo mysqli_error($conex);
		if ($insertar) {

			$select_paciente = mysqli_query($conex, "SELECT ID_PACIENTE FROM bayer_pacientes ORDER BY ID_PACIENTE DESC LIMIT 1");
			while ($dato = mysqli_fetch_array($select_paciente)) {
				$ID_PACIENTE = $dato['ID_PACIENTE'];
			}
			$insert_trt = mysqli_query($conex, "INSERT INTO bayer_tratamiento(PRODUCTO_TRATAMIENTO,NOMBRE_REFERENCIA,CLASIFICACION_PATOLOGICA_TRATAMIENTO, CONSENTIMIENTO_TRATAMIENTO,FECHA_INICIO_TERAPIA_TRATAMIENTO,REGIMEN_TRATAMIENTO,ASEGURADOR_TRATAMIENTO,OPERADOR_LOGISTICO_TRATAMIENTO, EXAMEN, FECHA_ULTIMA_RECLAMACION_TRATAMIENTO,MEDIOS_ADQUISICION_TRATAMIENTO,IPS_ATIENDE_TRATAMIENTO,MEDICO_TRATAMIENTO,ESPECIALIDAD_TRATAMIENTO, ZONA_ATENCION_PARAMEDICO_TRATAMIENTO,CIUDAD_BASE_PARAMEDICO_TRATAMIENTO,NOTAS_ADJUNTOS_TRATAMIENTO,ID_PACIENTE_FK)
			VALUES ('" . $producto_tratamiento . "','" . $producto_tratamiento . "','" . $clasificacion_patologica . "', '" . $consentimiento . "','" . $fecha_inicio_trt . "','" . $regimen . "','" . $asegurador . "','" . $operador_logistico . "', '" . $examen . "', '" . $fecha_ultima_reclamacion . "','" . $medio_adquision . "','" . $ips_atiende . "','" . $medico . "','" . $especialidad . "','" . $zona_atencion . "','" . $ciudad_base . "','" . $nota . "','" . $ID_PACIENTE . "')");
			echo mysqli_error($conex);
			if ($insert_trt) {
				$nombre_completo = $nombre . ' ' . $apellidos;
				$insert_gestion = mysqli_query($conex, "INSERT INTO bayer_gestiones (MOTIVO_COMUNICACION_GESTION,LOGRO_COMUNICACION_GESTION, FECHA_PROXIMA_LLAMADA,AUTOR_GESTION,NOTA,DESCRIPCION_COMUNICACION_GESTION,FECHA_PROGRAMADA_GESTION,ID_PACIENTE_FK2,FECHA_COMUNICACION,TIPO_ADJUNTO, CENTRO_DIAGNOSTICO, CENTRO_PATOLOGICO,TIPO_PAD,FECHA_RESULTADO,FECHA_ENTREGA_BLOQUES,DOCUMENTOS_RECIBIDOS,CASO)VALUES('Ingreso','SI','" . $fecha_proxima_llamada . "','" . $usuname . "','" . $nota . "','" . $nota . "','" . $fecha_proxima_llamada . "','" . $ID_PACIENTE . "',CURRENT_TIMESTAMP,'" . $tipo_adjunto . "', '" . $laboratorio_diagnostico . "','" . $laboratorio_patologico . "','" . $tipo_pad . "','" . $fecha_resultado . "','" . $fecha_entrega_bloque . "','" . $documentos_recibidos_p . "','" . $caso . "')");
				echo mysqli_error($conex);
				$ID_PACIENTE;
				$select_gestion = mysqli_query($conex, "SELECT * FROM bayer_gestiones WHERE ID_PACIENTE_FK2='" . $ID_PACIENTE . "' ORDER BY ID_GESTION DESC LIMIT 1");
				while ($datos_gestion = mysqli_fetch_array($select_gestion)) {
					$ID_ULTIMA_GESTION = $datos_gestion['ID_GESTION'];
				}
				$update_codigo_gestion = mysqli_query($conex, "UPDATE bayer_pacientes SET ID_ULTIMA_GESTION='" . $ID_ULTIMA_GESTION . "' WHERE ID_PACIENTE='" . $ID_PACIENTE . "'");
				echo mysqli_error($conex);
				if ($_FILES['archivo']["error"] > 0) {
				} else {
					$SELECT_GES = mysqli_query($conex, "SELECT ID_GESTION FROM bayer_gestiones ORDER BY ID_GESTION DESC LIMIT 1");
					while ($fila2 = mysqli_fetch_array($SELECT_GES)) {
						$ID_GES = $fila2['ID_GESTION'];
					}
					$CARPETA = "../ADJUNTOS_BAYER/$ID_GES";
					if (!is_dir($CARPETA)) {
						mkdir("../ADJUNTOS_BAYER/$ID_GES", 0777);
					}
					move_uploaded_file($_FILES['archivo']['tmp_name'], "../ADJUNTOS_BAYER/$ID_GES/" . $_FILES['archivo']['name']);
				}
				if ($insert_gestion) {
					include("../presentacion/email/mail_paciente_nuevo.php");
	?>
					<span style="margin-top:5%;">
						<center>
							<img src="../presentacion/imagenes/chulo.png" width="118" height="117" style="width:100px; margin-top:100px;margin-top:5%;" />
						</center>
					</span>
					<p class="aviso3" style=" width:68.9%; margin:auto auto;">HA REGISTRADO AL PACIENTE CORRECTAMENTE.</p>
					<br />
					<br />
					<center>
						<a href="../presentacion/form_paciente_nuevo.php" target="info" class="btn_continuar"><img src="../presentacion/imagenes/BTN_CONTINUAR2.png" style="width:152px; height:37px" /></a>
					</center>
				<?php
				} else {
				?>
					<span style="margin-top:5%;">
						<center>
							<img src="../presentacion/imagenes/advertencia.png" style="width:50px; margin-top:100px;margin-top:5%;" />
						</center>
					</span>
					<p class="error" style=" width:68.9%; margin:auto auto;">

						<span style="border-left-color:red">ERROR EN GESTI&Oacute;N.</span>
					</p>
					<br />
					<br />
					<center>
						<a href="javascript:history.go(-1)" target="info" class="btn_continuar"><img src="../presentacion/imagenes/BOTON_REGISTRAR_NARANJA.png" style="width:152px; height:37px" /></a>
					</center>
				<?php
				}
			} else {
				?>
				<span style="margin-top:5%;">
					<center>
						<img src="../presentacion/imagenes/advertencia.png" style="width:50px; margin-top:100px;margin-top:5%;" />
					</center>
				</span>
				<p class="error" style=" width:68.9%; margin:auto auto;">

					<span style="border-left-color:red">ERROR EN INFORMACI&Oacute;N ACERCA DEL TRATAMIENTO.</span>
				</p>
				<br />
				<br />
				<center>
					<a href="javascript:history.go(-1)" target="info" class="btn_continuar"><img src="../presentacion/imagenes/BOTON_REGISTRAR_NARANJA.png" style="width:152px; height:37px" /></a>
				</center>
			<?php
			}
		} else {
			?>
			<span style="margin-top:5%;">
				<center>
					<img src="../presentacion/imagenes/advertencia.png" style="width:50px; margin-top:100px;margin-top:5%;" />
				</center>
			</span>
			<p class="error" style=" width:68.9%; margin:auto auto;">

				<span style="border-left-color:red">ERROR. VERIFIQUE LOS DATOS REGISTRADOS.</span>
			</p>
			<br />
			<br />
			<center>
				<a href="javascript:history.go(-1)" target="info" class="btn_continuar"><img src="../presentacion/imagenes/BOTON_REGISTRAR_NARANJA.png" style="width:152px; height:37px" /></a>
			</center>
			<?php
		}
		$codigo_usuario2 = $ID_PACIENTE;
		$select_temporal = mysqli_query($conex, "SELECT * FROM bayer_temporal_producto WHERE ID_PACIENTE_FK='" . $codigo_usuario2 . "'");
		$nreg = mysqli_num_rows($select_temporal);
		if ($nreg > 0) {
			while ($datos_temporales = (mysqli_fetch_array($select_temporal))) {
				$tipo_envio = $datos_temporales['ID_REFERENCIA_FK'];
				$verificar_cantidad = mysqli_query($conex, "SELECT * FROM bayer_referencia WHERE CANTIDAD>0 AND ID_REFERENCIA='$tipo_envio'");
				echo mysqli_error($conex);
				$cantidad = mysqli_num_rows($verificar_cantidad);
				if ($cantidad > 0) {
					$SELECT_ID_INV = mysqli_query($conex, "SELECT ID_INVENTARIO from bayer_inventario WHERE LUGAR_MATERIAL='BODEGA' AND ID_REFERENCIA_FK='" . $tipo_envio . "' ORDER BY ID_INVENTARIO ASC LIMIT 1");
					echo mysqli_error($conex);
					while ($fila1 = mysqli_fetch_array($SELECT_ID_INV)) {
						$ID_ULT_INV = $fila1['ID_INVENTARIO'];
					}
					$INSERT_MOVIMIENTO = mysqli_query($conex, "INSERT INTO bayer_movimientos(TIPO_MOVIMIENTO, NO_REMICION, CANTIDAD, RESPONSABLE, DESTINATARIO, DIRECCION_DESTINATARIO, CIUDAD_ENVIO, FECHA_MOVIMIENTO, OBSERVACIONES, ESTADO_MOVIMIENTO,ID_REFERENCIA_FK) VALUES('2', '', '1', '" . $usuname . "', '" . $nombre . ' ' . $apellidos . "', '" . $direccion . "', '" . $ciudad . "', CURRENT_TIMESTAMP, 'ENVIO PRODUCTO(S)', 'EN PROCESO','" . $tipo_envio . "')");
					echo mysqli_error($conex);
					$SELECT_CANTIDAD = mysqli_query($conex, "SELECT * FROM bayer_referencia WHERE ID_REFERENCIA = '" . $tipo_envio . "'");
					echo mysqli_error($conex);

					while ($fila1 = mysqli_fetch_array($SELECT_CANTIDAD)) {
						$CANTIDAD_I = $fila1['CANTIDAD'];
					}
					$TOTAL = $CANTIDAD_I - 1;

					$UPDATE_REFERENCIA = mysqli_query($conex, "UPDATE bayer_referencia SET CANTIDAD='" . $TOTAL . "' WHERE ID_REFERENCIA='" . $tipo_envio . "'");
					echo mysqli_error($conex);

					$SELECT_ID_MOVIMIENTO = mysqli_query($conex, "SELECT ID_MOVIMIENTOS FROM bayer_movimientos WHERE DESTINATARIO='" . $nombre . ' ' . $apellidos . "' AND TIPO_MOVIMIENTO='2' ORDER BY ID_MOVIMIENTOS DESC LIMIT 1");
					echo mysqli_error($conex);
					while ($fila_mov = mysqli_fetch_array($SELECT_ID_MOVIMIENTO)) {
						$ID_ULT_MOVIMIENTO = $fila_mov['ID_MOVIMIENTOS'];
					}

					$INSERT_MOVIMIENTO_PACIENTE = mysqli_query($conex, "INSERT INTO bayer_paciente_movimientos(ID_PACIENTE_FK,ID_MOVIMIENTOS_FK, ESTADO_PACIENTE_MOVIMIENTO)VALUES('" . $codigo_usuario2 . "','" . $ID_ULT_MOVIMIENTO . "','EN PROCESO')");
					echo mysqli_error($conex);

					$INSERT_MOVIMIENTO_USUARIO = mysqli_query($conex, "INSERT INTO bayer_usuario_movimientos(ID_USUARIO_FK,ID_MOVIMIENTOS_FK)VALUES('" . $id_usu . "','" . $ID_ULT_MOVIMIENTO . "')");
					echo mysqli_error($conex);
					$verificar_cantidad = mysqli_query($conex, "SELECT * FROM bayer_referencia WHERE ID_REFERENCIA='" . $tipo_envio . "' AND CANTIDAD<STOCK_MINIMO");
					echo mysqli_error($conex);
					$nreg_vrf = mysqli_num_rows($verificar_cantidad);
			?>
					<table style="margin:auto auto; font-size:80%;">
						<?php
						if ($nreg_vrf > 0) {

							while ($daro_ref = mysqli_fetch_array($verificar_cantidad)) {
								$MATERIAL = $daro_ref['MATERIAL'];
						?>
								<tr align="left">
									<td align="left">
										<span class="error" style="font-size:100%; text-align:left">ADVERTENCIA SE ESTA AGOTANDO EL PRODUCTO <?php echo $MATERIAL ?>
										</span>
									</td>
								</tr>
							<?php

							}
						}
					} else {
						$verificar_cantidad = mysqli_query($conex, "SELECT * FROM bayer_referencia WHERE ID_REFERENCIA='" . $tipo_envio . "'");
						echo mysqli_error($conex);
						while ($cantidad = mysqli_fetch_array($verificar_cantidad)) {
							$nombre_producto = $cantidad['MATERIAL'];
							?>
							<tr align="left">
								<td align="left">
									<span style="margin-top:3%;">
										<center>
											<img src="../presentacion/imagenes/advertencia2.png" width="52" height="50" style=" margin-top:100px;margin-top:5%;" />
										</center>
									</span>
									<p class="error" style=" width:68.9%; margin:auto auto; font-size:80%;color:#F00; font-weight:bold">EL PRODUCTO &nbsp;&nbsp; <span style=""><?php echo $nombre_producto ?></span> &nbsp;&nbsp; ESTA AGOTADO POR FAVOR COMUNICARSE CON EL COORDINADOR.</p>
									<br />
									<br />
									<br />
								</td>
							</tr>
					<?php
						}
					}
				}
				if ($nreg_vrf > 0) {
					?>
					<tr>
						<td align="center">
							<span class="error" style="font-size:100%; ">POR FAVOR COMUNICARSE CON EL COORDINADOR.</span>
							<span>
								<center>
									<img src="../presentacion/imagenes/advertencia.png" width="52" height="50" style=" margin-top:100px;margin-top:5%;" />
								</center>
							</span>
						</td>
					</tr>
				<?php
				}
				?>
					</table>
					<?php
					$BORRAR_PRODUCTOS_TEMPORAL = mysqli_query($conex, "DELETE  FROM bayer_temporal_producto WHERE ID_PACIENTE_FK='" . $codigo_usuario2 . "'");
					echo mysqli_error($conex);
				} else {

					$tipo_envio = $_POST['tipo_envio'];
					if ($tipo_envio == 'Kit de bienvenida') {
						$listado_envio = mysqli_query($conex, "SELECT MATERIAL,ID_REFERENCIA FROM bayer_referencia WHERE ID_REFERENCIA='" . $tipo_envio . "'");
						while ($opcion = mysqli_fetch_array($listado_envio)) {
							$nombre_producto = $opcion['MATERIAL'];
						}
						if ($nombre_producto == 'Kit de bienvenida') {
							$tipo_envio = $_POST['tipo_envio'];
							$verificar_cantidad = mysqli_query($conex, "SELECT * FROM bayer_referencia WHERE CANTIDAD>0 AND ID_REFERENCIA='$tipo_envio'");
							echo mysqli_error($conex);
							$cantidad_ref = mysqli_num_rows($verificar_cantidad);
							if ($cantidad_ref > 0) {
								$verificar_cantidad = mysqli_query($conex, "SELECT * FROM bayer_referencia WHERE CANTIDAD>0 AND ID_REFERENCIA='$tipo_envio'");
								echo mysqli_error($conex);
								$cantidad = mysqli_num_rows($verificar_cantidad);
								if ($cantidad > 0) {
									$SELECT_ID_INV = mysqli_query($conex, "SELECT ID_INVENTARIO from bayer_inventario WHERE LUGAR_MATERIAL='BODEGA' AND ID_REFERENCIA_FK='" . $tipo_envio . "' ORDER BY ID_INVENTARIO ASC LIMIT 1");
									echo mysqli_error($conex);
									while ($fila1 = mysqli_fetch_array($SELECT_ID_INV)) {
										$ID_ULT_INV = $fila1['ID_INVENTARIO'];
									}
									$INSERT_MOVIMIENTO = mysqli_query($conex, "INSERT INTO bayer_movimientos(TIPO_MOVIMIENTO, NO_REMICION, CANTIDAD, RESPONSABLE, DESTINATARIO, DIRECCION_DESTINATARIO, CIUDAD_ENVIO, FECHA_MOVIMIENTO, OBSERVACIONES, ESTADO_MOVIMIENTO,ID_REFERENCIA_FK) VALUES('2', '', '1', '" . $usuname . "', '" . $nombre . ' ' . $apellidos . "', '" . $direccion . "', '" . $ciudad . "', CURRENT_TIMESTAMP, 'ENVIO PRODUCTO(S)', 'EN PROCESO','" . $tipo_envio . "')");
									echo mysqli_error($conex);
									$SELECT_CANTIDAD = mysqli_query($conex, "SELECT * FROM bayer_referencia WHERE ID_REFERENCIA = '" . $tipo_envio . "'");
									echo mysqli_error($conex);

									while ($fila1 = mysqli_fetch_array($SELECT_CANTIDAD)) {
										$CANTIDAD_I = $fila1['CANTIDAD'];
									}
									$TOTAL = $CANTIDAD_I - 1;

									$UPDATE_REFERENCIA = mysqli_query($conex, "UPDATE bayer_referencia SET CANTIDAD='" . $TOTAL . "' WHERE ID_REFERENCIA='" . $tipo_envio . "'");
									echo mysqli_error($conex);

									$SELECT_ID_MOVIMIENTO = mysqli_query($conex, "SELECT ID_MOVIMIENTOS FROM bayer_movimientos WHERE DESTINATARIO='" . $nombre . ' ' . $apellidos . "' AND TIPO_MOVIMIENTO='2' ORDER BY ID_MOVIMIENTOS DESC LIMIT 1");
									echo mysqli_error($conex);
									while ($fila_mov = mysqli_fetch_array($SELECT_ID_MOVIMIENTO)) {
										$ID_ULT_MOVIMIENTO = $fila_mov['ID_MOVIMIENTOS'];
									}

									$INSERT_MOVIMIENTO_PACIENTE = mysqli_query($conex, "INSERT INTO bayer_paciente_movimientos(ID_PACIENTE_FK,ID_MOVIMIENTOS_FK,
						            ESTADO_PACIENTE_MOVIMIENTO)VALUES('" . $codigo_usuario2 . "','" . $ID_ULT_MOVIMIENTO . "','EN PROCESO')");
									echo mysqli_error($conex);

									$INSERT_MOVIMIENTO_USUARIO = mysqli_query($conex, "INSERT INTO bayer_usuario_movimientos(ID_USUARIO_FK,ID_MOVIMIENTOS_FK)VALUES('" . $id_usu . "','" . $ID_ULT_MOVIMIENTO . "')");
									echo mysqli_error($conex);

									$BORRAR_PRODUCTOS_TEMPORAL = mysqli_query($conex, "DELETE  FROM bayer_temporal_producto WHERE ID_PACIENTE_FK='" . $codigo_usuario2 . "'");
									echo mysqli_error($conex);
									$verificar_cantidad = mysqli_query($conex, "SELECT ID_REFERENCIA FROM bayer_referencia WHERE ID_REFERENCIA='" . $tipo_envio . "' AND CANTIDAD<STOCK_MINIMO");
									echo mysqli_error($conex);
									$nreg_vrf = mysqli_num_rows($verificar_cantidad);
									if ($nreg_vrf > 0) {
					?>
										<span style="margin-top:3%;">
											<center>
												<img src="../presentacion/imagenes/advertencia.png" width="52" height="50" style=" margin-top:100px;margin-top:5%;" />
											</center>
										</span>
										<p class="error" style=" width:68.9%; margin:auto auto; font-size:80%;">ADVERTENIA SE ESTA AGOTANDO EL PRODUCTO &nbsp;&nbsp; <span style="color:#F00; font-weight:bold"><?php echo $nombre_producto ?></span> &nbsp;&nbsp; POR FAVOR COMUNICARSE CON EL COORDINADOR.</p>
										<br />
										<br />
										<br />
									<?php
									}
								} else {
									?>
									<span style="margin-top:3%;">
										<center>
											<img src="../presentacion/imagenes/advertencia2.png" width="52" height="50" style=" margin-top:100px;margin-top:5%;" />
										</center>
									</span>
									<p class="error" style=" width:68.9%; margin:auto auto; font-size:80%;color:#F00; font-weight:bold">EL PRODUCTO &nbsp;&nbsp; <span style=""><?php echo $nombre_producto ?></span> &nbsp;&nbsp; ESTA AGOTADO POR FAVOR COMUNICARSE CON EL COORDINADOR.</p>
									<br />
									<br />
									<br />
								<?php
								}
							} else {
								$verificar_cantidad = mysqli_query($conex, "SELECT * FROM bayer_referencia WHERE ID_REFERENCIA='" . $tipo_envio . "'");
								echo mysqli_error($conex);
								while ($cantidad = mysqli_fetch_array($verificar_cantidad)) {
									$nombre_producto = $cantidad['MATERIAL'];
								?>
									<tr align="left">
										<td align="left">
											<span style="margin-top:3%;">
												<center>
													<img src="../presentacion/imagenes/advertencia2.png" width="52" height="50" style=" margin-top:100px;margin-top:5%;" />
												</center>
											</span>
											<p class="error" style=" width:68.9%; margin:auto auto; font-size:80%;color:#F00; font-weight:bold">EL PRODUCTO &nbsp;&nbsp; <span style=""><?php echo $nombre_producto ?></span> &nbsp;&nbsp; ESTA AGOTADO POR FAVOR COMUNICARSE CON EL COORDINADOR.</p>
											<br />
											<br />
											<br />
										</td>
									</tr>
						<?php
								}
							}
						}
					}
				}
				if ($tipo_envio == 'Kit de bienvenida' || $nreg > 0) {
					if (!$INSERT_MOVIMIENTO || !$UPDATE_REFERENCIA || !$INSERT_MOVIMIENTO_PACIENTE || !$INSERT_MOVIMIENTO_USUARIO) {
						?>
						<span style="margin-top:5%;">
							<center>
								<img src="../presentacion/imagenes/advertencia.png" width="118" height="117" style="width:100px; margin-top:100px;margin-top:5%;" />
							</center>
						</span>
						<p class="error" style=" width:68.9%; margin:auto auto;">
							LA SOLICITUD NO HA SIDO ENVIO CORRECTAMENTE.</p>
						<br />
						<br />
						<center>
							<a href="javascript:history.go(-1)" target="info" class="btn_continuar"><img src="../presentacion/imagenes/BOTON_REGISTRAR_NARANJA.png" style="width:152px; height:37px" /></a>
						</center>
						<br />
			<?php
					}
				}
			}
			?>
</body>

</html>