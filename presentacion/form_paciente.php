<?php
//require('./../datos/parse_str.php');
include('../logica/session.php')
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Documento sin titulo</title>
    <link type="text/css" rel="stylesheet" href="css/estilo_form_paciente.css" />
    <script src="css/SpryAssets/SpryAccordion.js" type="text/javascript"></script>
    <link href="css/SpryAssets/SpryAccordion.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery.js"></script>
    <script type="text/javascript" src="js/calcular_edad.js"></script>
    <script type="text/javascript" src="js/direccion.js"></script>
    <script type="text/javascript" src="js/validar_campos_pacientes.js"></script>
    <script type="text/javascript" src="js/validaciones.js"></script>
    <script type="text/javascript" src="js/validar_caracteres.js"></script>
    <script type="text/javascript" src="../presentacion/js/jquery.js"></script>
    <script type="text/javascript" src="../logica/js/validaciones.js"></script>
    <script type="text/javascript" src="../logica/js/validaciones_campos.js"></script>
    <link rel="stylesheet" type="text/css" href="multiple-select.css">
    <script src="multiple-select.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.rawgit.com/wenzhixin/multiple-select/e14b36de/multiple-select.css">
    <script src="https://cdn.rawgit.com/wenzhixin/multiple-select/e14b36de/multiple-select.js"></script>
    <style>
        td {
            padding: 3px;
            background-color: transparent;
        }

        .input__row {
            margin-top: 10px;
        }

        .upload {
            display: none;
        }

        .uploader {
            border: 1px solid #12a9e3;
            width: 300px;
            position: relative;
            height: 30px;
            display: flex;
        }

        .uploader .input-value {
            width: 250px;
            padding: 5px;
            overflow: hidden;
            text-overflow: ellipsis;
            line-height: 25px;
            font-family: sans-serif;
            font-size: 16px;
        }

        .uploader label {
            cursor: pointer;
            margin: 0;
            width: 30px;
            height: 30px;
            position: absolute;
            right: 0;
            background: #17a8e391 url('https://www.interactius.com/wp-content/uploads/2017/09/folder.png') no-repeat center;
        }
    </style>
    <script>
        function moveOption(e1, e2) {
            for (var i = 0; i < e1.options.length; i++) {
                if (e1.options[i].selected) {
                    var e = e1.options[i];
                    e2.options.add(new Option(e.text, e.value));
                    e1.remove(i);
                    i = i - 1
                }
            }
            document.getElementById('examen_p').value = getvalue(document.getElementById('examen'));
        }

        function moveOption2(e1, e2) {
            for (var i = 0; i < e1.options.length; i++) {
                if (e1.options[i].selected) {
                    var e = e1.options[i];
                    e2.options.add(new Option(e.text, e.value));
                    e1.remove(i);
                    i = i - 1
                }
            }
            document.getElementById('documentos_recibidos_p').value = getvalue(document.getElementById('documentos_recibidos'));
        }

        function getvalue(geto) {
            var resultArray = new Array();
            for (var i = 0; i < geto.options.length; i++) {
                resultArray.push(geto.options[i].value);
            }
            return resultArray.join();
        }
    </script>
    <script type="text/javascript">
        function trat_previo(sel) {
            if (sel.value == "Otro") {
                divC = document.getElementById("otro_tratamiento");
                divC.style.display = "";
            }
            if (sel.value != "Otro") {
                divC = document.getElementById("otro_tratamiento");
                divC.style.display = "none";
            }
        }

        function mostrar_departamento() {
            var pais = $('#pais').val();
            $("#departamento").html('<img src="imgagenes/cargando.gif" />');
            $.ajax({
                url: '../presentacion/departamentos.php',
                data: {
                    pais: pais,
                },
                type: 'post',
                beforeSend: function() {
                    $('#departamento').attr('disabled');
                    $("#departamento").html("Procesando, espere por favor" + '<img src="img/cargando.gif" />');
                },
                success: function(data) {
                    $('#departamento').removeAttr("disabled");
                    $('#departamento').html(data);
                }
            })
        }

        function mostrar_ciudades() {
            var departamento = $('#departamento').val();
            $("#ciudad").html('<img src="imgagenes/cargando.gif" />');
            $.ajax({
                url: '../presentacion/ciudades.php',
                data: {
                    dep: departamento,
                },
                type: 'post',
                beforeSend: function() {
                    $('#ciudad').attr('disabled');
                    $("#ciudad").html("Procesando, espere por favor" + '<img src="img/cargando.gif" />');
                },
                success: function(data) {
                    $('#ciudad').removeAttr("disabled");
                    $('#ciudad').html(data);
                }
            })
        }
        $(document).ready(function() {
            $("input[name=logro_comunicacion]").change(function() {
                var LOGRO_COMUNICACION = $('input:radio[name=logro_comunicacion]:checked').val();
                $('#motivo_comunicacion option:eq(0)').attr('selected', 'selected');
                $('#motivo_no_comunicacion option:eq(0)').attr('selected', 'selected');
                if (LOGRO_COMUNICACION == 'SI') {
                    $('#motivo_no_comunicacion').attr("disabled", "disabled");
                    $('#motivo_comunicacion').removeAttr("disabled", "disabled");
                }
                if (LOGRO_COMUNICACION == 'NO') {
                    $('#motivo_comunicacion').attr("disabled", "disabled");
                    $('#motivo_no_comunicacion').removeAttr("disabled", "disabled");
                }
            });
        });
    </script>
</head>
<?php
$string_intro = getenv("QUERY STRING");
parse_str($string_intro);
require('../datos/conex.php');
$ID_PACIENTE;
$ID_GESTION;
include('../logica/consulta_paciente.php');
$DIAS_ANTES = date('Y-m-d', strtotime('-31 day'));
if ($privilegios != '' && $usuname_peru != '') { ?>

    <body class="body" style="width:80.9%;margin-left:12%;">
        <form id="seguimiento" name="seguimiento" method="post" action="../logica/actualizar_seguimiento.php" onkeydown="return filtro(2)" enctype="multipart/form-data" class="letra">
            <div id="Accordion1" class="Accordion" tabindex="0" style="height:100%;">
                <div class="AccordionPanel">
                    <div class="AccordionPanelTab">PACIENTE</div>
                    <div class="AccordionPanelContent">
                        <?php
                        $Seleccion = mysqli_query($conex, "SELECT * FROM `bayer_pacientes` AS P
                        INNER JOIN bayer_tratamiento AS T ON T.ID_PACIENTE_FK=P.ID_PACIENTE
                        WHERE ID_PACIENTE = '" . $ID_PACIENTE . "'");
                        while ($fila = mysqli_fetch_array($Seleccion)) {
                            $ID_PACIENTE2 = $fila['ID_PACIENTE'];
                            $ID_PA = $fila['ID_PACIENTE'];
                            $ID = $fila['ID_PACIENTE'];
                            $PAIS = $fila['PAIS_PACIENTE'];
                            $DEPARTAMENTO = $fila['DEPARTAMENTO_PACIENTE'];
                            $CIUDAD = $fila['CIUDAD_PACIENTE'];
                            $MEDICO = $fila['MEDICO_TRATAMIENTO'];
                            $EXAMEN = $fila['EXAMEN'];
                            $ASEGURADOR_TRATAMIENTO = $fila['ASEGURADOR_TRATAMIENTO'];
                            function Zeros($numero, $largo)
                            {
                                $resultado = $numero;
                                while (strlen($resultado) < $largo) {
                                    $resultado = "0" . $resultado;
                                }
                                return $resultado;
                            }
                            $ID_PACIENTE = Zeros($ID_PA, 5);
                        ?>
                            <table width="100%" border="0">
                                <tr>
                                    <td width="20%">
                                        <span>Codigo de Usuario</span>
                                    </td>
                                    <td width="30%">
                                        <input name="codigo_gestion" type="text" id="codigo_gestion" max="10" readonly="readonly" value="<?php echo $ID_GESTION; ?>" style="display:none" />
                                        <input name="codigo_usuario" class="input_t" type="text" id="codigo_usuario" max="10" readonly="readonly" value="<?php echo 'PAD' . $ID_PACIENTE; ?>" />
                                        <input name="codigo_usuario2" type="text" id="codigo_usuario2" max="10" readonly="readonly" value="<?php echo $fila['ID_PACIENTE']; ?>" style="display:none" />
                                    </td>
                                    <td width="20%">
                                        <span>Estado<span class="asterisco">*</span></span>
                                    </td>
                                    <td width="30%">
                                        <?php if ($privilegios == 1) { ?>
                                            <select type="text" name="estado_paciente" id="estado_paciente">
                                                <option><?php echo $fila['ESTADO_PACIENTE']; ?></option>
                                                <option>Proceso</option>
                                                <option>Activo</option>
                                                <option>Cierre Efectivo</option>
                                                <option>Cierre No efectivo</option>
                                            </select>
                                        <?php } else { ?>
                                            <input name="estado_paciente" type="text" id="estado_paciente" readonly="readonly" value="<?php echo $fila['ESTADO_PACIENTE']; ?>" />
                                        <?php    } ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        <span>Fecha de Activacion<span class="asterisco">*</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                    </td>
                                    <td width="30%">
                                        <input type="date" name="fecha_activacion" id="fecha_activacion" value="<?php echo $fila['FECHA_ACTIVACION_PACIENTE']; ?>" readonly="readonly" />
                                    </td>
                                    <td width="20%">
                                        <span>Fecha de Cierre&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                    </td>
                                    <td width="30%">
                                        <input type="date" name="fecha_retiro" id="fecha_retiro" max="10" value="<?php echo $fila['FECHA_RETIRO_PACIENTE']; ?>" />
                                    </td>
                                <tr>
                                    <td>
                                        <span>Observaciones Motivo de Cierre</span>
                                    </td>
                                    <td colspan="3">
                                        <textarea name="observacion_retiro" id="observacion_retiro" style="width:98%; height:100px"><?php echo $fila['OBSERVACION_MOTIVO_RETIRO_PACIENTE']; ?></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span>Nombre Medico<span class="asterisco">*</span></span>
                                    </td>
                                    <td>
                                        <input type="text" class="input_t" name="nombre" id="nombre" value="<?php echo $fila['NOMBRE_PACIENTE']; ?>" readonly="readonly" />
                                    </td>
                                    <td>
                                        <span>Apellidos Medico<span class="asterisco">*</span></span>
                                    </td>
                                    <td>
                                        <input type="text" class="input_t" name="apellidos" id="apellidos" value="<?php echo $fila['APELLIDO_PACIENTE']; ?>" readonly="readonly" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span>Correo Medico</span>
                                    </td>
                                    <td>
                                        <input type="text" class="input_t" name="correo" id="correo" value="<?php echo $fila['CORREO_MEDICO']; ?>" readonly="readonly" />
                                    </td>
                                    <td>
                                        <span>Telefono Medico<span class="asterisco">*</span></span>
                                    </td>
                                    <td>
                                        <input type="text" class="input_t" name="telefono1" id="telefono1" value="<?php echo $fila['TELEFONO_PACIENTE']; ?>" readonly="readonly" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span>Iniciales Paciente</span>
                                    </td>
                                    <td>
                                        <input type="text" class="input_t" name="telefono3" id="telefono3" value="<?php echo $fila['INICIALES_PACIENTE']; ?>" readonly="readonly" />
                                    </td>
                                    <td>
                                        <span>Numero De Documento Paciente<span class="asterisco">*</span></span>
                                    </td>
                                    <td>
                                        <input type="text" class="input_t" name="identificacion_pa" id="identificacion_pa" value="<?php echo $fila['IDENTIFICACION_PACIENTE']; ?>" readonly="readonly" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        <span>Fecha de Nacimiento Paciente</span>
                                    </td>
                                    <td width="30%">
                                        <input type="date" name="fecha_nacimiento" id="fecha_nacimiento" max="<?php echo date('Y-m-d'); ?>" value="<?php echo $fila['FECHA_NACIMINETO_PACIENTE']; ?>" />
                                    </td>
                                    <td width="20%">
                                        <span>Tipo de Documento Paciente<span class="asterisco">*</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                    </td>
                                    <td width="20%">
                                        <input type="text" class="input_t" name="tipo_documento_p" id="tipo_documento_p" value="<?php echo $fila['TIPO_DE_IDENTIFICACION']; ?>" readonly="readonly">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span>Clasificacion Patologica<span class="asterisco">*</span></span>
                                    </td>
                                    <td>
                                        <span style="width:30%;">
                                            <input type="text" class="input_t" name="clasificacion_patologicas" id="clasificacion_patologicas" value="<?php echo $fila['CLASIFICACION_PATOLOGICA_TRATAMIENTO'] ?>" readonly="readonly">
                                        </span>
                                    </td>
                                    <td>
                                        <span>Pais<span class="asterisco">*</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                    </td>
                                    <td>
                                        <select type="text" name="pais" id="pais" onchange="mostrar_departamento()">
                                            <option><?php echo $fila['PAIS_PACIENTE']; ?></option>
                                            <?php
                                            $Seleccionar = mysqli_query($conex, "SELECT NOMBRE_PAIS FROM `bayer_pais` WHERE NOMBRE_PAIS != '' AND ID_PAIS ORDER BY NOMBRE_PAIS ASC");
                                            while ($fila3 = mysqli_fetch_array($Seleccionar)) {
                                                $PAIS = $fila3['NOMBRE_PAIS'];
                                                echo "<option>" . $PAIS . "</option>";
                                            }
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span>Departamento<span class="asterisco">*</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                    </td>
                                    <td>
                                        <?php
                                        $consulta_dep = mysqli_query($conex, "SELECT D.id,D.NOMBRE_DEPARTAMENTO FROM bayer_pacientes AS P
                                        INNER JOIN bayer_departamento AS D ON D.id = P.DEPARTAMENTO_PACIENTE
                                        WHERE ID_PACIENTE = '" . $ID_PACIENTE . "'");
                                        ?>
                                        <select type="text" name="departamento" id="departamento" onchange="mostrar_ciudades()">
                                            <option><?php echo $fila['DEPARTAMENTO_PACIENTE']; ?></option>
                                        </select>
                                    </td>
                                    <td>
                                        <span>Ciudad<span class="asterisco">*</span></span>
                                    </td>
                                    <td>
                                        <select type="text" name="ciudad" id="ciudad">
                                            <option><?php echo $fila['CIUDAD_PACIENTE']; ?></option>
                                            <?php
                                            $Selecciones = mysqli_query($conex, "SELECT c.NOMBRE_CIUDAD FROM bayer_ciudad AS c
                                            INNER JOIN bayer_pais AS d ON d.id=c.ID_DEPARTAMENTO_FK
                                            WHERE d.NOMBRE_DEPARTAMENTO='$DEPT' AND d.ID_PAIS_FK='3' ORDER BY c.NOMBRE_CIUDAD ASC");
                                            while ($fila2 = mysqli_fetch_array($Selecciones)) {
                                                $CIUDAD = $fila2['NOMBRE_CIUDAD'];
                                                echo "<option>" . $CIUDAD . "</option>";
                                            }
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                    </div>
                </div>
                <div class="AccordionPanel">
                    <div class="AccordionPanelTab">GENERAL</div>
                    <div class="AccordionPanelContent">
                        <br />
                        <table width="93.5%">
                            <?php
                            $fecha_actual = date('Y-m-d');
                            $fecha_rec_act = explode("-", $fecha_actual);
                            $anio_act = $fecha_rec_act[0];
                            $mes_act = $fecha_rec_act[1];
                            $dia_act = $fecha_rec_act[2];
                            $dato = ((int)$mes_act);
                            ?>
                            <tr>
                                <td>
                                    <span>Se Logro la Comunicacion<span class="asterisco">*</span></span>
                                    <br />
                                    <br />
                                </td>
                                <td>
                                    <input type="radio" name="logro_comunicacion" id="logro_comunicacion" style=" width:20%; display:none" value="" checked="checked" />
                                    <input type="radio" name="logro_comunicacion" id="logro_comunicacion" style=" width:20%;" value="SI" />SI
                                    <input type="radio" name="logro_comunicacion" id="logro_comunicacion" style=" width:20%;" value="NO" />NO
                                    <br />
                                    <br />
                                </td>
                                <td class="tit">
                                    <span>Motivo de Comunicaci&oacute;n<span class="asterisco">*</span></span>
                                    <br />
                                    <br />
                                </td>
                                <td style="width:30%;">
                                    <select type="text" name="motivo_comunicacion" id="motivo_comunicacion">
                                        <option value="">Seleccione...</option>
                                        <option>Seguimiento</option>
                                        <option>Cierre</option>
                                        <option>Remision de Caso</option>
                                    </select>
                                    <br />
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td class="tit">
                                    <span>Medio de Contacto<span class="asterisco">*</span></span>
                                    <br />
                                    <br />
                                </td>
                                <td style="width:30%;">
                                    <select type="text" name="medio_contacto" id="medio_contacto">
                                        <option value="">Seleccione...</option>
                                        <option>Presencial</option>
                                        <option>llamada</option>
                                        <option>Electronico</option>
                                    </select>
                                    <br />
                                    <br />
                                </td>
                                <td>
                                    <span>Tipo de Llamada<span class="asterisco">*</span></span>
                                    <br />
                                    <br />
                                </td>
                                <td>
                                    <select type="text" name="tipo_llamada" id="tipo_llamada">
                                        <option value="">Seleccione...</option>
                                        <option>Entrada</option>
                                        <option>Salida</option>
                                    </select>
                                    <br />
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span>Motivo de No Comunicaci&oacute;n</span>
                                    <br />
                                    <br />
                                </td>
                                <td>
                                    <select type="text" name="motivo_no_comunicacion" id="motivo_no_comunicacion">
                                        <option value="">Seleccione...</option>
                                        <option>Apagado</option>
                                        <option>No Esta</option>
                                        <option>No Contesta</option>
                                        <option>No Vive Ahi</option>
                                        <option>Numero Equivocado</option>
                                        <option>Telefono Ocupado</option>
                                        <option>Telefono Fuera de Servicio</option>
                                        <option>Otro</option>
                                    </select>
                                    <br />
                                    <br />
                                </td>
                                <td>
                                    <span>Numero de Intentos<span class="asterisco">*</span></span>
                                    <br />
                                    <br />
                                </td>
                                <td>
                                    <input type="text" class="input_t" name="via_recepcion" id="via_recepcion" />
                                    <br />
                                    <br />
                                </td>
                            </tr>
                            <?php
                            $Seleccion = mysqli_query($conex, "SELECT * FROM bayer_gestiones
                            WHERE ID_PACIENTE_FK2 = '" . $ID_PACIENTE . "' ORDER BY ID_GESTION DESC LIMIT 1");
                            while ($fila1 = mysqli_fetch_array($Seleccion)) {
                                $CENTRO_DIAGNOSTICO = $fila1['CENTRO_DIAGNOSTICO'];
                                $CENTRO_PATOLOGICO = $fila1['CENTRO_PATOLOGICO'];
                                $TIPO_PAD = $fila1['TIPO_PAD'];
                                $FECHA_RESULTADO = $fila1['FECHA_RESULTADO'];
                                $FECHA_ENTREGA_BLOQUES = $fila1['FECHA_ENTREGA_BLOQUES'];
                                $DOCUMENTOS_RECIBIDOS = $fila1['DOCUMENTOS_RECIBIDOS'];
                                $CASO = $fila1['CASO'];
                            ?>
                                <tr>
                                    <td>
                                        <span>Laboratorio Diagnostico</span>
                                        <br />
                                        <br />
                                    </td>
                                    <td>
                                        <input type="text" class="input_t" name="centro_diagnostico" id="centro_diagnostico" value="<?php echo $fila1['CENTRO_DIAGNOSTICO'] ?>">
                                    </td>
                                    <td width="20%">
                                        <span>
                                            Laboratorio Patologico
                                        </span>
                                    </td>
                                    <td width="30%">
                                        <input class="input_t" type="text" name="laboratorio_patologico" id="laboratorio_patologico" value="<?php echo $fila1['CENTRO_PATOLOGICO'] ?>">
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td>
                                    <span>Asegurador<span class="asterisco">*</span></span>
                                </td>
                                <td>
                                    <?php $query =  mysqli_query($conex, "SELECT DISTINCT ASEGURADOR FROM bayer_asegurador WHERE ESTADO = 'IN' ORDER BY ID_ASEGURADOR DESC")
                                    ?>
                                    <input list="asegura" name="asegurador" id="asegurador" autocomplete="off" value="<?php echo $ASEGURADOR_TRATAMIENTO ?>">
                                    <datalist id="asegura">
                                        <?php
                                        while ($valores = mysqli_fetch_array($query)) {
                                        ?>
                                            <option><?php echo $valores['ASEGURADOR'] ?></option>
                                        <?php
                                        }
                                        ?>
                                    </datalist>
                                </td>
                                <td>
                                    <span>Autor</span>
                                    <br />
                                    <br />
                                </td>
                                <td>
                                    <input type="text" class="input_t" name="autor" id="autor" readonly="readonly" value="<?php echo $usuname_peru ?>" />
                                    <br />
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td width="20%">
                                    <span>
                                        Examenes
                                    </span>
                                </td>
                                <td width="30%" align="center">
                                    <select name="left" id="left" style="height:143px;" multiple="multiple">
                                        <option value="Ecografia Hepato - biliar (EC)">Ecografia Hepato - biliar (EC)</option>
                                        <option value="Alfa-fetoproteina (AFP)">Alfa-fetoproteina (AFP)</option>
                                        <option value="Tomografia Axial Computarizada (TAC) de Cuatro Fases dinamicas especializada en nodulo hepatico">Tomografia Axial Computarizada (TAC) de Cuatro Fases dinamicas especializada en nodulo hepatico</option>
                                        <option value="Resonancia Magnetica (RM) si y con medio de contraste">Resonancia Magnetica (RM) si y con medio de contraste</option>
                                        <option value="Creatinina">Creatinina</option>
                                        <option value="Nitrogeno ureico en Sangre (BUN)">Nitrogeno ureico en Sangre (BUN)</option>
                                        <option value="Inmunohistoquimica (IHC)">Inmunohistoquimica (IHC)</option>
                                        <option value="Next generation Sequencing (NGS)">Next generation Sequencing (NGS)</option>
                                    </select>
                                </td>
                                <td width="20%" align="center">
                                    <input type="button" value=" >>>> " id="turnRightBtn" onClick="moveOption(document.getElementById('left'), document.getElementById('examen'))">
                                    <br>
                                    <br>
                                    <input type="button" id="turnLeftBtn" value=" <<<< " onClick="moveOption(document.getElementById('examen'), document.getElementById('left'))">
                                </td>
                                <td align="left">
                                    <select name="examen" id="examen" style="height:143px;" multiple="multiple">
                                        <option><?php echo $EXAMEN ?></option>
                                    </select>
                                    <input type="hidden" name="examen_p" id="examen_p" value="<?php echo $EXAMEN ?>">
                                    <br />
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span>
                                        Tipo de PAD
                                    </span>
                                </td>
                                <td>
                                    <select name="tipo_pad" id="tipo_pad">
                                        <option><?php echo $TIPO_PAD ?></option>
                                        <option>Seleccione...</option>
                                        <option>HCC</option>
                                        <option>NTRK</option>
                                    </select>
                                </td>
                                <td>
                                    <span>
                                        Fecha de resultado
                                    </span>
                                </td>
                                <td>
                                    <input type="date" name="fecha_resultado" id="fecha_resultado" value="<?php echo $FECHA_RESULTADO ?>">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span>
                                        Fecha de entrega bloques de patología
                                    </span>
                                </td>
                                <td>
                                    <input type="date" name="fecha_entrega_bloque" id="fecha_entrega_bloque" value="<?php echo $FECHA_ENTREGA_BLOQUES ?>">
                                </td>
                                <td>
                                    <span>
                                        Caso En Proceso O Cerrado
                                    </span>
                                </td>
                                <td>
                                    <select name="caso" id="caso">
                                        <option><?php echo $CASO ?></option>
                                        <option>Seleccione...</option>
                                        <option>CERRADO - IHC</option>
                                        <option>PENDIENTE RESULTADOS IHC</option>
                                        <option>PENDIENTE BLOQUE</option>
                                        <option>PENDIENTE RESULTADOS NGS</option>
                                        <option>CERRADO - NO PROCESADO</option>
                                        <option>PENDIENTE BLOQUE (Perdido)</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td width="20%">
                                    <span>
                                        Documentos recibidos
                                    </span>
                                </td>
                                <td width="30%" align="center">
                                    <select name="left_docu" id="left_docu" style="height:143px;" multiple="multiple">
                                        <option value="Orden Medica">Orden Medica</option>
                                        <option value="Vocher">Vocher</option>
                                        <option value="Fotocopia cedula paciente">Fotocopia cedula paciente</option>
                                        <option value="Historia Clinica">Historia Clinica</option>
                                        <option value="Autorizacion de retiro de Muestras">Autorizacion de retiro de Muestras</option>
                                    </select>
                                </td>
                                <td width="20%" align="center">
                                    <input type="button" value=" >>>> " id="turnRightBtn" onClick="moveOption2(document.getElementById('left_docu'), document.getElementById('documentos_recibidos'))">
                                    <br>
                                    <br>
                                    <input type="button" id="turnLeftBtn" value=" <<<< " onClick="moveOption2(document.getElementById('documentos_recibidos'), document.getElementById('left_docu'))">
                                </td>
                                <td align="left_docu">
                                    <select name="documentos_recibidos" id="documentos_recibidos" style="height:143px;" multiple="multiple">
                                        <option><?php echo $DOCUMENTOS_RECIBIDOS ?></option>
                                    </select>
                                    <input type="hidden" name="documentos_recibidos_p" id="documentos_recibidos_p" value="<?php echo $DOCUMENTOS_RECIBIDOS ?>">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span>Fecha de la Proxima Llamada<span class="asterisco">*</span></span>
                                </td>
                                <?php
                                $Sel = mysqli_query($conex, "SELECT FECHA_PROGRAMADA_GESTION, ID_GESTION FROM bayer_gestiones
                                    WHERE ID_PACIENTE_FK2 = '" . $ID_PACIENTE . "' ORDER BY FECHA_PROGRAMADA_GESTION DESC LIMIT 1");
                                while ($con = mysqli_fetch_array($Sel)) {
                                    $ID_GESTION_ULT = $con['ID_GESTION'];
                                ?>
                                    <td>
                                        <input type="text" name="id_gestion_ult" id="id_gestion_ult" value="<?php echo $ID_GESTION_ULT ?>" style="display:none" />
                                        <input type="date" name="fecha_actual" id="fecha_actual" value="<?php echo date('Y-m-d'); ?>" readonly="readonly" hidden />
                                        <input type="date" name="fecha_proxima_llamada_ant" id="fecha_proxima_llamada_ant" value="<?php echo $con['FECHA_PROGRAMADA_GESTION'] ?>" style="display:none" />
                                        <input type="date" name="fecha_proxima_llamada" id="fecha_proxima_llamada" min="<?php echo date('Y-m-d'); ?>" />
                                    </td>
                                <?php
                                }
                                ?>
                                <td>
                                    <span>Motivo de Proxima Llamada<span class="asterisco">*</span></span>
                                    <br />
                                    <br />
                                </td>
                                <td>
                                    <select type="text" name="motivo_proxima_llamada" id="motivo_proxima_llamada">
                                        <option value="">Seleccione...</option>
                                        <option>Remision de Caso</option>
                                        <option>Respuesta de Caso</option>
                                        <option>Seguimiento</option>
                                    </select>
                                    <br />
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span>Descripcion de Comunicaci&oacute;n</span>
                                    <br />
                                    <br />
                                </td>
                                <td colspan="3">
                                    <textarea style="width:98%; height:72.5px;" id="descripcion_comunicacion" name="descripcion_comunicacion" onKeyDown="return filtro(1)"></textarea>
                                    <br />
                                    <br />
                                </td>
                            </tr>
                        </table>
                    <?php
                        }
                    ?>
                    <br />
                    <br />
                    </div>
                </div>
                <div class="AccordionPanel">
                    <div class="AccordionPanelTab" style="padding:5px">COMUNICACIONES</div>
                    <div class="AccordionPanelContent">
                        <?PHP
                        $gestion = mysqli_query($conex, "SELECT * FROM `bayer_gestiones` WHERE `ID_PACIENTE_FK2` = '" . $ID_PACIENTE2 . "' ORDER BY `FECHA_COMUNICACION` DESC");
                        echo mysqli_error($conex);
                        echo "<table width=100% border=1 rules=all  align=left class=Estilo2 >";
                        echo "<tr style='border:1px solid #fff'>";
                        echo "<th class=AccordionPanelTab><strong>FECHA DE GESTION</strong></th>";
                        echo "<th class=AccordionPanelTab><strong>DESCRIPCION</strong></th>";
                        echo "<th class=AccordionPanelTab><strong>FECHA PROXIMO CONTACTO</strong></th>";
                        echo "<th class=AccordionPanelTab><strong>AUTOR</strong></th>";
                        echo "<th class=AccordionPanelTab><strong>MOTIVO COMUNICACION GESTION</strong></th>";
                        echo "<td class=AccordionPanelTab><strong>TIPO ADJUNTO</strong></td>";
                        echo "<td class=AccordionPanelTab><strong>CARCHIVO ADJUNTO</strong></td>";
                        echo "</tr>";
                        $numges = 1;
                        while ($fila2 = mysqli_fetch_array($gestion)) {
                            echo "<tr>";
                            echo "<td>" . $fila2['FECHA_COMUNICACION'] . "</td>";
                            echo "<td>";
                        ?>
                            <textarea name="observaciones" cols="60" rows="2" readonly="readonly" id="observaciones" class="letra" style="text-transform:uppercase"><?php echo $fila2['DESCRIPCION_COMUNICACION_GESTION']; ?></textarea>
                            <?PHP
                            echo "</td>";
                            echo "<td>" . $fila2['FECHA_PROGRAMADA_GESTION'] . "</td>";
                            echo "<td>" . $fila2['AUTOR_GESTION'] . "</td>";
                            echo "<td>" . $fila2['MOTIVO_COMUNICACION_GESTION'] . "</td>";
                            if ($privilegios == '1') { ?>
                                <td> <input name="id_paciente" id="id_paciente" type="text" style="display:none;" value="<?php echo $ID_PACIENTE; ?>" />
                                    <input name="ID" id="ID" type="text" style="display:none;" value="<?php echo $fila2['ID_GESTION']; ?>" />
                                    <samp><?php echo $fila2['TIPO_ADJUNTO']; ?>
                                        <input name="TIPO_ADJUNTO" id="TIPO_ADJUNTO" type="hidden" maxlength="25" style="width:80%" value="<?php echo $fila2['TIPO_ADJUNTO']; ?>" /></samp>
                                </td>
                            <?php    } else if ($privilegios == '2') { ?>
                                <td> <input name="id_paciente" id="id_paciente" type="text" style="display:none;" value="<?php echo $ID_PACIENTE; ?>" />
                                    <input name="ID" id="ID" type="text" style="display:none;" value="<?php echo $fila2['ID_GESTION']; ?>" />
                                    <samp><?php echo $fila2['TIPO_ADJUNTO']; ?>
                                        <input name="TIPO_ADJUNTO" id="TIPO_ADJUNTO" type="hidden" maxlength="25" style="width:80%" value="<?php echo $fila2['TIPO_ADJUNTO']; ?>" /></samp>
                                </td>
                                <?php            } else {
                            }
                            $ID_GES = $fila2['ID_GESTION'];
                            $dir = "../ADJUNTOS_BAYER/$ID_GES";
                            if (file_exists($dir)) {
                                $directorio = opendir($dir);
                                while ($archivo = readdir($directorio)) {
                                    if ($archivo == '.' or $archivo == '..') {
                                    } else {
                                        $enlace = $dir . "/" . $archivo;
                                ?>
                                        <td>
                                            <a class="highslide" onclick="return hs.expand(this)">
                                                <img src="<?php echo $enlace; ?>" alt="" title="Click to enlarge" height="100" width="100" onclick="javascript:this.width=500;this.height=500" ondblclick="javascript:this.width=100;this.height=100" /></a>
                                            <a href="<?php echo $enlace; ?>">ver</a>
                                            <br />
                                            <br />
                                        </td>
                                <?php
                                    }
                                }
                                closedir($directorio);
                            } else {        ?>
                                <td>
                                </td>
                        <?php
                            }
                            echo "</tr>";
                            $numges = $numges + 1;
                        }
                        echo "</table>";
                        echo "<br />";
                        ?>
                    </div>
                </div>
                <div class="AccordionPanel">
                    <div class="AccordionPanelTab">NOTAS Y ADJUNTOS</div>
                    <div class="AccordionPanelContent">
                        <br />
                        <br />
                        <div style="width:91.4%;margin-left: 15px; ">
                            <textarea class="tf w-input" style="width:100%; height:100px" id="txtCurp" name="nota" maxlength="5000" onkeypress="return check(event)" placeholder="Nota" id="test"></textarea>
                        </div>
                        <br />
                        <br />
                        <div style=" display: flex;  flex-wrap: wrap; margin-left: 15px; ">
                            <div style="flex: 0 0 50%;  max-width: 50%;">
                                <span>Tipo de Adjunto<span class="asterisco"></span></span>
                                <input type="radio" name="tipo_adjunto" id="tipo_adjunto" style=" width:5%; display:none" value="" checked="checked" />
                                <input type="radio" name="tipo_adjunto" id="tipo_adjunto" style=" width:5%;" value="Voucher" />Vaucher
                                <input type="radio" name="tipo_adjunto" id="tipo_adjunto" style=" width:5%;" value="CI" />CI
                                <div class="input__row uploader">
                                    <div id="inputval" class="input-value"></div>
                                    <label for="archivo"></label>
                                    <input type="file" class="upload" name="archivo" id="archivo" class="aceptar" multiple></input>
                                </div>
                                <script>
                                    $('#archivo').on('change', function() {
                                        $('#inputval').text($(this).val());
                                    });
                                </script>
                            </div>
                        </div>
                        <div style="flex: 0 0 33.333333%; max-width: 33.333333%;"></div>
                        <div style="flex: 0 0 33.333333%; max-width: 33.333333%;"></div>
                        <center>
                            <?php
                            if ($privilegios != 5) {
                            ?>
                                <input id="registrar" name="registrar" type="submit" value="REGISTRAR" class="btn_registrar" onClick="return validar(seguimiento,2)" />
                            <?php
                            }
                            ?>
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                    </div>
                </div>
            </div>
        </form>
        <script type="text/javascript">
            function check(e) {
                tecla = (document.all) ? e.keyCode : e.which;
                if (tecla == 8) {
                    return true;
                }
                patron = /[A-Za-z0-9-.,)(., ]/;
                tecla_final = String.fromCharCode(tecla);
                return patron.test(tecla_final);
            }
            var Accordion1 = new Spry.Widget.Accordion("Accordion1");
        </script>
    </body>
<?php
} else {
?>
    <script type="text/javascript">
        window.onload = window.top.location.href = "../logica/cerrar_sesion2.php";
    </script>
<?php
}
?>

</html>