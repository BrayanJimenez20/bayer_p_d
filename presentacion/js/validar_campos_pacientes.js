function validar(tuformulario, val) {
	var LOGRO_COMUNICACION = $('input:radio[name=logro_comunicacion]:checked').val();
	if (LOGRO_COMUNICACION == '') {
		alert('El logro de la comunicacion esta vacio');
		$('#logro_comunicacion').focus();
		return false;
	}
	if (LOGRO_COMUNICACION == 'SI') {
		var ESTADO = $("#estado_paciente").val();
		if (ESTADO == '' || ESTADO == 'Seleccione...') {
			alert('El estado esta vacio');
			$('#estado_paciente').focus();
			return false;
		}
		var FECHA_ACTIVACION = $("#fecha_activacion").val();
		if (FECHA_ACTIVACION == '') {
			alert('La fecha de activacion esta vacia');
			$('#fecha_activacion').focus();
			return false;
		}
		if (val == 2) {
			var ESTADO = $("#estado_paciente").val();
			if (ESTADO == 'Abandono') {
				var fecha_retiro = $("#fecha_retiro").val();
				if (fecha_retiro == '') {
					alert('La fecha de retiro esta vacio');
					$('#fecha_retiro').focus();
					return false;
				}
			}
			var fecha_retiro = $("#fecha_retiro").val();
			if (fecha_retiro != '') {
				var motivo_retiro = $("#motivo_retiro").val();
				if (motivo_retiro == '' || motivo_retiro == 'Seleccione...') {
					alert('Seleccione motivo de retiro');
					$('#motivo_retiro').focus();
					return false;
				}
				var observacion_retiro = $("#observacion_retiro").val();
				if (observacion_retiro == '') {
					alert('La observacion de retiro esta vacia');
					$('#observacion_retiro').focus();
					return false;
				}
			}
		}
		if (val == 2) {
			var DIRECCION = $("#direccion_act").val();
			if (DIRECCION == '') {
				var DIRECCION2 = $("#DIRECCION").val();
				if (DIRECCION2 == '') {
					alert('La direccion esta vacia');
					$('#DIRECCION').focus();
					return false;
				}
			}
		}
		if (val == 1) {
			var DIRECCION = $("#DIRECCION").val();
			if (DIRECCION == '') {
				alert('La direccion esta vacia');
				$('#DIRECCION').focus();
				return false;
			}
		}
		var FECHA_NACIMIENTO = $("#fecha_nacimiento").val();
		if (FECHA_NACIMIENTO == '') {
			alert('La fecha de nacimiento esta vacia');
			$('#fecha_nacimiento').focus();
			return false;
		}

		var CLASIFICACION_PATOLOGICA = $("#clasificacion_patologica").val();
		if (CLASIFICACION_PATOLOGICA == '') {
			alert('La clasificacion patologica esta vacia');
			$('#clasificacion_patologica').focus();
			return false;
		}
		var NOMBRE = $("#nombre_medico").val();
		if (NOMBRE == '') {
			alert('El nombre del medico esta vacio');
			$('#nombre_medico').focus();
			return false;
		}
		var APELLIDO = $("#apellidos_medico").val();
		if (APELLIDO == '') {
			alert('El apellido del medico esta vacio');
			$('#apellidos_medico').focus();
			return false;
		}
		var CORREO = $("#correo_medico").val();
		if (CORREO == '') {
			alert('El correo del medico esta vacio');
			$('#correo_medico').focus();
			return false;
		}
		var TELEFONO = $("#telefono").val();
		if (TELEFONO == '') {
			alert('El telefono del medico esta vacio');
			$('#telefono').focus();
			return false;
		}
		var PAIS = $("#pais").val();
		if (PAIS == '') {
			alert('El pais esta vacio');
			$('#pais').focus();
			return false;
		}
		var DEPARTAMENTO = $("#departamento").val();
		if (DEPARTAMENTO == '') {
			alert('El departamento esta vacio');
			$('#departamento').focus();
			return false;
		}
		var CIUDAD = $("#ciudad").val();
		if (CIUDAD == '') {
			alert('La ciudad esta vacia');
			$('#ciudad').focus();
			return false;
		}
		var INICIALES_PA = $("#ini_pas").val();
		if (INICIALES_PA == '') {
			alert('Las iniciales del paciente estan vacia');
			$('#ini_pas').focus();
			return false;
		}
		var TIPO_DOCUMENTO = $("#tipo_documento_p").val();
		if (TIPO_DOCUMENTO == '') {
			alert('El tipo de documento esta vacio');
			$('#tipo_documento_p').focus();
			return false;
		}
		var IDENTIFICACION = $("#identificacion_pa").val();
		if (IDENTIFICACION == '') {
			alert('La identificacion esta vacia');
			$('#identificacion_pa').focus();
			return false;
		}
		if (LOGRO_COMUNICACION == 'SI') {
			var MOTIVO_COMUNICACION = $("#motivo_comunicacion").val();
			if (MOTIVO_COMUNICACION == '' || MOTIVO_COMUNICACION == 'Seleccione...') {
				alert('El motivo de comunicacion esta vacio');
				$('#motivo_comunicacion').focus();
				return false;
			}
			var MEDIO_CONTACTO = $("#medio_contacto").val();
			if (MEDIO_CONTACTO == '' || MEDIO_CONTACTO == 'Seleccione...') {
				alert('El medio de contacto esta vacio');
				$('#medio_contacto').focus();
				return false;
			}
			var TIPO_LLAMADA = $("#tipo_llamada").val();
			if (TIPO_LLAMADA == '' || TIPO_LLAMADA == 'Seleccione...') {
				alert('El tipo de llamada esta vacio');
				$('#tipo_llamada').focus();
				return false;
			}
		}
		var ASEGURADOR = $("#asegurador").val();
		if (ASEGURADOR == '' || ASEGURADOR == 'Seleccione...') {
			alert('El asegurador esta vacio');
			$('#asegurador').focus();
			return false;
		}
		var REGIMEN = $("#regimen").val();
		if (REGIMEN == '' || REGIMEN == 'Seleccione...') {
			alert('El regimen esta vacio');
			$('#regimen').focus();
			return false;
		}

		var IPS_ATIENDE = $("#ips_atiende").val();
		if (IPS_ATIENDE == '') {
			alert('La ips que atiende esta vacia');
			$('#ips_atiende').focus();
			return false;
		}

		var FECHA_PROXIMA_LLAMADA = $("#fecha_proxima_llamada").val();
		if (FECHA_PROXIMA_LLAMADA == '') {
			alert('La fecha de proxima llamada esta vacia');
			$('#fecha_proxima_llamada').focus();
			return false;
		}
		var MOTIVO_PROXIMA_LLAMADA = $("#motivo_proxima_llamada").val();
		if (MOTIVO_PROXIMA_LLAMADA == '') {
			alert('El motivo de proxima llamada esta vacio');
			$('#motivo_proxima_llamada').focus();
			return false;
		}
		var DESCRIPCION_COMUNICACION = $("#descripcion_comunicacion").val();
		if (DESCRIPCION_COMUNICACION == '') {
			alert('La descripcion de comunicacion esta vacia');
			$('#descripcion_comunicacion').focus();
			return false;
		}
	}
	else {
		var MEDIO_CONTACTO = $("#medio_contacto").val();
		if (MEDIO_CONTACTO == '' || MEDIO_CONTACTO == 'Seleccione...') {
			alert('El medio de contacto esta vacio');
			$('#medio_contacto').focus();
			return false;
		}
		var TIPO_LLAMADA = $("#tipo_llamada").val();
		if (TIPO_LLAMADA == '' || TIPO_LLAMADA == 'Seleccione...') {
			alert('El tipo de llamada esta vacio');
			$('#tipo_llamada').focus();
			return false;
		}
		var MOTIVO_NO_COMUNICACION = $("#motivo_no_comunicacion").val();
		if (MOTIVO_NO_COMUNICACION == '' || MOTIVO_NO_COMUNICACION == 'Seleccione...') {
			alert('El motivo de no comunicacion esta vacio');
			$('#motivo_no_comunicacion').focus();
			return false;
		}
		var NUMERO_INTENTOS = $("#via_recepcion").val();
		if (NUMERO_INTENTOS == '') {
			alert('El numero de intentos esta vacio');
			$('#via_recepcion').focus();
			return false;
		}
		var FECHA_PROXIMA_LLAMADA = $("#fecha_proxima_llamada").val();
		if (FECHA_PROXIMA_LLAMADA == '') {
			alert('La fecha de proxima llamada esta vacia');
			$('#fecha_proxima_llamada').focus();
			return false;
		}
		var MOTIVO_PROXIMA_LLAMADA = $("#motivo_proxima_llamada").val();
		if (MOTIVO_PROXIMA_LLAMADA == '') {
			alert('El motivo de proxima llamada esta vacio');
			$('#motivo_proxima_llamada').focus();
			return false;
		}
		var DESCRIPCION_COMUNICACION = $("#descripcion_comunicacion").val();
		if (DESCRIPCION_COMUNICACION == '') {
			alert('La descripcion de comunicacion esta vacia');
			$('#descripcion_comunicacion').focus();
			return false;
		}
	}
}