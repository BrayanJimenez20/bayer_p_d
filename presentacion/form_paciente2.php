<?php

include('../logica/session.php')

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Documento sin titulo</title>
    <link type="text/css" rel="stylesheet" href="css/estilo_form_paciente.css" />
    <script src="css/SpryAssets/SpryAccordion.js" type="text/javascript"></script>
    <link href="css/SpryAssets/SpryAccordion.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery.js"></script>
    <script type="text/javascript" src="js/calcular_edad.js"></script>
    <script type="text/javascript" src="js/direccion.js"></script>
    <script type="text/javascript" src="js/validar_campos_pacientes.js"></script>
    <script type="text/javascript" src="js/validaciones.js"></script>
    <script type="text/javascript" src="js/validar_caracteres.js"></script>
    <script type="text/javascript" src="../presentacion/js/jquery.js"></script>
    <script type="text/javascript" src="../logica/js/validaciones.js"></script>
    <script type="text/javascript" src="../logica/js/validaciones_campos.js"></script>

    <style>
        td {
            padding: 3px;
            background-color: transparent;
        }

        .input__row {
            margin-top: 10px;
        }

        /* Radio button */

        /* Upload button */
        .upload {
            display: none;
        }

        .uploader {
            border: 1px solid #12a9e3;
            width: 300px;
            position: relative;
            height: 30px;
            display: flex;
        }

        .uploader .input-value {
            width: 250px;
            padding: 5px;
            overflow: hidden;
            text-overflow: ellipsis;
            line-height: 25px;
            font-family: sans-serif;
            font-size: 16px;
        }

        .uploader label {
            cursor: pointer;
            margin: 0;
            width: 30px;
            height: 30px;
            position: absolute;
            right: 0;
            background: #17a8e391 url('https://www.interactius.com/wp-content/uploads/2017/09/folder.png') no-repeat center;

        }
    </style>
    <script type="text/javascript">
        function trat_previo(sel) {
            if (sel.value == "Otro") {
                divC = document.getElementById("otro_tratamiento");
                divC.style.display = "";
            }

            if (sel.value != "Otro") {
                divC = document.getElementById("otro_tratamiento");
                divC.style.display = "none";
            }
        }
    </script>



</head>

<?php
$string_intro = getenv("QUERY STRING");
parse_str($string_intro);
require('../datos/conex.php');
$ID_PACIENTE;
$ID_GESTION;
include('../logica/consulta_paciente.php');
$DIAS_ANTES = date('Y-m-d', strtotime('-31 day')); // resta 7 d�a
if ($privilegios != '' && $usuname_peru != '') { ?>

    <body class="body" style="width:80.9%;margin-left:12%;">
        <form id="seguimiento" name="seguimiento" method="post" action="../logica/actualizar_seguimiento.php" onkeydown="return filtro(2)" enctype="multipart/form-data" class="letra">
            <div id="Accordion1" class="Accordion" tabindex="0" style="height:100%;">
                <div class="AccordionPanel">
                    <div class="AccordionPanelTab">PACIENTE</div>
                    <div class="AccordionPanelContent">
                        <?php
                        $Seleccion = mysqli_query($conex, "SELECT * FROM `bayer_pacientes` AS P
                        INNER JOIN bayer_tratamiento AS T ON T.ID_PACIENTE_FK=P.ID_PACIENTE
                        WHERE ID_PACIENTE = '" . $ID_PACIENTE . "'");
                        while ($fila = mysqli_fetch_array($Seleccion)) {
                            $ID_PACIENTE2 = $fila['ID_PACIENTE'];
                            $ID_PA = $fila['ID_PACIENTE'];
                            function Zeros($numero, $largo)
                            {
                                $resultado = $numero;
                                while (strlen($resultado) < $largo) {
                                    $resultado = "0" . $resultado;
                                }
                                return $resultado;
                            }
                            $ID_PACIENTE = Zeros($ID_PA, 5);
                        ?>
                            <table width="100%" border="0">
                                <tr>
                                    <td width="20%">
                                        <span>Codigo de Usuario</span>

                                    </td>
                                    <td width="30%">
                                        <input name="codigo_gestion" type="text" id="codigo_gestion" max="10" readonly="readonly" value="<?php echo $ID_GESTION; ?>" style="display:none" />
                                        <input name="codigo_usuario" type="text" id="codigo_usuario" max="10" readonly="readonly" value="<?php echo 'PAD' . $ID_PACIENTE; ?>" />

                                        <input name="codigo_usuario2" type="text" id="codigo_usuario2" max="10" readonly="readonly" value="<?php echo $fila['ID_PACIENTE']; ?>" style="display:none" />
                                    </td>
                                    <td width="20%">
                                        <span>Estado del Paciente<span class="asterisco">*</span></span>
                                    </td>
                                    <td width="30%">
                                        <?php if ($privilegios == 1) { ?>
                                            <select type="text" name="estado_paciente" id="estado_paciente">
                                                <option><?php echo $fila['ESTADO_PACIENTE']; ?></option>
                                                <option>Abandono</option>
                                                <option>Activo</option>
                                                <option>Interrumpido</option>
                                                <option>Proceso</option>
                                            </select>
                                        <?php } else { ?>
                                            <input name="estado_paciente" type="text" id="estado_paciente" readonly="readonly" value="<?php echo $fila['ESTADO_PACIENTE']; ?>" />
                                        <?php    } ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        <span>Fecha de Activacion<span class="asterisco">*</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                    </td>
                                    <td width="30%">
                                        <input type="date" name="fecha_activacion" id="fecha_activacion" value="<?php echo $fila['FECHA_ACTIVACION_PACIENTE']; ?>" readonly="readonly" />

                                    </td>

                                    <td width="20%">
                                        <span>Solicitar cambio de estado Paciente</span>
                                    </td>
                                    <td width="30%">
                                        <select type="text" name="cambio_estado_paciente" id="cambio_estado_paciente">
                                            <option>No</option>
                                            <option>Abandono</option>
                                            <option>Activo</option>
                                            <option>Interrumpido</option>
                                            <option>Proceso</option>
                                        </select>
                                    </td>
                                <tr>
                                    <td width="20%">
                                        <span>Fecha de Retiro&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                    </td>
                                    <td width="30%">
                                        <input type="date" name="fecha_retiro" id="fecha_retiro" max="10" value="<?php echo $fila['FECHA_RETIRO_PACIENTE']; ?>" />
                                    </td>
                                    <td width="20%">
                                        <span>Motivo de Retiro</span>
                                    </td>
                                    <td>
                                        <select type="text" name="motivo_retiro" id="motivo_retiro">
                                            <option><?php echo $fila['MOTIVO_RETIRO_PACIENTE']; ?></option>
                                            <option>Cambio de tratamiento</option>
                                            <option>Evento adverso</option>
                                            <option>Fuera del pais</option>
                                            <option>Muerte</option>
                                            <option>No interesado</option>
                                            <option>Off label</option>
                                            <option>Orden medica</option>
                                            <option>Otro</option>
                                            <option>Progresion de la enfermedad</option>
                                            <option>Terminacion del tratamiento</option>
                                            <option>Voluntario</option>
                                            <option value="">NO APLICA</option>

                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span>Observaciones Motivo de Retiro</span>
                                    </td>

                                    <td colspan="3">

                                        <textarea name="observacion_retiro" id="observacion_retiro" style="width:98%; height:100px"><?php echo $fila['OBSERVACION_MOTIVO_RETIRO_PACIENTE']; ?></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span>Nombre<span class="asterisco">*</span></span>
                                    </td>
                                    <td>
                                        <input type="text" name="nombre" id="nombre" value="<?php echo $fila['NOMBRE_PACIENTE']; ?>" readonly="readonly" />
                                    </td>
                                    <td>
                                        <span>Apellidos<span class="asterisco">*</span></span>
                                    </td>
                                    <td>

                                        <input type="text" name="apellidos" id="apellidos" value="<?php echo $fila['APELLIDO_PACIENTE']; ?>" readonly="readonly" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span>Identificacion<span class="asterisco">*</span></span>
                                    </td>
                                    <td>
                                        <input type="text" name="identificacion" id="identificacion" value="<?php echo $fila['IDENTIFICACION_PACIENTE']; ?>" readonly="readonly" />
                                    </td>
                                    <td>
                                        <span>Telefono 1<span class="asterisco">*</span></span>
                                    </td>
                                    <td>
                                        <input type="text" name="telefono1" id="telefono1" value="<?php echo $fila['TELEFONO_PACIENTE']; ?>" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span>Telefono 2</span>
                                    </td>
                                    <td>
                                        <input type="text" name="telefono2" id="telefono2" value="<?php echo $fila['TELEFONO2_PACIENTE']; ?>" />
                                    </td>
                                    <td>
                                        <span>Telefono 3</span>
                                    </td>
                                    <td>
                                        <input type="text" name="telefono3" id="telefono3" value="<?php echo $fila['TELEFONO3_PACIENTE']; ?>" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span>Correo Electronico</span>
                                    </td>
                                    <td>
                                        <input type="text" name="correo" id="correo" value="<?php echo $fila['CORREO_PACIENTE']; ?>" />
                                    </td>
                                    <td>
                                        <span>Departamento<span class="asterisco">*</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                    </td>
                                    <td>
                                        <select type="text" name="departamento" id="departamento" onchange="mostrar_ciudades()">
                                            <option><?php echo $fila['DEPARTAMENTO_PACIENTE']; ?></option>
                                            <?php
                                            $DEPT = $fila['DEPARTAMENTO_PACIENTE'];
                                            $Seleccionar = mysqli_query($conex, "SELECT NOMBRE_DEPARTAMENTO FROM `bayer_departamento` WHERE NOMBRE_DEPARTAMENTO != '' AND NOMBRE_DEPARTAMENTO != '$DEPT' AND ID_PAIS_FK='3' ORDER BY NOMBRE_DEPARTAMENTO ASC");
                                            while ($fila3 = mysqli_fetch_array($Seleccionar)) {
                                                $DEPARTAMENTO = $fila3['NOMBRE_DEPARTAMENTO'];
                                                echo "<option>" . $DEPARTAMENTO . "</option>";
                                            }  ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span>Ciudad<span class="asterisco">*</span></span>
                                    </td>
                                    <td>
                                        <select type="text" name="ciudad" id="ciudad">
                                            <option><?php echo $fila['CIUDAD_PACIENTE']; ?></option>
                                            <?php
                                            $Selecciones = mysqli_query($conex, "SELECT c.NOMBRE_CIUDAD FROM bayer_ciudad AS c
                                            INNER JOIN bayer_departamento AS d ON d.id=c.ID_DEPARTAMENTO_FK
                                            WHERE d.NOMBRE_DEPARTAMENTO='$DEPT' AND d.ID_PAIS_FK='3' ORDER BY c.NOMBRE_CIUDAD ASC");
                                            while ($fila2 = mysqli_fetch_array($Selecciones)) {
                                                $CIUDAD = $fila2['NOMBRE_CIUDAD'];
                                                echo "<option>" . $CIUDAD . "</option>";
                                            }   ?>
                                        </select>
                                    </td>
                                    <td>
                                        <span>Barrio<span class="asterisco">*</span></span>
                                    </td>
                                    <td>
                                        <input type="text" name="barrio" id="barrio" value="<?php echo $fila['BARRIO_PACIENTE']; ?>" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span>Direccion<span class="asterisco">*</span></span>
                                    </td>
                                    <td colspan="3">
                                        <input name="direccion_act" id="direccion_act" style="width:93%" value="<?php echo $fila['DIRECCION_PACIENTE']; ?>" readonly="readonly" />
                                        <img src="imagenes/lapiz 100.png" id="cambio" name="cambio" title="Editar" style="width:4%; height:20px; margin-left:-10%;" align="right" />
                                    </td>
                                </tr>
                                <tr style="padding:3%;">
                                    <td colspan="4" width="90%">
                                        <div id="cambio_direccion" style="display:none; border:#F00 1px solid;">
                                            <table width="99%">
                                                <tr style="padding:3%;">
                                                    <td style="width:10%;"><span>Direccion<span class="asterisco">*</span></span></td>
                                                    <td bgcolor="#FFFFFF" colspan="3">
                                                        <input type="text" name="DIRECCION" id="DIRECCION" readonly style="width:99.8%;" />
                                                    </td>
                                                </tr>
                                                <tr style="padding:3%;">
                                                    <td><span>Via:</span></td>
                                                    <td style="width:35%"><span>
                                                            <select id="VIA" name="VIA" style="width:96%">
                                                                <option value="">Seleccione...</option>
                                                                <option>ANILLO VIAL</option>
                                                                <option>AUTOPISTA</option>
                                                                <option>AVENIDA</option>
                                                                <option>BOULEVAR</option>
                                                                <option>CALLE</option>
                                                                <option>CALLEJON</option>
                                                                <option>CARRERA</option>
                                                                <option>CIRCUNVALAR</option>
                                                                <option>CONDOMINIO</option>
                                                                <option>DIAGONAL</option>
                                                                <option>KILOMETRO</option>
                                                                <option>LOTE</option>
                                                                <option>SALIDA</option>
                                                                <option>SECTOR</option>
                                                                <option>TRANSVERSAL</option>
                                                                <option>VEREDA</option>
                                                                <option>VIA</option>

                                                            </select>

                                                        </span></td>
                                                    <td style="width:10%;"><span>Detalle via:</span></td>
                                                    <td width="177" bgcolor="#FFFFFF"><span>
                                                            <input name="detalle_via" id="detalle_via" type="text" maxlength="30" style="width:99%" />
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="96"><span>N&uacute;mero:</span></td>
                                                    <td bgcolor="#FFFFFF">
                                                        <span>
                                                            <input name="numero" id="numero" type="text" maxlength="5" style=" width:45%" /> -
                                                            <input name="numero2" id="numero2" type="text" maxlength="5" style=" width:45%" />
                                                        </span>
                                                    </td>
                                                    <td></td>
                                                    <td bgcolor="#FFFFFF"></td>
                                                </tr>
                                                <tr style="padding:3%;">
                                                    <td><span>Interior:</span></td>
                                                    <td bgcolor="#FFFFFF"><span>
                                                            <select id="interior" name="interior" style="width:96%">
                                                                <option value="">Seleccione...</option>
                                                                <option>APARTAMENTO</option>
                                                                <option>BARRIO</option>
                                                                <option>BLOQUE</option>
                                                                <option>CASA</option>
                                                                <option>CIUDADELA</option>
                                                                <option>CONJUNTO</option>
                                                                <option>CONJUNTO RESIDENCIAL</option>
                                                                <option>EDIFICIO</option>
                                                                <option>ENTRADA</option>
                                                                <option>ETAPA</option>
                                                                <option>INTERIOR</option>
                                                                <option>MANZANA</option>
                                                                <option>NORTE</option>
                                                                <option>OFICINA</option>
                                                                <option>OCCIDENTE</option>
                                                                <option>ORIENTE</option>
                                                                <option>PENTHOUSE</option>
                                                                <option>PISO</option>
                                                                <option>PORTERIA</option>
                                                                <option>SOTANO</option>
                                                                <option>SUR</option>
                                                                <option>TORRE</option>
                                                            </select>

                                                        </span></td>

                                                    <td><span>Detalle Interior:</span></td>

                                                    <td bgcolor="#FFFFFF"><span>
                                                            <input name="detalle_int" id="detalle_int" type="text" maxlength="30" readonly style="width:99%" />
                                                        </span></td>
                                                </tr>
                                                <tr style="padding:3%;">
                                                    <td><span>Interior:</span></td>
                                                    <td bgcolor="#FFFFFF"><span>
                                                            <select id="interior2" name="interior2" style="width:96%">
                                                                <option value="">Seleccione...</option>
                                                                <option>APARTAMENTO</option>
                                                                <option>BARRIO</option>
                                                                <option>BLOQUE</option>
                                                                <option>CASA</option>
                                                                <option>CIUDADELA</option>
                                                                <option>CONJUNTO</option>
                                                                <option>CONJUNTO RESIDENCIAL</option>
                                                                <option>EDIFICIO</option>
                                                                <option>ENTRADA</option>
                                                                <option>ETAPA</option>
                                                                <option>INTERIOR</option>
                                                                <option>MANZANA</option>
                                                                <option>NORTE</option>
                                                                <option>OFICINA</option>
                                                                <option>OCCIDENTE</option>
                                                                <option>ORIENTE</option>
                                                                <option>PENTHOUSE</option>
                                                                <option>PISO</option>
                                                                <option>PORTERIA</option>
                                                                <option>SOTANO</option>
                                                                <option>SUR</option>
                                                                <option>TORRE</option>

                                                            </select>

                                                        </span></td>

                                                    <td><span>Detalle Interior:</span></td>

                                                    <td bgcolor="#FFFFFF"><span>

                                                            <input name="detalle_int2" id="detalle_int2" type="text" maxlength="30" readonly style="width:99%" />

                                                        </span></td>



                                                </tr>

                                                <tr style="padding:3%;">

                                                    <td><span>Interior:</span></td>

                                                    <td bgcolor="#FFFFFF"><span>

                                                            <select id="interior3" name="interior3" style="width:96%">
                                                                <option value="">Seleccione...</option>
                                                                <option>APARTAMENTO</option>
                                                                <option>BARRIO</option>
                                                                <option>BLOQUE</option>
                                                                <option>CASA</option>
                                                                <option>CIUDADELA</option>
                                                                <option>CONJUNTO</option>
                                                                <option>CONJUNTO RESIDENCIAL</option>
                                                                <option>EDIFICIO</option>
                                                                <option>ENTRADA</option>
                                                                <option>ETAPA</option>
                                                                <option>INTERIOR</option>
                                                                <option>MANZANA</option>
                                                                <option>NORTE</option>
                                                                <option>OFICINA</option>
                                                                <option>OCCIDENTE</option>
                                                                <option>ORIENTE</option>
                                                                <option>PENTHOUSE</option>
                                                                <option>PISO</option>
                                                                <option>PORTERIA</option>
                                                                <option>SOTANO</option>
                                                                <option>SUR</option>
                                                                <option>TORRE</option>
                                                            </select>

                                                        </span></td>

                                                    <td><span>Detalle Interior:</span></td>
                                                    <td bgcolor="#FFFFFF"><span>
                                                            <input name="detalle_int3" id="detalle_int3" type="text" maxlength="30" style="width:99%" readonly />
                                                        </span></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        <span>Fecha de Nacimiento<span class="asterisco">*</span></span>
                                    </td>
                                    <td width="30%">
                                        <input type="date" name="fecha_nacimiento" id="fecha_nacimiento" max="<?php echo date('Y-m-d'); ?>" value="<?php echo $fila['FECHA_NACIMINETO_PACIENTE']; ?>" />
                                    </td>
                                    <td>
                                        <span>Edad</span>
                                    </td>
                                    <td>
                                        <input type="text" name="edad" id="edad" readonly="readonly" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span>Acudiente&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                    </td>
                                    <td>
                                        <input type="text" name="acudiente" id="acudiente" readonly="readonly" value="<?php echo $fila['ACUDIENTE_PACIENTE'] ?>" />
                                    </td>
                                    <td>
                                        <span>Telefono del Acudiente&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                    </td>
                                    <td>
                                        <input type="text" name="telefono_acudiente1" id="telefono_acudiente1" value="<?php echo $fila['TELEFONO_ACUDIENTE_PACIENTE'] ?>" readonly="readonly" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span>Clasificacion Patologica<span class="asterisco">*</span></span>
                                    </td>
                                    <td>
                                        <span style="width:30%;">
                                            <input type="text" name="clasificacion_patologicas" id="clasificacion_patologicas" value="<?php echo $fila['CLASIFICACION_PATOLOGICA_TRATAMIENTO'] ?>" readonly="readonly">
                                        </span>
                                    </td>
                                    <td>
                                        <span>Fecha Inicio Terapia<span class="asterisco">*</span></span>
                                    </td>

                                    <td>
                                        <input type="date" name="fecha_ini_terapia" id="fecha_ini_terapia" value="<?php echo $fila['FECHA_INICIO_TERAPIA_TRATAMIENTO'] ?>" />
                                    </td>
                                </tr>
                            </table>

                    </div>
                </div>



                <div class="AccordionPanel">
                    <div class="AccordionPanelTab">GENERAL</div>
                    <div class="AccordionPanelContent">

                        <br />

                        <table width="93.5%">

                            <?php

                            $fecha_actual = date('Y-m-d');
                            $fecha_rec_act = explode("-", $fecha_actual);
                            $anio_act = $fecha_rec_act[0]; // a�o
                            $mes_act = $fecha_rec_act[1]; // mes
                            $dia_act = $fecha_rec_act[2]; // dia
                            $dato = ((int)$mes_act);
                            $ID = $fila['ID_PACIENTE'];

                            ?>

                            <tr>
                                <td>
                                    <span>Se Logro la Comunicacion<span class="asterisco">*</span></span>
                                    <br />
                                    <br />
                                </td>

                                <td>
                                    <input type="radio" name="logro_comunicacion" id="logro_comunicacion" style=" width:20%; display:none" value="" checked="checked" />
                                    <input type="radio" name="logro_comunicacion" id="logro_comunicacion" style=" width:20%;" value="SI" />SI
                                    <input type="radio" name="logro_comunicacion" id="logro_comunicacion" style=" width:20%;" value="NO" />NO
                                    <br />
                                    <br />
                                </td>

                                <td class="tit">
                                    <span>Motivo de Comunicaci&oacute;n<span class="asterisco">*</span></span>
                                    <br />
                                    <br />
                                </td>

                                <td style="width:30%;">
                                    <select type="text" name="motivo_comunicacion" id="motivo_comunicacion">
                                        <option value="">Seleccione...</option>
                                        <option>Actualizacion de datos</option>
                                        <option>Egreso</option>
                                        <option>Seguimiento</option>
                                        <option>Respuesta de Caso</option>
                                        <option>Remision de Caso</option>
                                    </select>
                                    <br />
                                    <br />
                                </td>
                            </tr>

                            <tr>
                                <td class="tit">
                                    <span>Medio de Contacto<span class="asterisco">*</span></span>
                                    <br />
                                    <br />
                                </td>

                                <td style="width:30%;">
                                    <select type="text" name="medio_contacto" id="medio_contacto">
                                        <option value="">Seleccione...</option>
                                        <option>Presencial</option>
                                        <option>llamada por contacto</option>

                                    </select>

                                    <br />
                                    <br />
                                </td>

                                <td>
                                    <span>Tipo de Llamada<span class="asterisco">*</span></span>
                                    <br />
                                    <br />
                                </td>

                                <td>

                                    <select type="text" name="tipo_llamada" id="tipo_llamada">
                                        <option value="">Seleccione...</option>
                                        <option>Entrada</option>
                                        <option>Salida</option>
                                    </select>

                                    <br />

                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span>Motivo de No Comunicaci&oacute;n</span>
                                    <br />
                                    <br />
                                </td>
                                <td>
                                    <select type="text" name="motivo_no_comunicacion" id="motivo_no_comunicacion">
                                        <option value="">Seleccione...</option>
                                        <option>Apagado</option>
                                        <option>No Esta</option>
                                        <option>No Contesta</option>
                                        <option>No Vive Ahi</option>
                                        <option>Numero Equivocado</option>
                                        <option>Telefono Ocupado</option>
                                        <option>Telefono Fuera de Servicio</option>
                                        <option>Otro</option>
                                    </select>
                                    <br />
                                    <br />
                                </td>

                                <td>
                                    <span>Numero de Intentos<span class="asterisco">*</span></span>
                                    <br />
                                    <br />
                                </td>
                                <td>
                                    <input type="text" name="via_recepcion" id="via_recepcion" />
                                    <br />
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span>Asegurador<span class="asterisco">*</span></span>
                                    <br />
                                    <br />
                                </td>
                                <td>
                                    <input name="asegurador" id="asegurador" type="text" style="width:78%;" value="<?php echo $fila['ASEGURADOR_TRATAMIENTO'] ?>" />
                                    <!-- <select type="text" name="asegurador" id="asegurador" >
         <option><?php echo $fila['ASEGURADOR_TRATAMIENTO'] ?></option>
    <?php $Seleccion = mysqli_query($conex, "SELECT ASEGURADOR FROM `bayer_asegurador_operador_logistico` WHERE DEPARTAMENTO='" . $fila['DEPARTAMENTO_PACIENTE'] . "' GROUP BY ASEGURADOR ORDER BY ASEGURADOR  ASC");
                            while ($fil = mysqli_fetch_array($Seleccion)) {
                                $ASEGURADOR = $fil['ASEGURADOR'];
                                echo "<option>" . $ASEGURADOR . "</option>";
                            } ?>

    </select> -->
                                    <br />
                                    <br />
                                </td>
                                <td><span>Medico<span class="asterisco">*</span></span><br />
                                    <br />
                                </td>
                                <td><?php $MEDICO = $fila['MEDICO_TRATAMIENTO'];
                                    $EXAMEN = $fila['EXAMEN']; ?>
                                    <input type="text" name="medico" id="medico" maxlength="60" value="<?php echo $MEDICO; ?>" />
                                </td>
                            </tr>
                            <!-- Examen campo new--->
                            <tr>
                                <td>
                                    <span>Examen</span>
                                    <br />
                                    <br />
                                </td>
                                <td>
                                    <select name='examen' id='examen'>
                                        <?php if ($EXAMEN == "Ecografia Hepato-biliar (EC)") {

                                            echo "
		<option style='color:#1289b9;'><strong>" . $EXAMEN . "</strong></option>
        <option>Alfa-fetoproteina (AFP)</option>
        <option>Tomografia axial computarizada (TAC), de cuatro fases dinamicas especializada en nodulo hepatico</option>
        <option>Resonancia Magnetica (RM) sin y con medio de contraste</option>
        <option>Creatinina</option>
        <option>Nitrogeno ureico en sangre (BUN)</option>";
                                        } elseif ($EXAMEN == "Alfa-fetoproteina (AFP)") {

                                            echo "
		<option style='color:#1289b9;'><strong>" . $EXAMEN . "</strong></option>
		<option>Ecografia Hepato-biliar (EC)</option>
        <option>Tomografia axial computarizada (TAC), de cuatro fases dinamicas especializada en nodulo hepatico</option>
        <option>Resonancia Magnetica (RM) sin y con medio de contraste</option>
        <option>Creatinina</option>
        <option>Nitrogeno ureico en sangre (BUN)</option>";
                                        } elseif ($EXAMEN == "Tomografia axial computarizada (TAC), de cuatro fases dinamicas especializada en nodulo hepatico") {

                                            echo "
		<option style='color:#1289b9;'><strong>" . $EXAMEN . "</strong></option>
		<option>Ecografia Hepato-biliar (EC)</option>
        <option>Alfa-fetoproteina (AFP)</option>
        <option>Resonancia Magnetica (RM) sin y con medio de contraste</option>
        <option>Creatinina</option>
        <option>Nitrogeno ureico en sangre (BUN)</option>";
                                        } elseif ($EXAMEN == "Resonancia Magnetica (RM) sin y con medio de contraste") {
                                            echo "
		<option style='color:#1289b9;'><strong>" . $EXAMEN . "</strong></option>
		<option>Ecografia Hepato-biliar (EC)</option>
        <option>Alfa-fetoproteina (AFP)</option>
        <option>Tomografia axial computarizada (TAC), de cuatro fases dinamicas especializada en nodulo hepatico</option>
        <option>Creatinina</option>
        <option>Nitrogeno ureico en sangre (BUN)</option>";
                                        } elseif ($EXAMEN == "Creatinina") {
                                            echo "
		<option style='color:#1289b9;'><strong>" . $EXAMEN . "</strong></option>
		<option>Ecografia Hepato-biliar (EC)</option>
        <option>Alfa-fetoproteina (AFP)</option>
        <option>Tomografia axial computarizada (TAC), de cuatro fases dinamicas especializada en nodulo hepatico</option>
        <option>Resonancia Magnetica (RM) sin y con medio de contraste</option>
        <option>Nitrogeno ureico en sangre (BUN)</option>";
                                        } elseif ($EXAMEN == "Nitrogeno ureico en sangre (BUN)") {
                                            echo "
		<option style='color:#1289b9;'><strong>" . $EXAMEN . "</strong></option>
		<option>Ecografia Hepato-biliar (EC)</option>
        <option>Alfa-fetoproteina (AFP)</option>
        <option>Tomografia axial computarizada (TAC), de cuatro fases dinamicas especializada en nodulo hepatico</option>
        <option>Resonancia Magnetica (RM) sin y con medio de contraste</option>
        <option>Creatinina</option>";
                                        } else {
                                            echo "<option>Seleccione...</option>
		<option>Ecografia Hepato-biliar (EC)</option>
        <option>Alfa-fetoproteina (AFP)</option>
        <option>Tomografia axial computarizada (TAC), de cuatro fases dinamicas especializada en nodulo hepatico</option>
        <option>Resonancia Magnetica (RM) sin y con medio de contraste</option>
        <option>Creatinina</option>
        <option>Nitrogeno ureico en sangre (BUN)</option>
        ";
                                        }         ?>
                                    </select>
                                    <br />
                                    <br />
                                </td>
                                <td>
                                    <span>Centro diagnostico</span>
                                    <br />
                                    <br />
                                </td>
                                <td>
                                    <input type="text" name="centro_diagnostico" id="centro_diagnostico" />
                                    <br />
                                    <br />
                                </td>
                            </tr>


                            <!------------------------->

                            <tr>
                                <td>
                                    <span>Fecha de la Pr&oacute;xima Llamada<span class="asterisco">*</span></span>
                                    <br />
                                    <br />
                                </td>
                                <td>
                                    <input type="date" name="fecha_proxima_llamada" id="fecha_proxima_llamada" min="<?php echo date('Y-m-d'); ?>" />
                                    <br />
                                    <br />
                                </td>
                                <td>
                                    <span>Motivo de Proxima Llamada<span class="asterisco">*</span></span>
                                    <br />
                                    <br />
                                </td>
                                <td>
                                    <select type="text" name="motivo_proxima_llamada" id="motivo_proxima_llamada">
                                        <option value="">Seleccione...</option>
                                        <option>Actualizacion de Datos</option>
                                        <option>Campanas</option>
                                        <option>Cumpleanos</option>
                                        <option>Egreso</option>
                                        <option>Encuestas</option>
                                        <option>Ingreso</option>
                                        <option>Reclamacion</option>
                                        <option>Remision de Caso</option>
                                        <option>Respuesta de Caso</option>
                                        <option>Seguimiento</option>
                                    </select>
                                    <br />
                                    <br />
                                </td>
                            </tr>

                            <!------/////////////////////////////-------------->


                            <tr>

                                <input style="text-transform:capitalize;" type="hidden" name="MEDICAMENTO" id="MEDICAMENTO" value="<?php echo $fila['PRODUCTO_TRATAMIENTO'] ?>" />


                                <td>
                                    <span>Autor</span>
                                    <br />
                                    <br />
                                </td>
                                <td>
                                    <input type="text" name="autor" id="autor" readonly="readonly" value="<?php echo $usuname_peru ?>" />
                                    <br />
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span>Descripcion de Comunicaci&oacute;n</span>
                                    <br />
                                    <br />
                                </td>
                                <td colspan="3">
                                    <textarea style="width:98%; height:72.5px;" id="descripcion_comunicacion" name="descripcion_comunicacion" onKeyDown="return filtro(1)"></textarea>

                                    <br />

                                    <br />

                                </td>

                            </tr>

                        </table>

                    <?php

                        }

                    ?>
                    <br />
                    <br />
                    </div>
                </div>


                <div class="AccordionPanel">
                    <div class="AccordionPanelTab" style="padding:5px">COMUNICACIONES</div>
                    <div class="AccordionPanelContent">


                        <?PHP

                        ///////////////////////////////////////////////////////

                        $gestion = mysqli_query($conex, "SELECT * FROM `bayer_gestiones` WHERE `ID_PACIENTE_FK2` = '" . $ID_PACIENTE2 . "' ORDER BY `FECHA_COMUNICACION` DESC");

                        echo mysqli_error($conex);

                        echo "<table width=100% border=1 rules=all  align=left class=Estilo2 >";
                        echo "<tr style='border:1px solid #fff'>";
                        echo "<th class=AccordionPanelTab><strong>FECHA DE GESTION</strong></th>";
                        echo "<th class=AccordionPanelTab><strong>DESCRIPCION</strong></th>";
                        echo "<th class=AccordionPanelTab><strong>FECHA PROXIMO CONTACTO</strong></th>";
                        echo "<th class=AccordionPanelTab><strong>AUTOR</strong></th>";
                        echo "<th class=AccordionPanelTab><strong>MOTIVO COMUNICACION GESTION</strong></th>";
                        echo "<td class=AccordionPanelTab><strong>TIPO ADJUNTO</strong></td>";
                        echo "<td class=AccordionPanelTab><strong>CARCHIVO ADJUNTO</strong></td>";
                        echo "</tr>";

                        $numges = 1;
                        while ($fila2 = mysqli_fetch_array($gestion)) {  //echo $fila2['ID_PACIENTE_FK2'];
                            /* echo "<tr bgcolor=#5C9DD1 rules=cols>";
	echo "<td colspan=5 height=15><strong>Gestion : ".$numges."</strong></td>";
	echo "</tr>";*/
                            echo "<tr>";
                            echo "<td>" . $fila2['FECHA_COMUNICACION'] . "</td>";
                            //echo "<td>".$fila2['DESCRIPCION_COMUNICACION_GESTION']."</td>";
                            echo "<td>";
                        ?>
                            <textarea name="observaciones" cols="60" rows="2" readonly="readonly" id="observaciones" class="letra" style="text-transform:uppercase"><?php echo $fila2['DESCRIPCION_COMUNICACION_GESTION']; ?></textarea>
                            <?PHP
                            echo "</td>";
                            echo "<td>" . $fila2['FECHA_PROGRAMADA_GESTION'] . "</td>";
                            echo "<td>" . $fila2['AUTOR_GESTION'] . "</td>";
                            echo "<td>" . $fila2['MOTIVO_COMUNICACION_GESTION'] . "</td>";
                            if ($privilegios == '1') { ?>

                                <td> <input name="id_paciente" id="id_paciente" type="text" style="display:none;" value="<?php echo $ID_PACIENTE; ?>" />
                                    <input name="ID" id="ID" type="text" style="display:none;" value="<?php echo $fila2['ID_GESTION']; ?>" />
                                    <samp><?php echo $fila2['TIPO_ADJUNTO']; ?>
                                        <input name="TIPO_ADJUNTO" id="TIPO_ADJUNTO" type="hidden" maxlength="25" style="width:80%" value="<?php echo $fila2['TIPO_ADJUNTO']; ?>" /></samp>
                                </td>

                            <?php    } else if ($privilegios == '2') { ?>


                                <td> <input name="id_paciente" id="id_paciente" type="text" style="display:none;" value="<?php echo $ID_PACIENTE; ?>" />
                                    <input name="ID" id="ID" type="text" style="display:none;" value="<?php echo $fila2['ID_GESTION']; ?>" />
                                    <samp><?php echo $fila2['TIPO_ADJUNTO']; ?>
                                        <input name="TIPO_ADJUNTO" id="TIPO_ADJUNTO" type="hidden" maxlength="25" style="width:80%" value="<?php echo $fila2['TIPO_ADJUNTO']; ?>" /></samp>
                                </td>


                                <?php            } else {
                            }



                            ///////////////////////////////////////////////////////

                            $ID_GES = $fila2['ID_GESTION'];

                            $dir = "../ADJUNTOS_BAYER/$ID_GES";

                            if (file_exists($dir)) {
                                $directorio = opendir($dir);
                                while ($archivo = readdir($directorio)) {
                                    if ($archivo == '.' or $archivo == '..') {
                                    } else {
                                        $enlace = $dir . "/" . $archivo;
                                ?>

                                        <td>

                                            <a class="highslide" onclick="return hs.expand(this)">

                                                <img src="<?php echo $enlace; ?>" alt="" title="Click to enlarge" height="100" width="100" onclick="javascript:this.width=500;this.height=500" ondblclick="javascript:this.width=100;this.height=100" /></a>
                                            <a href="<?php echo $enlace; ?>">ver</a>
                                            <br />
                                            <br />
                                        </td>
                                <?php
                                    }
                                }
                                closedir($directorio);
                            } else {        ?>
                                <td>
                                </td>
                        <?php

                                //echo "El fichero $dir no existe";

                            }



                            echo "</tr>";

                            $numges = $numges + 1;
                        }

                        echo "</table>";

                        echo "<br />";

                        ?>

                    </div>

                </div>
                <div class="AccordionPanel">

                    <div class="AccordionPanelTab">NOTAS Y ADJUNTOS</div>

                    <div class="AccordionPanelContent">
                        <br />
                        <br />
                        <div style="width:91.4%;margin-left: 15px; ">
                            <textarea class="tf w-input" style="width:100%; height:100px" id="txtCurp" name="nota" maxlength="5000" onkeypress="return check(event)" placeholder="Nota" id="test"></textarea>

                        </div>
                        <br />
                        <br />

                        <div style=" display: flex;  flex-wrap: wrap; margin-left: 15px; ">
                            <div style="flex: 0 0 50%;  max-width: 50%;">
                                <span>Tipo de Adjunto<span class="asterisco"></span></span>
                                <input type="radio" name="tipo_adjunto" id="tipo_adjunto" style=" width:5%; display:none" value="" checked="checked" />
                                <input type="radio" name="tipo_adjunto" id="tipo_adjunto" style=" width:5%;" value="Vaucher" />Vaucher
                                <input type="radio" name="tipo_adjunto" id="tipo_adjunto" style=" width:5%;" value="CI" />CI
                                <div class="input__row uploader">
                                    <div id="inputval" class="input-value"></div>
                                    <label for="archivo"></label>
                                    <input type="file" class="upload" name="archivo" id="archivo" class="aceptar" multiple></input>
                                </div>

                                <script>
                                    $('#archivo').on('change', function() {
                                        $('#inputval').text($(this).val());
                                    });
                                </script>
                            </div>

                        </div>
                        <div style="flex: 0 0 33.333333%;
    max-width: 33.333333%;"></div>
                        <div style="flex: 0 0 33.333333%;
    max-width: 33.333333%;"></div>

                        <center>

                            <?php

                            if ($privilegios != 5) {

                            ?>

                                <input id="registrar" name="registrar" type="submit" value="REGISTRAR" class="btn_registrar" onClick="return validar(seguimiento,2)" />

                            <?php

                            }

                            ?>
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                    </div>
                </div>
            </div>

        </form>

        <script type="text/javascript">
            function check(e) {
                tecla = (document.all) ? e.keyCode : e.which;

                //Tecla de retroceso para borrar, siempre la permite
                if (tecla == 8) {
                    return true;
                }

                // Patron de entrada, en este caso solo acepta numeros,letras, guiones, puntos, parentesis y comas.
                patron = /[A-Za-z0-9-.,)(., ]/;
                tecla_final = String.fromCharCode(tecla);
                return patron.test(tecla_final);
            }


            var Accordion1 = new Spry.Widget.Accordion("Accordion1");
        </script>

    </body>

<?php

} else {

?>

    <script type="text/javascript">
        window.onload = window.top.location.href = "../logica/cerrar_sesion2.php";
    </script>

<?php

}

?>


</html>