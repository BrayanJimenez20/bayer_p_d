<?php
include('../logica/session.php');
//require('./../datos/parse_str.php');
header("Content-Type: text/html;charset=utf-8");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html lang="es">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Documento sin titulo</title>
	<link type="text/css" rel="stylesheet" href="css/estilo_form_paciente.css" />
	<script src="css/SpryAssets/SpryAccordion.js" type="text/javascript"></script>
	<link href="css/SpryAssets/SpryAccordion.css" rel="stylesheet" type="text/css" />
	<link href="css/estilo_form_paciente.css" type="text/css" />
	<script type="text/javascript" src="js/calcular_edad.js"></script>
	<script type="text/javascript" src="js/direccion.js"></script>
	<script type="text/javascript" src="js/validar_campos_pacientes.js"></script>
	<script type="text/javascript" src="js/validaciones.js"></script>
	<script type="text/javascript" src="js/validar_caracteres.js"></script>
	<script type="text/javascript" src="../presentacion/js/jquery.js"></script>
	<script type="text/javascript" src="../logica/js/validaciones.js"></script>
	<script type="text/javascript" src="../logica/js/validaciones_campos.js"></script>
	<script>
		function moveOption(e1, e2) {
			for (var i = 0; i < e1.options.length; i++) {
				if (e1.options[i].selected) {
					var e = e1.options[i];
					e2.options.add(new Option(e.text, e.value));
					e1.remove(i);
					i = i - 1
				}
			}
			document.getElementById('examen_p').value = getvalue(document.getElementById('examen'));
		}

		function moveOption2(e1, e2) {
			for (var i = 0; i < e1.options.length; i++) {
				if (e1.options[i].selected) {
					var e = e1.options[i];
					e2.options.add(new Option(e.text, e.value));
					e1.remove(i);
					i = i - 1
				}
			}
			document.getElementById('documentos_recibidos_p').value = getvalue(document.getElementById('documentos_recibidos'));
		}

		function getvalue(geto) {
			var resultArray = new Array();
			for (var i = 0; i < geto.options.length; i++) {
				resultArray.push(geto.options[i].value);
			}
			return resultArray.join();
		}

		function trat_previo1(sel) {
			if (sel.value == "NO ENCONTRADO") {
				divC = document.getElementById("otro_asegurador");
				divC.style.display = "";
			}
			if (sel.value != "NO ENCONTRADO") {
				divC = document.getElementById("otro_asegurador");
				divC.style.display = "none";
			}
		}
	</script>
	<script type="text/javascript">
		function trat_previo(sel) {
			if (sel.value == "Otro") {
				divC = document.getElementById("otro_tratamiento");
				divC.style.display = "";
			}
			if (sel.value != "Otro") {
				divC = document.getElementById("otro_tratamiento");
				divC.style.display = "none";
			}
		}

		function mostrar_departamento() {
			var pais = $('#pais').val();
			$("#departamento").html('<img src="imgagenes/cargando.gif" />');
			$.ajax({
				url: '../presentacion/departamentos.php',
				data: {
					pais: pais,
				},
				type: 'post',
				beforeSend: function() {
					$('#departamento').attr('disabled');
					$("#departamento").html("Procesando, espere por favor" + '<img src="img/cargando.gif" />');
				},
				success: function(data) {
					$('#departamento').removeAttr("disabled");
					$('#departamento').html(data);
				}
			})
		}

		function mostrar_ciudades() {
			var departamento = $('#departamento').val();
			$("#ciudad").html('<img src="imgagenes/cargando.gif" />');
			$.ajax({
				url: '../presentacion/ciudades.php',
				data: {
					dep: departamento,
				},
				type: 'post',
				beforeSend: function() {
					$('#ciudad').attr('disabled');
					$("#ciudad").html("Procesando, espere por favor" + '<img src="img/cargando.gif" />');
				},
				success: function(data) {
					$('#ciudad').removeAttr("disabled");
					$('#ciudad').html(data);
				}
			})
		}
	</script>
	<script>
		$(document).ready(function() {
			function reclamo() {
				$("#causa_no_reclamacion option:eq(0)").attr("selected", "selected");
				$("#fecha_reclamacion").val('');
				var reclamo = $('#reclamo').val();
				var MEDICAMENTO = $('#producto_tratamiento').val();
				if (reclamo == '') {
					$("#causa").css('display', 'none');
					$('#causa_no_reclamacion').css('display', 'none');
					$("#fecha_reclamacion_span").css('display', 'none');
					$('#fecha_reclamacion').css('display', 'none');
					$("#consecutivo_betaferon_span").css('display', 'none');
					$('#consecutivo_betaferon').css('display', 'none');
					$('#numero_cajas option:eq(0)').attr('selected', 'selected');
					$('#tipo_numero_cajas option:eq(0)').attr('selected', 'selected');
					$('#numero_cajas').attr('disabled', 'disabled');
					$('#tipo_numero_cajas').attr('disabled', 'disabled');
				}
				if (reclamo == 'NO') {
					$("#causa").css('display', 'block');
					$('#causa_no_reclamacion').css('display', 'block');
					$("#fecha_reclamacion_span").css('display', 'none');
					$('#fecha_reclamacion').css('display', 'none');
					$("#consecutivo_betaferon_span").css('display', 'none');
					$('#consecutivo_betaferon').css('display', 'none');
					$('#numero_cajas option:eq(0)').attr('selected', 'selected');
					$('#tipo_numero_cajas option:eq(0)').attr('selected', 'selected');
					$('#causa_no_reclamacion option:eq(1)').attr('selected', 'selected');
					$('#numero_cajas').attr('disabled', 'disabled');
					$('#tipo_numero_cajas').attr('disabled', 'disabled');
				}
				if (reclamo == 'SI' && MEDICAMENTO == 'BETAFERON CMBP X 15 VPFS (3750 MCG) MM') {
					$("#fecha_reclamacion").val($('#fecha_reclamacion').prop('defaultValue'));
					$("#consecutivo").val($('#consecutivo').prop('defaultValue'));
					$("#consecutivo_betaferon_span").css('display', 'block');
					$('#consecutivo_betaferon').css('display', 'block');
					$("#fecha_reclamacion_span").css('display', 'block');
					$('#fecha_reclamacion').css('display', 'block');
					$("#causa").css('display', 'none');
					$('#causa_no_reclamacion').css('display', 'none');
					$('#numero_cajas').removeAttr('disabled');
					$('#tipo_numero_cajas').removeAttr('disabled');
				} else {
					if (reclamo == 'SI') {
						$("#consecutivo_betaferon_span").css('display', 'none');
						$('#consecutivo_betaferon').css('display', 'none');
						$("#fecha_reclamacion_span").css('display', 'block');
						$('#fecha_reclamacion').css('display', 'block');
						$("#causa").css('display', 'none');
						$('#causa_no_reclamacion').css('display', 'none');
						$('#numero_cajas').removeAttr('disabled');
						$('#tipo_numero_cajas').removeAttr('disabled');
						$("#fecha_reclamacion").val($('#fecha_reclamacion').prop('defaultValue'));
					}
				}
			}
			reclamo();
			$("#reclamo").change(function() {
				reclamo();
			});
			$("#departamento").change(function() {});
			$("#asegurador").change(function() {
				operador();
			});
			$('#cambio').click(function() {
				$('#cambio_direccion').toggle();
				$('#DIRECCION').val('');
				$("#VIA option:eq(0)").attr("selected", "selected");
				$("#interior option:eq(0)").attr("selected", "selected");
				$("#interior2 option:eq(0)").attr("selected", "selected");
				$("#interior3 option:eq(0)").attr("selected", "selected");
				$("#TERAPIA option:eq(0)").attr("selected", "selected");
				$('#detalle_via').val('');
				$('#detalle_int').val('');
				$('#detalle_int2').val('');
				$('#detalle_int3').val('');
				$('#numero').val('');
				$('#numero2').val('');
			});
			var via = $('#VIA').val();
			var dt_via = $('#detalle_via').val();
			$('#VIA').change(function() {
				dir();
			});
			$('#detalle_via').change(function() {
				dir();
			});
			$('#numero').change(function() {
				dir();
			});
			$('#numero2').change(function() {
				dir();
			});
			$('#interior').change(function() {
				dir();
			});
			$('#detalle_int').change(function() {
				dir();
			});
			$('#interior2').change(function() {
				dir();
			});
			$('#detalle_int2').change(function() {
				dir();
			});
			$('#interior3').change(function() {
				dir();
			});
			$('#detalle_int3').change(function() {
				dir();
			});
		});
		$(document).ready(function() {
			var fecha = $('input[name=fecha_nacimiento]').val();
			if (fecha != '') {
				var edad = nacio(fecha);
				$("#edad").val(edad);
			}
			$("input[name=fecha_nacimiento]").change(function() {
				var fecha = $('input[name=fecha_nacimiento]').val();
				var edad = nacio(fecha);
				$("#edad").val(edad);
			});
			$("#medico").change(function() {
				$("#medico_nuevo").val('');
				var medico = $('#medico').val();
				if (medico == 'Otro') {
					$('#medico_nuevo').css('display', 'inline-block');
					$('#cual_medico').css('display', 'inline-block');
				}
				if (medico != 'Otro') {
					$('#medico_nuevo').css('display', 'none');
					$('#cual_medico').css('display', 'none');
				}
			});
			$("#producto_tratamiento").click(function mostrar_nebu() {
				$("#nebulizaciones").val('');
				var producto_tratamiento = $('#producto_tratamiento').val();
				if (producto_tratamiento == 'VENTAVIS 10 1SOL/2ML X30AMP(Conse) MM') {
					$('#span_nebulizaciones').css('display', 'inline-block');
					$('#div_nebulizaciones').css('display', 'inline-block');
				}
				if (producto_tratamiento != 'VENTAVIS 10 1SOL/2ML X30AMP(Conse) MM') {
					$('#span_nebulizaciones').css('display', 'none');
					$('#div_nebulizaciones').css('display', 'none');
				}
			});
			$("#producto_tratamiento").click(function mostrar_tabletas() {
				$("#numero_tabletas_diarias").val('');
				var reclamo = $('#reclamo').val();
				var producto_tratamiento = $('#producto_tratamiento').val();
				if (reclamo == 'SI' && producto_tratamiento == 'NEXAVAR 200MGX60C(12000MG)INST' || producto_tratamiento == 'ADEMPAS') {
					$('#span_tabletas_diarias').css('display', 'inline-block');
					$('#div_tabletas_diarias').css('display', 'inline-block');
				}
				if (producto_tratamiento != 'NEXAVAR 200MGX60C(12000MG)INST' && producto_tratamiento != 'ADEMPAS') {
					$('#span_tabletas_diarias').css('display', 'none');
					$('#div_tabletas_diarias').css('display', 'none');
				}
				if (reclamo == 'NO' || reclamo == '') {
					$('#span_tabletas_diarias').css('display', 'none');
					$('#div_tabletas_diarias').css('display', 'none');
				}
			});
			$("#reclamo").click(function mostrar_tabletas2() {
				$("#numero_tabletas_diarias").val('');
				var reclamo = $('#reclamo').val();
				var producto_tratamiento = $('#producto_tratamiento').val();
				if (reclamo == 'SI' && producto_tratamiento == 'NEXAVAR 200MGX60C(12000MG)INST' || producto_tratamiento == 'ADEMPAS') {
					$('#span_tabletas_diarias').css('display', 'inline-block');
					$('#div_tabletas_diarias').css('display', 'inline-block');
				}
				if (producto_tratamiento != 'NEXAVAR 200MGX60C(12000MG)INST' && producto_tratamiento != 'ADEMPAS') {
					$('#span_tabletas_diarias').css('display', 'none');
					$('#div_tabletas_diarias').css('display', 'none');
				}
				if (reclamo == 'NO' || reclamo == '') {
					$('#span_tabletas_diarias').css('display', 'none');
					$('#div_tabletas_diarias').css('display', 'none');
				}
			});
			$("#producto_tratamiento").change(function() {
				$('nombre_producto').val('');
				mostrar_dosis();
			});
			$('#producto_tratamiento').change(function() {
				materiales();
				clasificacion();
				status();
			});
			$("#tipo_envio").change(function() {
				mostrar_producto();
			});
			$("#agregar_nuevo").click(function() {
				$('#div_material_agregar').css('display', 'block');
				//$("#tipo_envio option:eq(0)").attr("selected", "selected");
				$('#div_agregar').css('visibility', 'hidden');
			});
		});
	</script>
	<script type="text/javascript" src="../logica/js/campos_form_nuevo.js"></script>
	<script type="text/javascript" src="../logica/js/validaciones_form_nuevo.js"></script>
	<style>
		td {
			padding: 6px;
			background-color: transparent;
		}

		.input__row {
			margin-top: 10px;
		}

		/* Radio button */
		/* Upload button */
		.upload {
			display: none;
		}

		.uploader {
			border: 1px solid #12a9e3;
			width: 300px;
			position: relative;
			height: 30px;
			display: flex;
		}

		.uploader .input-value {
			width: 250px;
			padding: 5px;
			overflow: hidden;
			text-overflow: ellipsis;
			line-height: 25px;
			font-family: sans-serif;
			font-size: 16px;
		}

		.uploader label {
			cursor: pointer;
			margin: 0;
			width: 30px;
			height: 30px;
			position: absolute;
			right: 0;
			background: #17a8e391 url('https://www.interactius.com/wp-content/uploads/2017/09/folder.png') no-repeat center;
		}
	</style>
</head>
<?php
$string_intro = getenv("QUERY STRING");
parse_str($string_intro);
require('../datos/conex.php');
$DIAS_ANTES = date('Y-m-d', strtotime('-31 day'));
if ($privilegios != '' && $usuname_peru != '') {
?>

	<body class="body" style="width:80.9%;margin-left:12%;">
		<form id="paciente_nuevo" name="paciente_nuevo" action="../logica/insertar_datos.php" method="post" enctype="multipart/form-data" class="letra">
			<div id="Accordion1" class="Accordion" tabindex="0">
				<div class="AccordionPanel">
					<div class="AccordionPanelTab" style="padding:5px"><strong>GENERAL</strong></div>
					<div class="AccordionPanelContent">
						<table width="100%" border="0">
							<tr>
								<td width="20%">
									<span>Codigo De Usuario</span>
								</td>
								<td width="30%">
									<?php
									$Seleccion = mysqli_query($conex, "SELECT ID_PACIENTE FROM `bayer_pacientes` WHERE ID_PACIENTE != '' ORDER BY ID_PACIENTE DESC LIMIT 1");
									while ($fila = mysqli_fetch_array($Seleccion)) {
										$ID_PA = $fila['ID_PACIENTE'] + 1;
										function Zeros($numero, $largo)
										{
											$resultado = $numero;
											while (strlen($resultado) < $largo) {
												$resultado = "0" . $resultado;
											}
											return $resultado;
										}
										$ID_PACIENTE = Zeros($ID_PA, 5);
									}
									?>
									<input type="radio" name="logro_comunicacion" id="logro_comunicacion" style=" width:20%;display:none" value="SI" checked="checked" />
									<input name="codigo_usuario" class="input_t" type="text" id="codigo_usuario" max="10" readonly="readonly" value="<?php echo 'PAD' . $ID_PACIENTE; ?>" />
									<input name="codigo_usuario2" class="input_t" type="hidden" id="codigo_usuario2" max="10" readonly="readonly" value="<?php echo $ID_PACIENTE; ?>" /> <input name="usuname" id="usuname" value="<?php echo $usuname_peru; ?>" type="hidden" />
								</td>
								<td width="20%">
									<span>Estado<span class="asterisco">*</span></span>
								</td>
								<td width="30%">
									<select type="text" name="estado_paciente" id="estado_paciente" required>
										<option value="">Seleccione...</option>
										<option>Proceso</option>
										<option>Activo</option>
										<option>Cierre Efectivo</option>
										<option>Cierre No efectivo</option>
									</select>
								</td>
							</tr>
							<tr>
								<td width="20%">
									<span>Fecha de Activacion</span>
								</td>
								<td width="30%">
									<input type="date" name="fecha_activacion" id="fecha_activacion" value="<?php echo date('Y-m-d'); ?>" readonly="readonly" />
								</td>
								<td>
									<span>Nombre Medico<span class="asterisco">*</span></span>
								</td>
								<td>
									<input type="text" class="input_t" name="nombre_medico" id="nombre_medico" required>
								</td>
							</tr>
							<tr>
								<td>
									<span>Apellidos Medico<span class="asterisco">*</span></span>
								</td>
								<td>
									<input type="text" class="input_t" name="apellidos_medico" id="apellidos_medico" required>
								</td>
								<td>
									<span>Telefono Medico<span class="asterisco">*</span></span>
								</td>
								<td>
									<input type="number" class="input_t" name="telefono" id="telefono" required>
								</td>
							</tr>
							<tr>
								<td>
									<span>Correo Medico<span class="asterisco">*</span></span>
								</td>
								<td>
									<input type="text" name="correo_medico" id="correo_medico" class="input_t" required>
								</td>
								<td width="20%">
									<span>Especialidad</span>
								</td>
								<td width="30%">
									<input class="input_t" type="text" name="especialidad" id="especialidad">
								</td>
							</tr>
							<tr>
								<td>
									<span>Pais<span class="asterisco">*</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
								</td>
								<td>
									<select type="text" name="pais" id="pais" onchange="mostrar_departamento()" style="text-transform:capitalize">
										<option value="">Seleccione...</option>
										<?php
										$Seleccion = mysqli_query($conex, "SELECT * FROM `bayer_pais` WHERE ID_PAIS != '2' AND ID_PAIS != '5' ORDER BY NOMBRE_PAIS ASC");
										while ($fila = mysqli_fetch_array($Seleccion)) {
											$PAIS = $fila['NOMBRE_PAIS'];
											echo "<option>" . $PAIS . "</option>";
										}
										?>
									</select>
								</td>
								<td>
									<span>Departamento<span class="asterisco">*</span></span>
								</td>
								<td>
									<select type="text" name="departamento" id="departamento" onchange="mostrar_ciudades()" style="text-transform:capitalize">
										<option value="">Seleccione...</option>
									</select>
								</td>
							</tr>
							<tr>
								<td>
									<span>Ciudad<span class="asterisco">*</span></span>
								</td>
								<td>
									<select type="text" name="ciudad" id="ciudad">
										<option value="">Seleccione...</option>
									</select>
								</td>
							</tr>
						</table>
					</div>
				</div>
				<div class="AccordionPanel">
					<div class="AccordionPanelTab" style="padding:5px"><strong>DETALLES</strong></div>
					<div class="AccordionPanelContent">
						<table width="100%" border="0">
							<tr>
								<td width="20%">
									<span>Iniciales Paciente<span class="asterisco">*</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
								</td>
								<td width="30%">
									<input type="text" class="input_t" name="ini_pas" id="ini_pas">
								</td>
								<td width="20%">
									<span>Tipo de Documento Paciente<span class="asterisco">*</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
								</td>
								<td width="20%">
									<select name="tipo_documento_p" id="tipo_documento_p">
										<option>Seleccione...</option>
										<option>R.C</option>
										<option>T.I</option>
										<option>C.C</option>
										<option>C.E</option>
										<option>P.T</option>
									</select>
								</td>
							</tr>
							<tr>
								<td>
									<span>Numero de Documento Paciente<span class="asterisco">*</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
								</td>
								<td>
									<input type="text" class="input_t" name="identificacion_pa" id="identificacion_pa" />
								</td>
								<td width="20%">
									<span>Fecha de Nacimiento Paciente</span>
								</td>
								<td width="30%">
									<input type="date" name="fecha_nacimiento_pa" id="fecha_nacimiento_pa" max="<?php echo date('Y-m-d'); ?>" />
								</td>
							</tr>
						</table>
					</div>
				</div>
				<div class="AccordionPanel">
					<div class="AccordionPanelTab" style="padding:5px"><strong>INFORMACION DE TRATAMIENTO</strong></div>
					<div class="AccordionPanelContent">
						<table width="100%" border="0">
							<tr>
								<td width="20%">
									<span>Clasificacion Patologica<span class="asterisco">*</span></span>
								</td>
								<td width="30%">
									<select name="clasificacion_patologica" id="clasificacion_patologica">
										<option>Seleccione...</option>
										<option>Cancer Colorectal</option>
										<option>Cancer Pulmon</option>
										<option>Cancer Tiroides</option>
										<option>Carcinomas Pediatricos (Cancer)</option>
										<option>Sarcoma</option>
									</select>
								</td>
								<td width="20%">
									<span>Forma de Ingreso<span class="asterisco">*</span></span>
								</td>
								<td width="30%">
									<select type="text" name="consentimiento" id="consentimiento">
										<option value="">Seleccione...</option>
										<option>Fisico</option>
										<option>Verbal</option>
										<option>SMS</option>
									</select>
								</td>
							</tr>
							<tr>
								<td>
									<span>Asegurador<span class="asterisco">*</span></span>
								</td>
								<td>
									<?php $query =  mysqli_query($conex, "SELECT DISTINCT ASEGURADOR FROM bayer_asegurador WHERE ESTADO = 'IN' ORDER BY ID_ASEGURADOR DESC")
									?>
									<input list="asegura" name="asegurador" id="asegurador" autocomplete="off">
									<datalist id="asegura">
										<?php
										while ($valores = mysqli_fetch_array($query)) {
										?>
											<option><?php echo $valores['ASEGURADOR'] ?></option>
										<?php
										}
										?>
									</datalist>
								</td>
								<td width="20%">
									<span>Regimen<span class="asterisco">*</span></span>
								</td>
								<td width="30%">
									<select type="text" name="regimen" id="regimen">
										<option value="">Seleccione...</option>
										<option>Contributivo</option>
										<option>Especial</option>
										<option>Subsidiado</option>
									</select>
								</td>
							</tr>
							<tr>
								<td></td>
								<td id="otro_asegurador" style="display:none">
									<span>Asegurador por habilitar<span class="asterisco">*</span></span>
									<input name="asegurador_otro" id="asegurador_otro" type="text" style="width:78%;" />
								</td>
							</tr>
							<tr>
								<td width="20%">
									<span>Ips Paciente<span class="asterisco">*</span></span>
								</td>
								<td width="30%">
									<input class="input_t" type="text" name="ips_atiende_pa" id="ips_atiende_pa">
								</td>
								<td width="20%">
									<span>Fecha de la Pr&oacute;xima Llamada<span class="asterisco">*</span></span>
								</td>
								<td width="30%">
									<input type="date" name="fecha_actual" id="fecha_actual" value="<?php echo date('Y-m-d'); ?>" readonly="readonly" hidden />
									<input class="input_t" type="date" class="input_t" name="fecha_proxima_llamada" id="fecha_proxima_llamada" min="<?php echo date('Y-m-d'); ?>">
								</td>
							</tr>
							<tr>
								<td width="20%">
									<span>
										Laboratorio Diagnostico
									</span>
								</td>
								<td width="30%">
									<input class="input_t" type="text" name="laboratorio_diagnostico" id="laboratorio_diagnostico">
								</td>
								<td width="20%">
									<span>
										Laboratorio Patologico
									</span>
								</td>
								<td width="30%">
									<input class="input_t" type="text" name="laboratorio_patologico" id="laboratorio_patologico">
								</td>
							</tr>
							<tr>
								<td width="20%">
									<span>
										Examenes
									</span>
								</td>
								<td width="30%" align="center">
									<select name="left" id="left" style="height:143px;" multiple="multiple">
										<option value="Ecografia Hepato - biliar (EC)">Ecografia Hepato - biliar (EC)</option>
										<option value="Alfa-fetoproteina (AFP)">Alfa-fetoproteina (AFP)</option>
										<option value="Tomografia Axial Computarizada (TAC) de Cuatro Fases dinamicas especializada en nodulo hepatico">Tomografia Axial Computarizada (TAC) de Cuatro Fases dinamicas especializada en nodulo hepatico</option>
										<option value="Resonancia Magnetica (RM) si y con medio de contraste">Resonancia Magnetica (RM) si y con medio de contraste</option>
										<option value="Creatinina">Creatinina</option>
										<option value="Nitrogeno ureico en Sangre (BUN)">Nitrogeno ureico en Sangre (BUN)</option>
										<option value="Inmunohistoquimica (IHC)">Inmunohistoquimica (IHC)</option>
										<option value="Next generation Sequencing (NGS)">Next generation Sequencing (NGS)</option>
									</select>
								</td>
								<td width="20%" align="center">
									<input type="button" value=" >>>> " id="turnRightBtn" onClick="moveOption(document.getElementById('left'), document.getElementById('examen'))">
									<br>
									<br>
									<input type="button" id="turnLeftBtn" value=" <<<< " onClick="moveOption(document.getElementById('examen'), document.getElementById('left'))">
								</td>
								<td align="left">
									<select name="examen" id="examen" style="height:143px;" multiple="multiple">
									</select>
									<input type="hidden" name="examen_p" id="examen_p">
								</td>
							</tr>
							<tr>
								<td>
									<span>
										Tipo de PAD
									</span>
								</td>
								<td>
									<select name="tipo_pad" id="tipo_pad">
										<option>Seleccione...</option>
										<option>HCC</option>
										<option>NTRK</option>
									</select>
								</td>
								<td>
									<span>
										Fecha de resultado
									</span>
								</td>
								<td>
									<input type="date" name="fecha_resultado" id="fecha_resultado">
								</td>
							</tr>
							<tr>
								<td>
									<span>
										Fecha de entrega bloques de patología
									</span>
								</td>
								<td>
									<input type="date" name="fecha_entrega_bloque" id="fecha_entrega_bloque">
								</td>
								<td>
									<span>
										Caso En Proceso O Cerrado
									</span>
								</td>
								<td>
									<select name="caso" id="caso">
										<option>Seleccione...</option>
										<option>CERRADO - IHC</option>
										<option>PENDIENTE RESULTADOS IHC</option>
										<option>PENDIENTE BLOQUE</option>
										<option>PENDIENTE RESULTADOS NGS</option>
										<option>CERRADO - NO PROCESADO</option>
										<option>PENDIENTE BLOQUE (Perdido)</option>
									</select>
								</td>
							</tr>
							<tr>
								<td width="20%">
									<span>
										Documentos recibidos
									</span>
								</td>
								<td width="30%" align="center">
									<select name="left_docu" id="left_docu" style="height:143px;" multiple="multiple">
										<option value="Orden Medica">Orden Medica</option>
										<option value="Vocher">Vocher</option>
										<option value="Fotocopia cedula paciente">Fotocopia cedula paciente</option>
										<option value="Historia Clinica">Historia Clinica</option>
										<option value="Autorizacion de retiro de Muestras">Autorizacion de retiro de Muestras</option>
									</select>
								</td>
								<td width="20%" align="center">
									<input type="button" value=" >>>> " id="turnRightBtn" onClick="moveOption2(document.getElementById('left_docu'), document.getElementById('documentos_recibidos'))">
									<br>
									<br>
									<input type="button" id="turnLeftBtn" value=" <<<< " onClick="moveOption2(document.getElementById('documentos_recibidos'), document.getElementById('left_docu'))">
								</td>
								<td align="left_docu">
									<select name="documentos_recibidos" id="documentos_recibidos" style="height:143px;" multiple="multiple">
									</select>
									<input type="hidden" name="documentos_recibidos_p" id="documentos_recibidos_p">
								</td>
							</tr>
							<tr>
								<td colspan="4">
									<div id="div_material_agregar" style="width:50%; margin:auto auto; display:none">
										<iframe name="registro_productos_nuevo" style="width:100%; height:250px; border:1px solid #000;" scrolling="auto"></iframe>
									</div>
								</td>
							</tr>
						</table>
						<br />
					</div>
				</div>
				<div class="AccordionPanel">
					<div class="AccordionPanelTab" style="padding:5px"><strong>NOTAS Y ADJUNTOS</strong></div>
					<div class="AccordionPanelContent">
						<br />
						<br />
						<div style="width:91.4%;margin-left: 15px; ">
							<textarea class="tf w-input" style="width:100%; height:100px" id="txtCurp" name="nota" maxlength="5000" onkeypress="return check(event)" placeholder="Nota" id="test"></textarea>
						</div>
						<br />
						<br />
						<div style=" display: flex;  flex-wrap: wrap; margin-left: 15px; ">
							<div style="flex: 0 0 50%;  max-width: 50%;">
								<span>Tipo de Adjunto<span class="asterisco"></span></span>
								<input type="radio" name="tipo_adjunto" id="tipo_adjunto" style=" width:5%; display:none" value="" checked="checked" />
								<input type="radio" name="tipo_adjunto" id="tipo_adjunto" style=" width:5%;" value="Voucher" />Voucher
								<input type="radio" name="tipo_adjunto" id="tipo_adjunto" style=" width:5%;" value="CI" />CI
								<div class="input__row uploader">
									<div id="inputval" class="input-value"></div>
									<label for="archivo"></label>
									<input type="file" class="upload" name="archivo" id="archivo" class="aceptar" multiple>
								</div>
								<script>
									$('#archivo').on('change', function() {
										$('#inputval').text($(this).val());
									});
								</script>
							</div>
						</div>
						<div style="flex: 0 0 33.333333%; max-width: 33.333333%;"></div>
						<div style="flex: 0 0 33.333333%; max-width: 33.333333%;"></div>
						<center>
							<?php
							if ($privilegios != 5) {
							?>
								<input id="registrar" name="registrar" type="submit" value="REGISTRAR" class="btn_registrar" onClick="return validar(paciente_nuevo,1)" />
								<br />
								<br />
							<?PHP
							}
							?>
					</div>
				</div>
			</div>
		</form>
		<script type="text/javascript">
			function check(e) {
				tecla = (document.all) ? e.keyCode : e.which;
				if (tecla == 13 || tecla == 8 || tecla == 9 || tecla == 28 || tecla == 15 || tecla == 37 || tecla == 39) {
					return true;
				}
				patron = /[A-Za-z0-9-., )(., ]/;
				tecla_final = String.fromCharCode(tecla);
				return patron.test(tecla_final);
			}
			var Accordion1 = new Spry.Widget.Accordion("Accordion1");
		</script>
	</body>
<?php
} else {
?>
	<script type="text/javascript">
		window.onload = window.top.location.href = "../logica/cerrar_sesion2.php";
	</script>
<?php
}
?>

</html>