<?php
//require('./../datos/parse_str.php');
include('../logica/session.php')
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Documento sin titulo</title>
    <link type="text/css" rel="stylesheet" href="css/estilo_form_paciente.css" />
    <script src="css/SpryAssets/SpryAccordion.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/validaciones.js"></script>
    <link href="css/SpryAssets/SpryAccordion.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery.js"></script>
    <script type="text/javascript" src="js/calcular_edad.js"></script>
    <script type="text/javascript" src="js/direccion.js"></script>
    </script>
    <script src="../presentacion/js/jquery.js"></script>
    <script type="text/javascript" src="js/validar_campos_modificacion.js"></script>
    <script language=javascript>
        function ventanaSecundaria(URL) {
            window.open(URL, "ventana1", "width=1300,height=500,Top=150,Left=50%");
        }
    </script>
    <script>
        function moveOption(e1, e2) {
            for (var i = 0; i < e1.options.length; i++) {
                if (e1.options[i].selected) {
                    var e = e1.options[i];
                    e2.options.add(new Option(e.text, e.value));
                    e1.remove(i);
                    i = i - 1
                }
            }
            document.getElementById('examen_p').value = getvalue(document.getElementById('examen'));
        }

        function moveOption2(e1, e2) {
            for (var i = 0; i < e1.options.length; i++) {
                if (e1.options[i].selected) {
                    var e = e1.options[i];
                    e2.options.add(new Option(e.text, e.value));
                    e1.remove(i);
                    i = i - 1
                }
            }
            document.getElementById('documentos_recibidos_p').value = getvalue(document.getElementById('documentos_recibidos'));
        }

        function getvalue(geto) {
            var resultArray = new Array();
            for (var i = 0; i < geto.options.length; i++) {
                resultArray.push(geto.options[i].value);
            }
            return resultArray.join();
        }
    </script>
    <style>
        td {
            padding: 3px;
            background-color: transparent;
        }
    </style>
    <script type="text/javascript">
        function trat_previo(sel) {
            if (sel.value == "Otro") {
                divC = document.getElementById("otro_tratamiento");
                divC.style.display = "";
            }
            if (sel.value != "Otro") {
                divC = document.getElementById("otro_tratamiento");
                divC.style.display = "none";
            }
        }
    </script>
    <script type="text/javascript">
        function status() {
            var REFERENCIA = $('#MEDICAMENTO').val();
            var STATUS = $('#status_paciente').val();
            $.ajax({
                url: '../presentacion/listado_producto_status_cargar.php',
                data: {
                    REFERENCIA: REFERENCIA,
                    STATUS: STATUS
                },
                type: 'post',
                beforeSend: function() {
                    $("#status_paciente").attr('disabled', 'disabled');
                },
                success: function(data) {
                    $("#status_paciente").removeAttr('disabled');
                    $('#status_paciente').html(data);
                }
            })
        }

        function mostrar_departamento() {
            var pais = $('#pais').val();
            $("#departamento").html('<img src="imgagenes/cargando.gif" />');
            $.ajax({
                url: '../presentacion/departamentos.php',
                data: {
                    pais: pais,
                },
                type: 'post',
                beforeSend: function() {
                    $('#departamento').attr('disabled');
                    $("#departamento").html("Procesando, espere por favor" + '<img src="img/cargando.gif" />');
                },
                success: function(data) {
                    $('#departamento').removeAttr("disabled");
                    $('#departamento').html(data);
                }
            })
        }

        function mostrar_ciudades() {
            var departamento = $('#departamento').val();
            $("#ciudad").html('<img src="imgagenes/cargando.gif" />');
            $.ajax({
                url: '../presentacion/ciudades.php',
                data: {
                    dep: departamento,
                },
                type: 'post',
                beforeSend: function() {
                    $('#ciudad').attr('disabled');
                    $("#ciudad").html("Procesando, espere por favor" + '<img src="img/cargando.gif" />');
                },
                success: function(data) {
                    $('#ciudad').removeAttr("disabled");
                    $('#ciudad').html(data);
                }
            })
        }
    </script>
    <script>
        $(document).ready(function() {
            status();
            $('#cambio').click(function() {
                $('#cambio_direccion').toggle();
                $('#DIRECCION').val('');
                $("#VIA option:eq(0)").attr("selected", "selected");
                $("#interior option:eq(0)").attr("selected", "selected");
                $("#interior2 option:eq(0)").attr("selected", "selected");
                $("#interior3 option:eq(0)").attr("selected", "selected");
                $("#TERAPIA option:eq(0)").attr("selected", "selected");
                $('#detalle_via').val('');
                $('#detalle_int').val('');
                $('#detalle_int2').val('');
                $('#detalle_int3').val('');
                $('#numero').val('');
                $('#numero2').val('');
            });
            var via = $('#VIA').val();
            var dt_via = $('#detalle_via').val();
            $('#VIA').change(function() {
                dir();
            });
            $('#detalle_via').change(function() {
                dir();
            });
            $('#numero').change(function() {
                dir();
            });
            $('#numero2').change(function() {
                dir();
            });
            $('#interior').change(function() {
                dir();
            });
            $('#detalle_int').change(function() {
                dir();
            });
            $('#interior2').change(function() {
                dir();
            });
            $('#detalle_int2').change(function() {
                dir();
            });
            $('#interior3').change(function() {
                dir();
            });
            $('#detalle_int3').change(function() {
                dir();
            });
        });

        function reclamacion() {
            var reclamo = $('#reclamo').val();
            var MEDICAMENTO = $('#MEDICAMENTO').val();
            if (reclamo == 'NO') {
                $("#causa").css('display', 'block');
                $('#causa_no_reclamacion').css('display', 'block');
                $("#fecha_reclamacion_span").css('display', 'none');
                $('#fecha_reclamacion').css('display', 'none');
                $("#consecutivo_betaferon_span").css('display', 'none');
                $('#consecutivo_betaferon').css('display', 'none');
            }
            if (reclamo == 'SI' && MEDICAMENTO == 'BETAFERON CMBP X 15 VPFS (3750 MCG) MM') {
                $("#consecutivo_betaferon_span").css('display', 'block');
                $('#consecutivo_betaferon').css('display', 'block');
                $("#fecha_reclamacion_span").css('display', 'block');
                $('#fecha_reclamacion').css('display', 'block');
                $("#causa").css('display', 'none');
                $('#causa_no_reclamacion').css('display', 'none');
            } else {
                if (reclamo == 'SI') {
                    $("#fecha_reclamacion_span").css('display', 'block');
                    $('#fecha_reclamacion').css('display', 'block');
                    $("#causa").css('display', 'none');
                    $('#causa_no_reclamacion').css('display', 'none');
                }
            }
        }
        $(document).ready(function() {
            $("#operador_logistico").change(function() {
                $("#operador_logistico_nuevo").val('');
                var operador_logistico = $('#operador_logistico').val();
                if (operador_logistico == 'Otro') {
                    $('#operador_logistico_nuevo').css('display', 'inline-block');
                    $('#cual_operador').css('display', 'inline-block');
                }
                if (operador_logistico != 'Otro') {
                    $('#operador_logistico_nuevo').css('display', 'none');
                    $('#cual_operador').css('display', 'none');
                }
            });
            $("#medico").change(function() {
                $("#medico_nuevo").val('');
                var medico = $('#medico').val();
                if (medico == 'Otro') {
                    $('#medico_nuevo').css('display', 'inline-block');
                    $('#cual_medico').css('display', 'inline-block');
                }
                if (medico != 'Otro') {
                    $('#medico_nuevo').css('display', 'none');
                    $('#cual_medico').css('display', 'none');
                }
            });
            var fecha = $('input[name=fecha_nacimiento]').val();
            if (fecha != '') {
                var edad = nacio(fecha);
                $("#edad").val(edad);
            }
            $("input[name=fecha_nacimiento]").change(function() {
                var fecha = $('input[name=fecha_nacimiento]').val();
                var edad = nacio(fecha);
                $("#edad").val(edad);
            });
            reclamacion();
            $("#reclamo").change(function() {
                $("#causa_no_reclamacion").val($('#causa_no_reclamacion').prop('defaultValue'));
                $("#causa_no_reclamacion option:eq(0)").attr("selected", "selected");
                $("#fecha_reclamacion").val($('#fecha_reclamacion').prop('defaultValue'));
                $("#consecutivo_betaferon").val($('#consecutivo_betaferon').prop('defaultValue'));
                reclamacion();
            });
            $("#operador_logistico").change(function() {
                $("#operador_logistico_nuevo").val('');
                var operador_logistico = $('#operador_logistico').val();
                if (operador_logistico == 'Otro') {
                    $('#operador_logistico_nuevo').css('display', 'inline-block');
                    $('#cual_operador').css('display', 'inline-block');
                }
                if (operador_logistico != 'Otro') {
                    $('#operador_logistico_nuevo').css('display', 'none');
                    $('#cual_operador').css('display', 'none');
                }
            });
            $('#crear').change(function() {
                var crear = $('#crear').val();
                $('#descripcion_nuevo_comunicacion').val('');
                if (crear == 'SI') {
                    $('#nueva').css('display', 'block');
                    $('#nueva2').css('display', 'block');
                }
                if (crear == 'NO') {
                    $('#nueva').css('display', 'none');
                    $('#nueva2').css('display', 'none');
                }
                if (crear == '') {
                    $('#nueva').css('display', 'none');
                    $('#nueva2').css('display', 'none');
                }
            });
        });
    </script>
</head>
<?php
$string_intro = getenv("QUERY STRING");
parse_str($string_intro);
require('../datos/conex.php');
$ID_PACIENTE;
$ID_GESTION;
include('../logica/consulta_paciente.php');
if ($privilegios != '' && $usuname_peru != '') {
?>

    <body class="body" style="width:80.9%;margin-left:12%;">
        <form id="seguimiento" name="seguimiento" method="post" action="../logica/actualizar_modificacion.php" onkeydown="return filtro(2)" enctype="multipart/form-data" class="letra">
            <div id="Accordion1" class="Accordion" tabindex="0" style="height:100%;">
                <div class="AccordionPanel">
                    <div class="AccordionPanelTab">PACIENTE</div>
                    <div class="AccordionPanelContent">
                        <table width="100%" border="0">
                            <?php
                            $Seleccion = mysqli_query($conex, "SELECT * FROM `bayer_pacientes` AS P
                            INNER JOIN bayer_tratamiento AS T ON T.ID_PACIENTE_FK=P.ID_PACIENTE
                            WHERE ID_PACIENTE = '" . $ID_PACIENTE . "'");
                            while ($fila = mysqli_fetch_array($Seleccion)) {
                                $ID_PACIENTE2 = $fila['ID_PACIENTE'];
                                $ID_PA = $fila['ID_PACIENTE'];
                                $PAIS = $fila['PAIS_PACIENTE'];
                                $DEPARTAMENTO = $fila['DEPARTAMENTO_PACIENTE'];
                                $CIUDAD = $fila['CIUDAD_PACIENTE'];
                                $MEDICO = $fila['MEDICO_TRATAMIENTO'];
                                $EXAMEN = $fila['EXAMEN'];
                                $ASEGURADOR_TRATAMIENTO = $fila['ASEGURADOR_TRATAMIENTO'];
                                function Zeros($numero, $largo)
                                {
                                    $resultado = $numero;
                                    while (strlen($resultado) < $largo) {
                                        $resultado = "0" . $resultado;
                                    }
                                    return $resultado;
                                }
                                $ID_PACIENTE = Zeros($ID_PA, 5);
                            ?>
                                <tr>
                                    <td width="20%">
                                        <span>Codigo de Usuario</span>
                                    </td>
                                    <td width="30%">
                                        <input name="codigo_gestion" type="text" id="codigo_gestion" max="10" readonly="readonly" value="<?php echo $ID_GESTION; ?>" style="display:none" />
                                        <input name="codigo_usuario" class="input_t" type="text" id="codigo_usuario" max="10" readonly="readonly" value="<?php echo 'PAD' . $ID_PACIENTE; ?>" />
                                        <input name="codigo_usuario2" type="text" id="codigo_usuario2" max="10" readonly="readonly" value="<?php echo $fila['ID_PACIENTE']; ?>" style="display:none" />
                                    </td>
                                    <td width="20%">
                                        <span>Estado<span class="asterisco">*</span></span>
                                    </td>
                                    <td width="30%">
                                        <?php if ($privilegios == 1) { ?>
                                            <select type="text" name="estado_paciente" id="estado_paciente">
                                                <option><?php echo $fila['ESTADO_PACIENTE']; ?></option>
                                                <option>Proceso</option>
                                                <option>Activo</option>
                                                <option>Cierre Efectivo</option>
                                                <option>Cierre No efectivo</option>
                                            </select>
                                        <?php } else { ?>
                                            <input name="estado_paciente" type="text" id="estado_paciente" readonly="readonly" value="<?php echo $fila['ESTADO_PACIENTE']; ?>" />
                                        <?php    } ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        <span>Fecha de Activacion<span class="asterisco">*</span></span>
                                    </td>
                                    <td width="30%">
                                        <input type="date" name="fecha_activacion" id="fecha_activacion" value="<?php echo $fila['FECHA_ACTIVACION_PACIENTE']; ?>" />
                                    </td>
                                    <td width="20%">
                                        <span>Fecha de Cierre</span>
                                    </td>
                                    <td width="30%">
                                        <input type="date" name="fecha_retiro" id="fecha_retiro" max="10" value="<?php echo $fila['FECHA_RETIRO_PACIENTE']; ?>" />
                                    </td>
                                <tr>
                                    <td>
                                        <span>Observaciones Motivo de Cierre</span>
                                    </td>
                                    <td colspan="3">
                                        <textarea name="observacion_retiro" id="observacion_retiro" style="width:98%; height:100px"><?php echo $fila['OBSERVACION_MOTIVO_RETIRO_PACIENTE']; ?></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span>Nombre Medico<span class="asterisco">*</span></span>
                                    </td>
                                    <td>
                                        <input type="text" class="input_t" name="nombre_medico" id="nombre_medico" value="<?php echo $fila['NOMBRE_PACIENTE']; ?>">
                                    </td>
                                    <td>
                                        <span>Apellidos Medico<span class="asterisco">*</span></span>
                                    </td>
                                    <td>
                                        <input type="text" class="input_t" name="apellidos_medico" id="apellidos_medico" value="<?php echo $fila['APELLIDO_PACIENTE']; ?>">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span>Correo Medico</span>
                                    </td>
                                    <td>
                                        <input type="text" class="input_t" name="correo_medico" id="correo_medico" value="<?php echo $fila['CORREO_MEDICO']; ?>">
                                    </td>
                                    <td>
                                        <span>Telefono Medico<span class="asterisco">*</span></span>
                                    </td>
                                    <td>
                                        <input type="text" class="input_t" name="telefono1" id="telefono1" value="<?php echo $fila['TELEFONO_PACIENTE']; ?>">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span>Iniciales Paciente</span>
                                    </td>
                                    <td>
                                        <input type="text" class="input_t" name="ini_pas" id="ini_pas" value="<?php echo $fila['INICIALES_PACIENTE']; ?>">
                                    </td>
                                    <td>
                                        <span>Numero De Documento Paciente<span class="asterisco">*</span></span>
                                    </td>
                                    <td>
                                        <input type="text" class="input_t" name="identificacion_pa" id="identificacion_pa" value="<?php echo $fila['IDENTIFICACION_PACIENTE']; ?>">
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        <span>Fecha de Nacimiento Paciente</span>
                                    </td>
                                    <td width="30%">
                                        <input type="date" name="fecha_nacimiento" id="fecha_nacimiento" max="<?php echo date('Y-m-d'); ?>" value="<?php echo $fila['FECHA_NACIMINETO_PACIENTE']; ?>" />
                                    </td>
                                    <td width="20%">
                                        <span>Tipo de Documento Paciente<span class="asterisco">*</span></span>
                                    </td>
                                    <td width="20%">
                                        <select name="tipo_documento_p" id="tipo_documento_p">
                                            <option value=""><?php echo $fila['TIPO_DE_IDENTIFICACION']; ?></option>
                                            <option>Seleccione...</option>
                                            <option>R.C</option>
                                            <option>T.I</option>
                                            <option>C.C</option>
                                            <option>C.E</option>
                                            <option>P.T</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span>Clasificacion Patologica<span class="asterisco">*</span></span>
                                    </td>
                                    <td>
                                        <select name="clasificacion_patologica" id="clasificacion_patologica">
                                            <option value=""><?php echo $fila['CLASIFICACION_PATOLOGICA_TRATAMIENTO'] ?></option>
                                            <option>Seleccione...</option>
                                            <option>Cancer Colorectal</option>
                                            <option>Cancer Pulmon</option>
                                            <option>Cancer Tiroides</option>
                                            <option>Carcinomas Pediatricos (Cancer)</option>
                                            <option>Sarcoma</option>
                                        </select>
                                    </td>
                                    <td>
                                        <span>Pais<span class="asterisco">*</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                    </td>
                                    <td>
                                        <select type="text" name="pais" id="pais" onchange="mostrar_departamento()" style="text-transform:capitalize">
                                            <option value=""><?php echo $PAIS ?></option>
                                            <option value="">Seleccione...</option>
                                            <?php
                                            $Seleccion = mysqli_query($conex, "SELECT * FROM `bayer_pais` WHERE ID_PAIS != '2' AND ID_PAIS != '5' ORDER BY NOMBRE_PAIS ASC");
                                            while ($fila = mysqli_fetch_array($Seleccion)) {
                                                $PAIS = $fila['NOMBRE_PAIS'];
                                                echo "<option>" . $PAIS . "</option>";
                                            }
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span>Departamento<span class="asterisco">*</span></span>
                                    </td>
                                    <td>
                                        <select type="text" name="departamento" id="departamento" onchange="mostrar_ciudades()" style="text-transform:capitalize">
                                            <option value=""><?php echo $DEPARTAMENTO ?></option>
                                            <option value="">Seleccione...</option>
                                        </select>
                                    </td>
                                    <td>
                                        <span>Ciudad<span class="asterisco">*</span></span>
                                    </td>
                                    <td>
                                        <select type="text" name="ciudad" id="ciudad">
                                            <option value=""><?php echo $CIUDAD ?></option>
                                            <option value="">Seleccione...</option>
                                        </select>
                                    </td>
                                </tr>
                        </table>
                    </div>
                </div>
                <div class="AccordionPanel">
                    <div class="AccordionPanelTab">GENERAL</div>
                    <div class="AccordionPanelContent">
                        <table width="100%">
                            <tr>
                                <td>
                                    <span>Asegurador Paciente<span class="asterisco">*</span></span>
                                </td>
                                <td>
                                    <?php $query =  mysqli_query($conex, "SELECT DISTINCT ASEGURADOR FROM bayer_asegurador WHERE ESTADO = 'IN' ORDER BY ID_ASEGURADOR DESC")
                                    ?>
                                    <input list="asegura" name="asegurador" id="asegurador" autocomplete="off" value="<?php echo $ASEGURADOR_TRATAMIENTO ?>">
                                    <datalist id="asegura">
                                        <?php
                                        while ($valores = mysqli_fetch_array($query)) {
                                        ?>
                                            <option><?php echo $valores['ASEGURADOR'] ?></option>
                                        <?php
                                        }
                                        ?>
                                    </datalist>
                                </td>
                                <td>
                                    <span>Autor</span>
                                    <br />
                                    <br />
                                </td>
                                <td>
                                    <input type="text" class="input_t" name="autor" id="autor" readonly="readonly" value="<?php echo $usuname_peru ?>" />
                                    <br />
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <?php
                                $Seleccion1 = mysqli_query($conex, "SELECT * FROM bayer_gestiones
		                        WHERE ID_PACIENTE_FK2 = '" . $ID_PACIENTE . "' ORDER BY ID_GESTION DESC LIMIT 1");
                                $num1 = mysqli_num_rows($Seleccion1);
                                if ($num1 > 0) {
                                    while ($consul = mysqli_fetch_array($Seleccion1)) {
                                        $DESCRIPCION_COMUNICACION_GESTION = $consul['DESCRIPCION_COMUNICACION_GESTION'];
                                        $TIPO_PAD = $consul['TIPO_PAD'];
                                        $FECHA_RESULTADO = $consul['FECHA_RESULTADO'];
                                        $FECHA_ENTREGA_BLOQUES = $consul['FECHA_ENTREGA_BLOQUES'];
                                        $DOCUMENTOS_RECIBIDOS = $consul['DOCUMENTOS_RECIBIDOS'];
                                        $CASO = $consul['CASO'];
                                ?>
                            </tr>
                            <tr>
                                <td width="20%">
                                    <span>
                                        Examenes
                                    </span>
                                </td>
                                <td width="30%" align="center">
                                    <select name="left" id="left" style="height:143px;" multiple="multiple">
                                        <option value="Ecografia Hepato - biliar (EC)">Ecografia Hepato - biliar (EC)</option>
                                        <option value="Alfa-fetoproteina (AFP)">Alfa-fetoproteina (AFP)</option>
                                        <option value="Tomografia Axial Computarizada (TAC) de Cuatro Fases dinamicas especializada en nodulo hepatico">Tomografia Axial Computarizada (TAC) de Cuatro Fases dinamicas especializada en nodulo hepatico</option>
                                        <option value="Resonancia Magnetica (RM) si y con medio de contraste">Resonancia Magnetica (RM) si y con medio de contraste</option>
                                        <option value="Creatinina">Creatinina</option>
                                        <option value="Nitrogeno ureico en Sangre (BUN)">Nitrogeno ureico en Sangre (BUN)</option>
                                        <option value="Inmunohistoquimica (IHC)">Inmunohistoquimica (IHC)</option>
                                        <option value="Next generation Sequencing (NGS)">Next generation Sequencing (NGS)</option>
                                    </select>
                                </td>
                                <td width="20%" align="center">
                                    <input type="button" value=" >>>> " id="turnRightBtn" onClick="moveOption(document.getElementById('left'), document.getElementById('examen'))">
                                    <br>
                                    <br>
                                    <input type="button" id="turnLeftBtn" value=" <<<< " onClick="moveOption(document.getElementById('examen'), document.getElementById('left'))">
                                </td>
                                <td align="left">
                                    <select name="examen" id="examen" style="height:143px;" multiple="multiple">
                                        <option><?php echo $EXAMEN ?></option>
                                    </select>
                                    <input type="hidden" name="examen_p" id="examen_p" value="<?php echo $EXAMEN ?>">
                                </td>
                            </tr>
                        <?php
                                    }
                                } else {
                        ?>
                        </tr>
                    <?php
                                }
                    ?>
                    <?php
                                $Seleccion1 = mysqli_query($conex, "SELECT * FROM bayer_gestiones
		                        WHERE ID_PACIENTE_FK2 = '" . $ID_PACIENTE . "' ORDER BY ID_GESTION DESC LIMIT 1");
                                while ($consul = mysqli_fetch_array($Seleccion1)) {
                                    $TIPO_PAD = $consul['TIPO_PAD'];
                                    $FECHA_RESULTADO = $consul['FECHA_RESULTADO'];
                                    $FECHA_ENTREGA_BLOQUES = $consul['FECHA_ENTREGA_BLOQUES'];
                                    $DOCUMENTOS_RECIBIDOS = $consul['DOCUMENTOS_RECIBIDOS'];
                                    $CASO = $consul['CASO'];
                    ?>
                        <tr>
                            <td>
                                <span>
                                    Tipo de PAD
                                </span>
                            </td>
                            <td>
                                <select name="tipo_pad" id="tipo_pad">
                                    <option><?php echo $TIPO_PAD ?></option>
                                    <option>Seleccione...</option>
                                    <option>HCC</option>
                                    <option>NTRK</option>
                                </select>
                            </td>
                            <td>
                                <span>
                                    Fecha de resultado
                                </span>
                            </td>
                            <td>
                                <input type="date" name="fecha_resultado" id="fecha_resultado" value="<?php echo $FECHA_RESULTADO ?>">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span>
                                    Fecha de entrega bloques de patología
                                </span>
                            </td>
                            <td>
                                <input type="date" name="fecha_entrega_bloque" id="fecha_entrega_bloque" value="<?php echo $FECHA_ENTREGA_BLOQUES ?>">
                            </td>
                            <td>
                                <span>
                                    Caso En Proceso O Cerrado
                                </span>
                            </td>
                            <td>
                                <select name="caso" id="caso">
                                    <option><?php echo $CASO ?></option>
                                    <option>Seleccione...</option>
                                    <option>CERRADO - IHC</option>
                                    <option>PENDIENTE RESULTADOS IHC</option>
                                    <option>PENDIENTE BLOQUE</option>
                                    <option>PENDIENTE RESULTADOS NGS</option>
                                    <option>CERRADO - NO PROCESADO</option>
                                    <option>PENDIENTE BLOQUE (Perdido)</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td width="20%">
                                <span>
                                    Documentos recibidos
                                </span>
                            </td>
                            <td width="30%" align="center">
                                <select name="left_docu" id="left_docu" style="height:143px;" multiple="multiple">
                                    <option value="Orden Medica">Orden Medica</option>
                                    <option value="Vocher">Vocher</option>
                                    <option value="Fotocopia cedula paciente">Fotocopia cedula paciente</option>
                                    <option value="Historia Clinica">Historia Clinica</option>
                                    <option value="Autorizacion de retiro de Muestras">Autorizacion de retiro de Muestras</option>
                                </select>
                            </td>
                            <td width="20%" align="center">
                                <input type="button" value=" >>>> " id="turnRightBtn" onClick="moveOption2(document.getElementById('left_docu'), document.getElementById('documentos_recibidos'))">
                                <br>
                                <br>
                                <input type="button" id="turnLeftBtn" value=" <<<< " onClick="moveOption2(document.getElementById('documentos_recibidos'), document.getElementById('left_docu'))">
                            </td>
                            <td align="left_docu">
                                <select name="documentos_recibidos" id="documentos_recibidos" style="height:143px;" multiple="multiple">
                                    <option><?php echo $DOCUMENTOS_RECIBIDOS ?></option>
                                </select>
                                <input type="hidden" name="documentos_recibidos_p" id="documentos_recibidos_p" value="<?php echo $DOCUMENTOS_RECIBIDOS ?>">
                            </td>
                        </tr>
                    <?php } ?>
                    <tr>
                        <td>
                            <span>Fecha de la Proxima Llamada<span class="asterisco">*</span></span>
                        </td>
                        <?php
                                $Sel = mysqli_query($conex, "SELECT FECHA_PROXIMA_LLAMADA, ID_GESTION FROM bayer_gestiones
                                    WHERE ID_PACIENTE_FK2 = '" . $ID_PACIENTE . "' ORDER BY FECHA_PROXIMA_LLAMADA DESC LIMIT 1");
                                while ($con = mysqli_fetch_array($Sel)) {
                                    $ID_GESTION_ULT = $con['ID_GESTION'];
                        ?>
                            <td>
                                <input type="text" name="id_gestion_ult" id="id_gestion_ult" value="<?php echo $ID_GESTION_ULT ?>" style="display:none" />
                                <input type="date" name="fecha_actual" id="fecha_actual" value="<?php echo date('Y-m-d'); ?>" readonly="readonly" hidden />
                                <input type="date" name="fecha_proxima_llamada_ant" id="fecha_proxima_llamada_ant" value="<?php echo $con['FECHA_PROXIMA_LLAMADA'] ?>" style="display:none" />
                                <input type="date" name="fecha_proxima_llamada" id="fecha_proxima_llamada" min="<?php echo date('Y-m-d'); ?>" value="<?php echo $con['FECHA_PROXIMA_LLAMADA'] ?>" />
                            </td>
                        <?php
                                }
                        ?>
                        <td>
                            <span>Motivo de Proxima Llamada<span class="asterisco">*</span></span>
                            <br />
                            <br />
                        </td>
                        <td>
                            <select type="text" name="motivo_proxima_llamada" id="motivo_proxima_llamada">
                                <option value="">Seleccione...</option>
                                <option>Actualizacion de Datos</option>
                                <option>Campanas</option>
                                <option>Cumpleanos</option>
                                <option>Egreso</option>
                                <option>Encuestas</option>
                                <option>Ingreso</option>
                                <option>Reclamacion</option>
                                <option>Remision de Caso</option>
                                <option>Respuesta de Caso</option>
                                <option>Seguimiento</option>
                            </select>
                            <br />
                            <br />
                        </td>
                    </tr>
                    <?php
                                if ($num1 > 0) {
                    ?>
                        <tr>
                            <td>
                                <span>Descripcion de Comunicaci&oacute;n</span>
                                <br />
                                <br />
                            </td>
                            <td colspan="3">
                                <textarea style="width:98%; height:72.5px;" id="descripcion_comunicacion" name="descripcion_comunicacion"><?php echo $DESCRIPCION_COMUNICACION_GESTION ?></textarea>
                                <br />
                                <br />
                            </td>
                        </tr>
                    <?php
                                }
                    ?>
                    <tr>
                        <td>
                            <span>Crear Gesti&oacute;n</span>
                            <br />
                            <br />
                        </td>
                        <td colspan="1">
                            <select name="crear" id="crear">
                                <option value="NO"></option>
                                <option>SI</option>
                                <option>NO</option>
                            </select>
                            <br />
                            <br />
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <div style="display:none" id="nueva">
                                <span>Descripcion Nueva Comunicaci&oacute;n</span>
                                <br />
                                <br />
                            </div>
                        </td>
                        <td colspan="3">
                            <div style="display:none" id="nueva2">
                                <textarea class="tf w-input" id="txtCurp" style="width:98%; height:72.5px;" id="descripcion_nuevo_comunicacion" maxlength="800" name="descripcion_nuevo_comunicacion" maxlength="5000" onkeypress="return check(event)" placeholder="Nota"></textarea>
                                <br />
                                <br />
                            </div>
                        </td>
                    </tr>
                <?php
                            }
                ?>
                        </table>
                        <br />
                        <br />
                        <center>
                            <input id="registrar" name="registrar" type="submit" value="REGISTRAR" class="btn_actualizar" onClick="return validar(seguimiento,1)" />
                        </center>
                        <br />
                        <br />
                    </div>
                </div>
            </div>
        </form>
        <script type="text/javascript">
            function check(e) {
                tecla = (document.all) ? e.keyCode : e.which;
                if (tecla == 8) {
                    return true;
                }
                patron = /[A-Za-z0-9-.,)(., ]/;
                tecla_final = String.fromCharCode(tecla);
                return patron.test(tecla_final);
            }
            var Accordion1 = new Spry.Widget.Accordion("Accordion1");
        </script>
    </body>
<?php
} else {
?>
    <script type="text/javascript">
        window.onload = window.top.location.href = "../logica/cerrar_sesion2.php";
    </script>
<?php
}
?>

</html>